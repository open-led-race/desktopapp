/*
*  PhysicalDevice: Store device properties (name, index, CommSpeed, ...) 
*    and the Car LIST contained in the device.
*  Car is defined as an INNER CLASS
*/

class PhysicalDevice {

  private String boardId = null;
  private String boardSoftwareId = null;
  private String boardSoftwareVer = null;

  private String name = null;
  private String description = null;
  private int    boardBrate = 0;
  private int    handshakeTimeout=2000; // default val
  private int    lookupTimeout=1400; // default val
          long   openToSendDelay=1200; 

  SerialPort     serialPort = null;  // Serial Port of the OLR Board
  SerialPortList serialPortList; // Class used to get list of Available (Valid) OS serial ports 
  SerialPort     ports[];        // List of available serial ports 

  String  port=null;
  String  portProperties=null;

  public  boolean isSearching=false; // SET in LocalClient.draw() and RESET here after a complete unsuccesful scan of any available ports
  
  /* 
   * Vars managing the "connect to device" phase 
   */
   
  // deviceFound: true after the Configured "Device ID" is found on a Port (step 1)
  private boolean deviceFound=false;  
  
  // isPortValid: true after checking if the configured DeviceId found 
  // on step 1, runs the correct firmware version (step 2)
  public  boolean isPortValid=false;    
  
  // true after succesfull handshake on the Port where the configured 
  // device was found (step 1+2)
  public  boolean isConnected=false;  

  
  private long lastHandshakeAttempt;  // System.currentTimeMillis()

  private long lastPortLookupTime;  // System.currentTimeMillis()
  private int lastLookupIdx; 
  private SerialPort lastLookupPort;
  private int lastLookupListLength;
  private char cmdBuf[];

  private long lastPortOpenTime;  // System.currentTimeMillis()   
  private long reset1200PortOpenTime;  // System.currentTimeMillis()

  private boolean querySent;
  private boolean setConfigurationModeDone;
  
  
  private boolean sendConfirmedCmd_sent;
  private boolean sendConfirmed_RCmd_sent;

  // used in readCmd()
  protected final static int READBUFFERSIZE = 120;
  private char    serialBuffer[];
  private int     sbufIdx;
  private boolean sbufBitEncoded;

  public  Deque<String> fromBoardQ;
  private boolean ignoreMessagesFromBoard=false; // when true discharge incoming messages (do not put them into fromBoardQ)    

  
  private Trace t;
  private static final String _CLASS_ = "PhysicalDevice";

  public  Car[] carList;

  /**
   * Constructor
   */
  PhysicalDevice (String sname, JSONObject pd, String logStr)   {   
    
    this.boardId     = getJSONStringOrExit(pd,"id");
    this.name        = getJSONStringOrExit(pd,"name");
    this.description = trim(pd.getString("description" , "-"));
    this.boardBrate  = pd.getInt("baudRate");  // fare util ..OrExit() per Int
    
    this.handshakeTimeout = pd.getInt("HandshakeTimeout"); 
    this.lookupTimeout  = pd.getInt("LookupTimeout");  
    this.openToSendDelay =  pd.getInt("OpenToSendDelay");  
    
    if(logStr.equals("-") ){
      t = new Trace(Trace.NONE);
    } else {
      t = new Trace(logStr);
    }    
    t.setPublisherId(this.boardId);
    t.setClassId(_CLASS_);
    t.log(Trace.METHOD, String.format("Constructor(%s, %s, %s, %d)\n",this.boardId,this.name,this.description,this.boardBrate));

    // Create Cars Array       
    JSONArray cars = getJSONArrayOrExit(pd, "Cars");
    int carsNum = cars.size();
    carList = new Car[carsNum];
    for (int j = 0; j < carsNum; j++) {
       // Get each Car object in the array
       JSONObject car = cars.getJSONObject(j);
       // get name, id
       int carid = car.getInt("id");       // fare util ..OrExit() per Int
       String carname = getJSONStringOrExit(car,"name"); 
       carList[j] = new Car (carid, carname);
    }

    serialBuffer = new char[READBUFFERSIZE];
    sbufIdx=0;
    cmdBuf = new char[READBUFFERSIZE];
    
    // Queue storing the Received commands
    fromBoardQ = new ArrayDeque<String>();
    
    
    lastPortLookupTime = 0;  
    lastLookupIdx  = -1; 
    lastLookupPort = null;
    lastLookupListLength = 0;

    this.lastPortOpenTime = 0;
    this.reset1200PortOpenTime = 0;
    this.querySent = false;
    this.setConfigurationModeDone = false;
    this.sendConfirmedCmd_sent = false;
    this.sendConfirmed_RCmd_sent = false;
    // used in findBoard() 
    serialPortList = new SerialPortList (this.boardId,logStr);
    ports = serialPortList.getPortList();  // Get list of VALID Ports

  }
  
  

  public void loop () {
    
   if( this.serialPort == null || ! this.serialPort.isOpen()) {
     return; // do nothing
   }
    
    // Read from Serial
    int bytesRead = readCmd(cmdBuf);
    if(bytesRead != 0){
      char buf[] = new char[bytesRead];
      for(int i=0;i<bytesRead;i++){
        buf[i]=cmdBuf[i];
      } 
      // OFFER to [fromBoardQ] commands received from Board  
      if(ProtocolSerial.Cmd.isValid(buf[0])) {
        if(this.ignoreMessagesFromBoard) {
           t.log(Trace.DETAILED, String.format("loop():ignoreMessagesFromBoard[TRUE] - Port[%s]->[%s] DISCHARGED\n", this.port, String.valueOf(buf)));
        } else {
          boolean ok = fromBoardQ.offer(String.valueOf(buf));
          if(! ok) {
            t.error(String.format("loop() ERROR: Serial [fromBoardQ] QUEUE FULL - offer() returns FALSE\n"));
          }
         if(buf[0]== ProtocolSerial.Cmd.CarLeave.ID) {
           String binStr = String.format("%8s", Integer.toBinaryString(Integer.valueOf((int)buf[1]))).replace(' ', '0');
           t.log(Trace.DETAILED, String.format("loop():Port[%s]->recv:[%c][%8s][EOC] -> added to QUEUE\n", this.port, buf[0], binStr ));
         } else if(buf[0]== ProtocolSerial.Cmd.CarTelemetry.ID) {
           t.log(Trace.LOWLEVEL,  String.format("loop():Port[%s]->recv:[%s] -> added to QUEUE\n", this.port, String.valueOf(buf)) );
         }  else {
           t.log(Trace.METHOD + Trace.DETAILED, String.format("loop():Port[%s]->recv:[%s] -> added to QUEUE\n", this.port, String.valueOf(buf)) );
         }
      }

      } else {
        t.error(String.format("loop() ERROR:Port[%s]->invalid Command Received:[%s]\n", this.port, String.valueOf(buf)));
      }
    }
    
  } // loop()




  /**
   * Non-blocking (to be called in a loop)
   *
   * -- This method is used when the caller (usually OpenLEDRace Board Config Software)
   * -- has found a Board on a specific serial port and wants to identify 
   * -- the software Id/Version running on it 
   * --   Unlike for "OLR Network", here the Board Unique ID here is not important
   * --   and it may be not set on the Board
   *
   *  Open Serial and check if there is a OLR board responding 
   *  to GetSoftwareID/GetSoftwareVersion commands.
   */
  boolean identifyBoard(SerialPort sp) { 
  
    // first call -> Open Port 
    if( this.serialPort == null) {
      t.log(Trace.INTERNALS, String.format("identifyBoard(): Opening Serial port [%s] at [%d]\n", sp.getSystemPortName(),this.boardBrate));

      String err = openSerialPort(sp);  
      if(err != null){  
        t.error(String.format("identifyBoard(): Error opening PORT [%s]\n  |__%s\n", sp.getSystemPortName(), err ));
        return(false);  
      }
      setSerialPort(sp); // set [sp] as the Serial Port for the Phisical Device
      this.isConnected=true; // 
      this.lastPortOpenTime = System.currentTimeMillis(); 
      querySent = false;  // reset
      this.setConfigurationModeDone = false; // reset
      
      return(false);   // will continue on next call
    } 

    // "Wait" until "openToSendDelay" msec expires 
    //    "! querySent && ..." avoid the evaluation of the second part of the condition when openToSendDelay expires
    if (! querySent && (System.currentTimeMillis() < this.lastPortOpenTime + this.openToSendDelay) ) {
      return(false);
    } 

    // Send query
    if (! querySent ) {

      // Send "Enter in configuration mode" command
      if (! this.setConfigurationModeDone) {
        //int done = sendConfirmedEnterCfgModeCmdToBoard();
        String str = String.format("%c%c",ProtocolSerial.Cmd.EnterCfgMode.ID,ProtocolSerial.EOC);
        int done = sendConfirmedCmdToBoard(str); 
        if ( done == 1 ) {
          t.log(Trace.DETAILED, "identifyBoard() -> Board [Configuration Mode] confirmed\n");
          this.setConfigurationModeDone = true;  // Done -> on next call will send the query to board 
        } else if ( done == -1 ) {
          t.log(Trace.DETAILED, String.format("identifyBoard() -> 'Enter Configuration Mode' request return error from board:[-1]\n"));
          return(true); // done (do not continue) 
        }
        return(false); // 0|1 -> continue
      }
 
      // Board confirmed to be in "Configuration Mode"
      querySent = true;
      // Send "GetSoftvareID" and "GetSoftwareVersion" commands
      t.log(Trace.DETAILED, "identifyBoard() -> Sending [GetSoftvareID + GetSoftwareVersion]  commands\n");
      byte cmd1[]={ProtocolSerial.Cmd.GetSoftwareId.ID, ProtocolSerial.EOC, ProtocolSerial.Cmd.GetSoftwareVersion.ID,  ProtocolSerial.EOC};
      sendCmdToBoard(cmd1);

    }


    // Serial "Commands" received from OLRBoard  
    ///////////////////////////////////////////
    if( ! this.fromBoardQ.isEmpty()) {
      String cmd  = this.fromBoardQ.poll() ;
      t.log(Trace.LOWLEVEL, String.format("identifyBoard() -> received from board:[%s]\n", cmd));
      
      char cmdId=cmd.charAt(0);  // first char = "Command.ID"

      // Got "SoftwareId" answer from board
      if(cmdId == ProtocolSerial.Cmd.GetSoftwareId.ID) {
        this.boardSoftwareId = cmd.substring(1);
        t.log(Trace.DETAILED, String.format("identifyBoard(): Found SoftwareId[%s]\n", this.boardSoftwareId));
      }     

      // Got "SoftwareVersion" answer from board
      else if(cmdId == ProtocolSerial.Cmd.GetSoftwareVersion.ID) {
        this.boardSoftwareVer = cmd.substring(1);
        if(this.boardSoftwareId != null) {
          t.log(Trace.DETAILED, String.format("identifyBoard(): Found SoftwareVersion[%s]\n", this.boardSoftwareVer));
          return(true);
        } else {
          t.error( String.format("identifyBoard(): Found SoftwareVersion[%s] but no SoftwareId !!! \n", this.boardSoftwareVer));
        }
      } else {
        t.log(Trace.LOWLEVEL, String.format("identifyBoard(): NOT a GetSoftwareVersion command:[%s] - Discharged\n", cmd));
        t.log(Trace.LOWLEVEL, String.format("identifyBoard(): Board did not 'ear' GetSoftwareId/Version command... Send Again\n"));

        // Board did'nt ear...send again  
            // Send "GetSoftvareID" and "GetSoftwareVersion" commands
            byte cmd1[]={ProtocolSerial.Cmd.GetSoftwareId.ID, ProtocolSerial.EOC, ProtocolSerial.Cmd.GetSoftwareVersion.ID,  ProtocolSerial.EOC};
            sendCmdToBoard(cmd1);        
      }
    } 

    return(false);  
  }



  String [] getBoardSoftware() {
    String ret[] = null;
    if(this.boardSoftwareId != null && this.boardSoftwareVer != null){
      ret = new String[]{this.boardSoftwareId,this.boardSoftwareVer};
    }
    return(ret);
  }

  String getBoardSoftwareId() {
    
    if(this.boardSoftwareId != null){
      return(this.boardSoftwareId);
    }
    return(null);
  }


  String getBoardSoftwareVer() {
    if(this.boardSoftwareVer != null){
      return(this.boardSoftwareVer);
    }
    return(null); 
  }




  /*
   *  Send a Command to board and "listen" for the <CommandId>OK|NOK" acknowledge - Non blocking
   *  Please Note:
   *    call this method only for commands that send back a "<CommandId>OK|NOK" acknowledge
   * @return  0 if expected <CommandId>OK|NOK not yet received  
   * @return  1 if done OK (<CommandId>OK received) 
   * @return -1 if done NOK (<CommandId>NOK received) 
   */
  int sendConfirmedCmdToBoard (String commandString) {
    // on first call -> Send Command
    if (!  this.sendConfirmedCmd_sent ) {
      this.sendConfirmedCmd_sent=true;
      this.sendString(commandString);
      return(0);
    }
    char cmdId=commandString.charAt(0);  // first char = "Command.ID"
    
    int gotIt = gotCommandAnswer(cmdId);
    if( gotIt != 0 ) {
      this.sendConfirmedCmd_sent = false; // reset 
    } 
    return(gotIt);
  }  
  
  /*
   *
   */
  void resetSendConfirmedCmdToBoard(){
    this.sendConfirmedCmd_sent = false; // reset 
  }


  /*
   * DEPRECATED - For previous versions of the serial protocol, with no @ command (R1 was used)
   * ------------------------------------------------------------------------------------------
   *  Send "commandString" to board and look for answer for the specific command - Non blocking
   *  Please Note:
   *    call this method only for commands that send back a "<CommandId>OK|NOK" acknowledge
   *
   * @param   commandString String containing command an parameters 
   *
   * @return  0 if <Enter Configuration Mode>OK|NOK not yet received  
   * @return  1 if done OK (<Enter Configuration Mode>OK received) 
   * @return -1 if done NOK (<Enter Configuration Mode>NOK received) 
   */
  int sendConfirmedEnterCfgModeCmdToBoard_DEPRECATED () {
    String commandString = ProtocolSerial.Cmd.RacePhase.getCommand(ProtocolSerial.Cmd.RacePhase.Phase.ENTERCFG);
    return(sendConfirmed_R_CmdToBoard_DEPRECATED(commandString));
  }
   
  
  /*
   *  Look for answer form board for a specific command (expectedCommand) - Non blocking
   *  Please Note:
   *    call this method only for commands that send back a "<CommandId>OK|NOK" acknowledge
   *
   * @param   expectedCommand command identifier 
   *
   * @return  0 if expected <CommandId>OK|NOK not yet received  
   * @return  1 if done OK (<CommandId>OK received) 
   * @return -1 if done NOK (<CommandId>NOK received) 
   */
  int gotCommandAnswer(char expectedCommand) {

    // Serial "Commands" received from OLRBoard  / waits for confirmation (<CommandId>OK)
    if( ! this.fromBoardQ.isEmpty()) {
      String cmd  = this.fromBoardQ.poll() ;
      t.log(Trace.DETAILED, String.format("gotCommandAnswer() -> received from board:[%s]\n", cmd));
      
      char cmdId=cmd.charAt(0);  // first char = "Command.ID"
      if(cmdId == expectedCommand) {
        int retval;
        String par = cmd.substring(1); // Command parameter string
        if(par.equals(ProtocolSerial.Cmd.OK)){
          t.log(Trace.DETAILED, String.format("gotCommandAnswer() -> DONE:[%s]\n",cmd));
          retval = 1;
        } else {
          t.log(Trace.DETAILED, String.format("gotCommandAnswer() -> ERROR from Board:[%s]\n",cmd));
          retval = -1;
        }
        return(retval); // done   
      } else {
        t.log(Trace.DETAILED, String.format("gotCommandAnswer() -> Not the expected command [%s] -> DISCHARGED\n",cmd));
      } 
    }     
    return(0);
  }    
  
  /* 
   *  Send "R" command to board and look for ROK|RNOK answer 
   *  DISCARDS any other R'x' status received from board 
   * 
   *  Used to send "R1" command (ENTER CFG MODE used in OLRNetwork and 4Players 0.9.5)
   *  ON sending R1 the board may send, before "ROK" other race Status
   *  Example 4Player 0.9.5:
   *    Board get reset (open serial) and send R4...R5 BEFORE process R1 request and answer ROK. 
   *
   * @param   commandString String containing command an parameters 
   *
   * @return  0 if expected <CommandId>OK|NOK not yet received  
   * @return  1 if done OK (<CommandId>OK received) 
   * @return -1 if done NOK (<CommandId>NOK received) 
   */
  int sendConfirmed_R_CmdToBoard_DEPRECATED (String commandString) {  
    // on first call -> Send Command
    if (!  this.sendConfirmed_RCmd_sent ) {
      t.log(Trace.DETAILED, String.format("sendConfirmed_R_CmdToBoard_DEPRECATED() -> Sending:[%s]\n", commandString));
      this.sendConfirmed_RCmd_sent=true;
      this.sendString(commandString);
      return(0);
    }
    char rcmdId=commandString.charAt(0);  // first char = "Command.ID"
    // Serial "Commands" received from OLRBoard  
    // look for a R[OK|NOK]
    if( ! this.fromBoardQ.isEmpty()) {
      String cmd  = this.fromBoardQ.poll() ;
      t.log(Trace.DETAILED, String.format("sendConfirmed_R_CmdToBoard_DEPRECATED() -> received from board:[%s]\n", cmd));
      char cmdId=cmd.charAt(0);  // first char = "Command.ID"
      if(cmdId == rcmdId) {
        int retval;
        String par = cmd.substring(1); // Command parameter string
        if(par.equals(ProtocolSerial.Cmd.OK)){
          t.log(Trace.DETAILED, String.format("sendConfirmed_R_CmdToBoard_DEPRECATED() -> DONE:[%s]\n",cmd));
          this.sendConfirmed_RCmd_sent=false; // reset
          retval = 1;
        } else if(par.equals(ProtocolSerial.Cmd.ERROR)) {
          t.log(Trace.DETAILED, String.format("sendConfirmed_R_CmdToBoard_DEPRECATED() -> ERROR from Board:[%s]\n",cmd));
          this.sendConfirmed_RCmd_sent=false; // reset
          retval = -1;
        } else {
          t.log(Trace.DETAILED, String.format("sendConfirmed_R_CmdToBoard_DEPRECATED() -> Unsolicited R Status [%s] from Board while waiting for [OK|NOK] -> DISCHARGED\n",cmd));
          retval = 0;
        }
        return(retval); // done   
      } else {
        t.log(Trace.DETAILED, String.format("sendConfirmed_R_CmdToBoard_DEPRECATED() -> Not an R command acknowledge [%s] from Board while waiting for [OK|NOK] -> DISCHARGED\n",cmd));
        return(0);
      }
    }     
    return(0);
  }
  
  
   /*
   *
   */
  void cleanupFromBoardQ () {  
    if( ! this.fromBoardQ.isEmpty()) {
      String cmd  = this.fromBoardQ.poll() ;
      t.log(Trace.DETAILED, String.format("cleanupFromBoardQ() -> [%s] DISCHARGED\n", cmd));
    }     
    return;
  }
  
  void ignoreMessagesFromBoard(boolean i){
    this.ignoreMessagesFromBoard=i;
  }
  
  

  /**
   *
   *
   */
   boolean isFirmwareVersionOK(char cmdBuf[], int bytesRead) {

     t.error("isFirmwareVersionOK(): code removed from PhisicalDevice !!!  \n Remove dependencies from req_firmware_version[] OR use Utils.compareSoftwareVersion() in the main app\n");
     return(false);
/**     
    char buf[] = new char[bytesRead];
      for(int i=0;i<bytesRead;i++){
        buf[i]=cmdBuf[i];
      } 
      t.log(Trace.LOWLEVEL, String.format("isFirmwareVersionOK(): got cmdBuf:[%s]\n", new String(buf)));

      if(ProtocolSerial.Cmd.GetSoftwareVersion.ID == (buf[0])) {
        String getVer = String.valueOf(buf).substring(1);
        t.log(Trace.LOWLEVEL, String.format("isFirmwareVersionOK(): got Version:[%s]\n", getVer));
         
        // Check if received Software Version is right 
        int boardVer[]=ProtocolSerial.Cmd.GetSoftwareVersion.getParam(getVer);
        if( req_firmware_version[0] == boardVer[0] &&
              req_firmware_version[1] <= boardVer[1]) {
          return(true);
        }
        return(false);
      } else {
        t.log(Trace.LOWLEVEL, String.format("isFirmwareVersionOK(): NOT a GetSoftwareVersion command:[%s]\n", (char) (buf[0])));
      }
      return(false);
 **/     
  }  
 
 
 
 
  /**
   *  Opens the Phisical Device's Serial port
   *
   *  PLASE NOTE:
   *    On Windows, openPort() take up to 600 mSec 
   *    AND the Arduino Board get RESET even without
   *    call s.clearDTR().
   *  
   *    This means we'll need to adjust:
   *      lookupTimeout=1400;
   *      openToSendDelay=1200;
   *    To let Arduino reset and than send
   *    the query string.
   *
   *  @param port :  OS device port name ("COM[*]" on Windows - "/dev/tty[*]" on Linux)
   *  @param properties : OS port description 
   *  @param baudRate 
   */
    String openSerialPort (SerialPort s, int bRate) {
      String pName = s.getSystemPortName();
      t.log(Trace.LOWLEVEL, String.format("openSerialPort(%s,%d) OPEN AT MILLIS:[%d]\n", pName, bRate, System.currentTimeMillis()));
      try {
        boolean ok=s.openPort(ProtocolSerial.SAFETYSLEEPTIME, ProtocolSerial.DEVICESENDQUEUESIZE, ProtocolSerial.DEVICERECVQUEUESIZE);
        //boolean ok=s.openPort();
        if(! ok){
          return(String.format("openSerialPort() Error opening port:[%s] - Already in use\n", pName));
        }
        s.setComPortTimeouts(SerialPort.TIMEOUT_NONBLOCKING, 0, 0);
        s.setBaudRate(bRate);
        
        // !!! WINDOWS 10 OLNY - Arduino Every BLOCCA UPLOAD via avrdude [Using Programmer:jtag2updi] non risponde!!! 
        //  avrdude: jtagmkII_getsync(): sign-on command: status -1
        // Eliminato ...sembra superfluo per reset anche su Linux
        //s.clearDTR(); 
        
    } catch (SerialPortInvalidPortException ex) {
      return(String.format("openSerialPort() Error opening:[%s]\n\t%s", pName, ex));
    }           
    this.lastHandshakeAttempt = 0 ;  // System.currentTimeMillis()    
    sbufIdx=0;
    t.log(Trace.LOWLEVEL, String.format("openSerialPort(%s,%d) DONE AT MILLIS:[%d]\n", pName, bRate, System.currentTimeMillis()));
    return(null);
  }

  String openSerialPort (SerialPort s) {
    return( openSerialPort(s, this.boardBrate));
  }

  /*
   *
   */
  boolean closeSerialPort(SerialPort s) {
      t.log(Trace.LOWLEVEL, String.format("closeSerialPort(%s):Closing port AT MILLIS:[%d]\n", s.getSystemPortName() ,System.currentTimeMillis()));
      boolean ret = s.closePort();
      t.log(Trace.LOWLEVEL, String.format("closeSerialPort(%s):Port closed AT MILLIS:[%d]\n", s.getSystemPortName() ,System.currentTimeMillis()));
      return(ret);
  }

  String closeCurrentSerialPort() {
      boolean ok=closeSerialPort(this.serialPort);
      this.serialPort = null;
      this.port = null;
      this.portProperties = null;
      if(! ok) return("closeCurrentSerialPort()->closePort() returned [false]");
      return(null); 
  }

  /*
   * Open->close port to force board reset
   */
  String force1200bpsReset(SerialPort sp){
      t.log(Trace.BASE, String.format("force1200bpsReset():Forcing reset using open1200bps/close on port [%s]\n", sp.getSystemPortName()));
      
      // Open port at 1200 bps
      String ret = openSerialPort (sp, 1200);
      if(ret != null) {
        return("force1200bpsReset() Error opening port at 1200 bps:" + ret);
      }
      // Close port
      boolean ok = closeSerialPort(sp);
      if(! ok) return("force1200bpsReset()->closePort() returned [false]");      
      
      return(null);
  }

  /* Non-blocking (to be called in a loop)
   *
   * For Arduino Every
   *   Forcing reset using 1200bps open/close on port
   *
   * @return  0 if reset not yet completed   
   * @return  1 if done 
   * @return -1 on error 
   */
  int closeCurrentSerialPort_force1200bpsReset(){
    
    // first call - Start timer, close+open port
    if(this.reset1200PortOpenTime == 0) {  
      t.log(Trace.BASE, String.format("closeCurrentSerialPort_force1200bpsReset(): Forcing reset using 1200bps open/close on port [%s] - Started at[%d]\n", this.serialPort.getSystemPortName(),System.currentTimeMillis()));
      this.reset1200PortOpenTime = System.currentTimeMillis();
      boolean ok = closeSerialPort(this.serialPort);
      if(! ok) {
        t.error("closeCurrentSerialPort_force1200bpsReset()->First closePort() returned [false]");
        this.reset1200PortOpenTime = 0; // reset
        return(-1);
      }
      // Open at 1200 bps
      String ret = openSerialPort (this.serialPort, 1200);
      if(ret != null) {
        t.error(String.format("closeCurrentSerialPort_force1200bpsReset()->OpenPort() error -> [%s]",ret));
        this.reset1200PortOpenTime = 0; // reset
        return(-1);
      }
      return(0);
    }
    
    // Timeout between Open and Close (uses "openToSendDelay" value)
    if ( System.currentTimeMillis() < this.reset1200PortOpenTime + this.openToSendDelay ) {
      return(0);
    } 

    t.log(Trace.BASE, String.format("closeCurrentSerialPort_force1200bpsReset(): Closing port at[%d]\n", System.currentTimeMillis()));
    boolean ok = closeSerialPort(this.serialPort);
    if(! ok) {
      t.error("closeCurrentSerialPort_force1200bpsReset()->Last closePort() returned [false]");
      this.reset1200PortOpenTime = 0; // reset
      return(-1);
    }
    
    this.serialPort = null;
    this.port = null;
    this.portProperties = null;    

    t.log(Trace.BASE, String.format("closeCurrentSerialPort_force1200bpsReset(): Done at[%d]\n", System.currentTimeMillis()));

    this.reset1200PortOpenTime = 0; // reset
    return(1);

/***    
      boolean ok = closeSerialPort(this.serialPort);
      if(! ok) return("closeCurrentSerialPort_force1200bpsReset()->First closePort() returned [false]");
      
      String ret = openSerialPort (this.serialPort, 1200);
      if(ret != null) {
        return("closeCurrentSerialPort_force1200bpsReset()." + ret);
      }
      ok = closeSerialPort(this.serialPort);
      if(! ok) return("closeCurrentSerialPort_force1200bpsReset()->Second closePort() returned [false]");      
      
      this.serialPort = null;
      this.port = null;
      this.portProperties = null;    
      return(null);
      
***/      
  }
  
  void  setSerialPort(SerialPort p){
          this.serialPort = p;
          this.port = serialPort.getSystemPortName();
          this.portProperties = serialPort.getDescriptivePortName();
  }

  boolean isConnected(){
    return(this.isConnected);
  }

  boolean isPortValid(){
    return(this.isPortValid);
  }


  /**
   * connect() - Handshake with OLR Board 
   */
  void connect() {

    if(lastHandshakeAttempt==0 || ( System.currentTimeMillis() > lastHandshakeAttempt + this.handshakeTimeout)) {  
      this.lastHandshakeAttempt = System.currentTimeMillis();
      try { // Send HANDSHAKE to Board
      //byte cmd[] = {ProtocolSerial.Cmd.Reset.ID,ProtocolSerial.EOC,ProtocolSerial.Cmd.Handshake.ID,ProtocolSerial.EOC};
      byte cmd[] = {ProtocolSerial.Cmd.Handshake.ID,ProtocolSerial.EOC};
        serialPort.writeBytes(cmd,cmd.length);
        t.log(Trace.DETAILED, String.format("connect(): Handshake sent to port:[%s]\n", this.port));
      } catch (Exception e) { e.printStackTrace(); }
    }
    
    if(serialPort.bytesAvailable() > 0) {
      byte[] in=new byte[1];
      int nb = serialPort.readBytes(in,1); // read 1 byte
      if(nb > 0 && in[0] == ProtocolSerial.Cmd.Handshake.ID) {
        t.log(Trace.LOWLEVEL, String.format("connect(): Port[%s]->Received HANDSHAKE [%c]\n", this.port, (char)in[0]));
        byte[] in2=new byte[1];
        nb = serialPort.readBytes(in2,1); // read 1 byte
        t.log(Trace.LOWLEVEL, String.format("connect(): Port:[%s]->Received char [%c]\n", this.port, (char)in2[0]));
        if(nb > 0 && in2[0] == ProtocolSerial.EOC) {
          // ??
          byte cmd[] = {ProtocolSerial.Cmd.Handshake.ID,ProtocolSerial.EOC};
          serialPort.writeBytes(cmd,cmd.length);
          
          // Provvisorio per vedere se rientra in cfg al riaccendere il prg processing e non arduino...
          byte cmd2[] = {ProtocolSerial.Cmd.RacePhase.ID,(byte)ProtocolSerial.Cmd.RacePhase.Phase.ENTERCFG.charAt(0),ProtocolSerial.EOC};
          serialPort.writeBytes(cmd2,cmd2.length);
          
          isConnected=true;
          t.log(Trace.BASE, String.format("connect(): CONNECTED TO BOARD on port:[%s]\n", this.port));
        }
      } else {
        ;t.log(Trace.LOWLEVEL, String.format("connect(): Port:[%s]->Received unexpected char [%c] in Handshake phase\n", this.port, (char)in[0]));
      }
    }
  }

  
  /**
   *  get Car [idx] from carList 
   */
  int getCarIdx(int carId) {
    for (int i=0; i<carList.length ; i++) {
      if( carList[i].id == carId){
        return(i);
      }
    }
    return(-1);
  }
  
  /**
   *  set Current Device for Car [idx] in carList
   *    (which OLRDevice contains the car now)  
   */  
  String setCarCurrentDevice(int carId, String dev, String s) {
    int idx=getCarIdx(carId);
    if(idx==-1){
      return(String.format("setCarCurrentDevice(): unknown carId:[%d]",carId));
    }
    carList[idx].curDev=dev;
    carList[idx].status=s;
    return(null);
  }

  /**
   *  get Current Device for Car [idx] in carList
   *    (which OLRDevice contains the car now)  
   */  
  String getCarCurrentDevice(int carId) {
    int idx=getCarIdx(carId);
    if(idx==-1){
      return(null);
    }
    return(carList[idx].curDev);
  }
  
  /**
   *  get Car Object at carList[idx] 
   */    
  Car getCar(int carId){
    int idx=getCarIdx(carId);
    if(idx==-1){
      return(null);
    }
    return(carList[idx]);
  }
 
 
  protected void emptyReadBuf(SerialPort sp) {
    if(sp.bytesAvailable() > 0) {
      byte[] readBuffer = new byte[sp.bytesAvailable()];
      int numRead = sp.readBytes(readBuffer, readBuffer.length);
      t.log(Trace.INTERNALS, String.format("emptyReadBuf(): port[%s]->discharged [%d] input bytes:[%s]\n",sp.getSystemPortName(), numRead, new String(readBuffer)));
    }    
  }

  protected void emptyReadBuf() {
    emptyReadBuf(this.serialPort);
  }
 
 
  ///////////////////////////////////////////////////////////////////////////
  ///////////  Methods actually writing to/reading from Serial //////////////
  ///////////////////////////////////////////////////////////////////////////
  /**
   *  Write a string in the Serial Port 
   */
  protected void sendString(String str) {
    if(  ! isConnected){
      t.error(String.format("sendString() SOFTWARE ERROR: NOT connected to a port\n" ));
      return;
    }
    byte b[] = str.getBytes();
    try {
    //  byte cmd[] = cmdStr.toString().getBytes();
      serialPort.writeBytes(b,b.length);
      t.log(Trace.LOWLEVEL, String.format("sendString(): [%s] sent to port [%s] \n", new String(b), this.port));
    } catch (Exception e) { e.printStackTrace(); }
  }

  /**
   *
   */
  void sendCmdToBoard (SerialPort sp, byte cmd0[]) {  
    try {
      // Send command  
      sp.writeBytes(cmd0,cmd0.length);
      t.log(Trace.LOWLEVEL, String.format("sendCmdToBoard(): WRITE [%s] on [%s]\n", new String(cmd0),sp.getSystemPortName()  ));

    } catch (Exception e) { e.printStackTrace(); }
    return;
  }

  void sendCmdToBoard (byte cmd0[]) {  
    sendCmdToBoard (this.serialPort, cmd0);
  }
 
  /**
   * Read a "Command" from the Serial Port and
   * write it into buf[]
   * Commands are defined in ProtocolSerial and are always
   * strings where the FIRST char is the CommandId and
   * the following chars, UP TO "End Of Command" char
   * represents the parameters (if any)
   *
   * @param   buf[] buffer where command characters get stored
   * @param   sp SerialPort to read from
   *
   * @return  the Number of valid chars written into buf[]
   * @return  0 if no complete commad is available at the moment
   * @return -1 on buffer size error
   */
  protected int readCmd(char buf[], SerialPort sp) {
    
    if(sp.bytesAvailable() > 0) {
      if(sbufIdx < READBUFFERSIZE - 2) {
         byte[] in=new byte[1];
         int nb = sp.readBytes(in,1); // read 1 byte
         
         if(sbufIdx==0) { // first byte: Command Id
           sbufBitEncoded=false;
           if(in[0] == ProtocolSerial.Cmd.CarLeave.ID) { // Car Leave: 2nd byte is bit-encoded
              sbufBitEncoded=true;
           }
         }
         
         if(nb > 0 && in[0] == ProtocolSerial.EOC) {
             int readCmdSize=sbufIdx;
             sbufIdx=0;
             for(int i=0;i<readCmdSize;i++){
               buf[i]=serialBuffer[i];
             }
             if(sbufBitEncoded) {
               t.log(Trace.LOWLEVEL, String.format("readCmd(): port:[%s]->Received [EOC]->buf[%c][%s][EOC] (size:%d)\n\n", sp.getSystemPortName(), serialBuffer[0], Integer.toBinaryString(Integer.valueOf((int)serialBuffer[1])) ,readCmdSize));
             } else {
               t.log(Trace.LOWLEVEL, String.format("readCmd(): port:[%s]->Received [EOC]->buf[%s] (size:%d)\n\n", sp.getSystemPortName(), new String(serialBuffer,0,readCmdSize),readCmdSize));               
             }
             return(readCmdSize);
         } else {
             if(sbufBitEncoded) {
               t.log(Trace.LOWLEVEL, String.format("readCmd(): port:[%s]->Received [%s] add to buffer[%d]\n", sp.getSystemPortName(), Integer.toBinaryString(Integer.valueOf((char) in[0])), sbufIdx));
             } else {
               t.log(Trace.LOWLEVEL, String.format("readCmd(): port:[%s]->Received [%c] add to buffer[%d]\n", sp.getSystemPortName(), (char) in[0],sbufIdx));   
             }
             serialBuffer[sbufIdx++] = (char) in[0];
             return(0);
         }
      }else {
        t.error(String.format("readCmd():Command String length=[%d] > READBUFFERSIZE=[%d]\n", sbufIdx, READBUFFERSIZE));
        return(-1);
      }
    }
    return(0);
  }  
  
  /**
   * calls readCmd on the PhysicalDevice serial port   
   */
  protected int readCmd(char buf[]){
    return(readCmd(buf, this.serialPort));
  } 
 
 
 
 
 
 
} // PhysicalDevice
