

/*
 *
 */
class SoftwareList  {

  /*
   * Inner Class "Software"
   * Fully describe a specific Software item identified by (ID + VERSION + PLATFORM).
   * In cfg files informations are spread in 3 different JSON objects
   * contained in SoftwareList (Software -> Version -> Platform)
   *
   * Used by methods to 'return' the complete software content 
   */
  class Software  {
    int        accuracy     ; // 0: Software Id NOT FOUND, 1: found [Software.id], 2: found [Software.id + Version.id], 3: found [Software.id + Version.id + Platform.id] 
    String     id           ;
    String     version      ;
    String     platform     ;
    String     name         ;
    String     description[]; 
    String     minOlrManagerVersion;
    String     maxOlrManagerVersion;
    String     hexfile      ;
    JSONObject vCfgparams    ; // extra cfg params (version-specific)
    JSONObject pCfgparams    ; // extra cfg params (platform-specific)
    String     url;

    // Constructor
    Software() {
        this.accuracy     = 0;
        this.id           = new String ("Unknown");
        this.name         = null;
        this.version      = new String ("Unknown");
        this.description  = null;
        this.minOlrManagerVersion = null;
        this.maxOlrManagerVersion = null;
        this.platform     = null;
        this.hexfile      = null;
        this.vCfgparams   = null;
        this.pCfgparams   = null;
        this.url          = null;
    }   
    // Constructor
    Software(int _accuracy, String _id, String _version, String _platform, String _name, String [] _description, String _minOlrManagerVersion , String _maxOlrManagerVersion ,String _hexfile, JSONObject _vCfgparams, JSONObject _pCfgparams, String _url) {
        this.accuracy    = _accuracy;
        this.id          = _id;
        this.name        = _name;
        this.version     = _version;
        this.description = _description;    
        this.minOlrManagerVersion = _minOlrManagerVersion;
        this.maxOlrManagerVersion = _maxOlrManagerVersion;
        this.platform    = _platform;
        this.hexfile     = _hexfile;
        this.vCfgparams  = _vCfgparams;
        this.pCfgparams  = _pCfgparams;      
        this.url         = _url;
    }       
    
    /*
     *
     */
    String toString(){
      String ret = String.format("Software Object: {\n accuracy[%d]\n id [%s]\n version [%s]\n platform [%s]\n name [%s]\n hexfile [%s]\n description [%s]\n minOlrManagerVersion [%s]\n maxOlrManagerVersion [%s]\n vCfgparams [%s]\n pCfgparams [%s]\n url [%s]\n}\n",
        this.accuracy,
        this.id,
        this.version,
        this.platform,
        this.name,
        this.hexfile,
        Arrays.toString(this.description),
        this.minOlrManagerVersion,
        this.maxOlrManagerVersion,        
        this.vCfgparams != null ? this.vCfgparams.toString() : "",
        this.pCfgparams != null ? this.pCfgparams.toString() : "",
        this.url
      );
      
      return(ret);
    }
  } // Inner class "Software"  
  
  JSONArray     jsonSList=null;
  int           sNum=0;
    
  private Trace t;
  private static final String _CLASS_ = "SoftwareList";
  
  // Constructor    
  SoftwareList(String sl_fname, String logStr) {
    // Set loglevel
    if(logStr.equals("-") ){
      t = new Trace(Trace.NONE);
    } else {
      t = new Trace(logStr);
    }      
    t.setClassId(_CLASS_);
    t.log(Trace.METHOD, String.format("Constructor(Logstring:%s)\n",logStr));
    
    JSONObject jsonSoftwareList=null;
    
    // Load JSON object form cfg file
    try {
      jsonSoftwareList = loadJSONObject(sl_fname);
    } catch (RuntimeException e) {  
      t.error(String.format("Error loading SoftwareList cfg file:[%s] - [%s]\n ", sl_fname, e));
      noLoop();
      exit();
    }
    this.jsonSList = jsonSoftwareList.getJSONArray("SoftwareList");
    if(this.jsonSList == null){
       t.error(String.format(String.format("Error! Json cfg file - Missing \"SoftwareList\" section.")));
       exit();
    }
    t.log(Trace.LOWLEVEL, String.format("Constructor() - Loaded JSON cfg file[%s]:\n%s\n",sl_fname,this.jsonSList.toString()));

    this.sNum = this.jsonSList.size();
  }
  
  
  // Destructor
  protected void finalize() {
    t.log(Trace.METHOD, String.format("Destructor()\n"));    
    this.jsonSList=null;
  }

 
   /*
   *
   */
  public Software getSoftware(String _platform)  {  
    Software    item = new Software();
    item.platform = _platform;
    return(item);
  }

  /*
   *
   */
  public Software getSoftware(String _id, String _version, String _platform)  {  
    Software    item = new Software();
    JSONArray   mainDescArr = null;
    JSONArray   verDescArr  = null;
    JSONArray   plaDescArr  = null;

    // Software Id lookup
    JSONObject jsw = idLookup(jsonSList, _id);
    if(jsw != null) {
        item.accuracy=1;
        item.id = _id;
        item.name  = getJSONStringOrExit(jsw, "name");
        mainDescArr = getJSONArrayOrExit(jsw, "description");
        t.log(Trace.LOWLEVEL, String.format("getSoftware(%s,%s,%s)-> Found id=[%s] -> name[%s]\n", _id,_version,_platform,_id, item.name));

        // Version lookup
        JSONArray verArr = getJSONArrayOrExit(jsw, "version");
        JSONObject swv = idLookup(verArr, _version);
        if(swv != null) {
          item.accuracy=2;
          item.version  = getJSONStringOrExit(swv, "id");
          t.log(Trace.LOWLEVEL, String.format("getSoftware()->    Found version=[%s]\n",_version));
          verDescArr = getJSONArrayOrExit(swv, "description");   
          item.minOlrManagerVersion = getJSONStringOrExit(swv, "minOlrManagerVersion");
          item.maxOlrManagerVersion = getJSONStringOrExit(swv, "maxOlrManagerVersion");   
          //item.vCfgparams = swv.getJSONObject("cfgparam"); // version-specific "cfgparam"
          item.vCfgparams = getJSONObjectOrExit(swv, ("cfgparam")); // version-specific "cfgparam"
          
          // Platform lookup
          JSONArray plaArr = getJSONArrayOrExit(swv, "platform");
          JSONObject plv = idLookup(plaArr, _platform);
          if(plv != null) {
            item.accuracy=3;
            item.platform  = getJSONStringOrExit(plv, "id");
            t.log(Trace.LOWLEVEL, String.format("getSoftware()->    Found platform=[%s]\n",_platform));
            plaDescArr = getJSONArrayOrExit(plv, "description");
            item.hexfile  = getJSONStringOrExit(plv, "hexfile");
            //item.pCfgparams = plv.getJSONObject("cfgparam"); // platform-specific "cfgparam"
            item.pCfgparams = getJSONObjectOrExit(plv, ("cfgparam")); // platform-specific "cfgparam"
            item.url  = getJSONStringOrExit(plv, "url");
          }
        }
      }
    item.description = mergeSoftwareDescriptions(mainDescArr, verDescArr, plaDescArr);
    return(item);
  }



  /*
   *
   */
  public ArrayList<Software> getList(String _platform, boolean uploadableOnly)  {
     
    ArrayList<Software> sList = new ArrayList<Software>();

    JSONArray   mainDescArr = null;
    JSONArray   verDescArr  = null;
    JSONArray   plaDescArr  = null;  
    
    // Software 
    for (int i = 0; i < this.sNum; i++) {
      JSONObject jsw = jsonSList.getJSONObject(i); // Software[i]
      String id    = getJSONStringOrExit(jsw, "id");
      String name  = getJSONStringOrExit(jsw, "name");
      mainDescArr  = getJSONArrayOrExit(jsw, "description");
      t.log(Trace.LOWLEVEL, String.format("getList()-> Found Software:id[%s] name[%s]\n",id, name));    

      // Version 
      JSONArray verArr = getJSONArrayOrExit(jsw, "version");
      int vnum = verArr.size();
      for (int j = 0; j < vnum; j++) {
        JSONObject jve = verArr.getJSONObject(j); // Version[j]
        String version  = getJSONStringOrExit(jve, "id");
        verDescArr = getJSONArrayOrExit(jve, "description");   
        String minOlrManagerVer = getJSONStringOrExit(jve, "minOlrManagerVersion");
        String maxOlrManagerVer = getJSONStringOrExit(jve, "maxOlrManagerVersion");  
        //JSONObject vCfgparams = jve.getJSONObject("cfgparam"); // version-specific "cfgparam"
        JSONObject vCfgparams = getJSONObjectOrExit(jve, "cfgparam"); // version-specific "cfgparam"
        t.log(Trace.LOWLEVEL, String.format("getList()-> Found version:[%s]\n", version));   

        // platform 
        JSONArray plaArr = getJSONArrayOrExit(jve, "platform");
        int pnum = plaArr.size();
        for (int k = 0; k < pnum; k++) {
          JSONObject plv = plaArr.getJSONObject(k); // Platform[k]
          plaDescArr = getJSONArrayOrExit(plv, "description");
          String platform  = getJSONStringOrExit(plv, "id");
          String hexfile  = getJSONStringOrExit(plv, "hexfile");
          String url  = getJSONStringOrExit(plv, "url");
          JSONObject pCfgparams = getJSONObjectOrExit(plv, "cfgparam"); // platform-specific "cfgparam"
          t.log(Trace.DETAILED, String.format("getList()-> Found platform:[%s]\n", platform));   

          
          if((_platform == null || _platform.length() == 0) | platform.equals(_platform.trim())){
            if( (! uploadableOnly) || (uploadableOnly && !(hexfile == null || hexfile.trim().isEmpty())) ) {
              String [] description = mergeSoftwareDescriptions(mainDescArr, verDescArr, plaDescArr);
              Software  item = new Software(3, id, version, platform, name, description, minOlrManagerVer, maxOlrManagerVer, hexfile, vCfgparams, pCfgparams, url);
              sList.add(item); 
            }
          } 
        }
      }
    }
    
    return(sList);
  }
  
   /**
   * get List Content 
   *
   * @return String[n][6] [n]:Items / [7]:{id, name, version, platform, hexfile, description[0], url}
   */
  String[][] getContent(String _platform, boolean uploadableOnly){
    
    ArrayList<Software> softwareList = getList(_platform, uploadableOnly) ;
    int n = softwareList.size();
    
    String[][] l = new String[n][7];
    for (int i=0; i<n ; i++) {
      l[i][0]=softwareList.get(i).id.trim();  
      l[i][1]=softwareList.get(i).name.trim();
      l[i][2]=softwareList.get(i).version.trim();
      l[i][3]=softwareList.get(i).platform.trim();
      l[i][4]=softwareList.get(i).hexfile.trim();
      l[i][5]=softwareList.get(i).description[0].trim();
      l[i][6]=softwareList.get(i).url.trim();
    }
    return(l);
  }
  
  String[][] getContent(String _platform){
    return(getContent(_platform, false));
  }
  
  String[][] getUploadableContent(String _platform){
    return(getContent(_platform, true));
  }


  /*
   *
   */
  String [] getFullDescription(int idx, String _platform, boolean uploadableOnly){
      ArrayList<Software> softwareList = getList(_platform, uploadableOnly) ;
      int n = softwareList.size(); 
      if( idx < 0 || idx >= n  ){    // Invalid index 
          t.error(String.format("getFullDescription(): idx[%d] Out of range\n",idx));    
          return(null);
      }  
      return(softwareList.get(idx).description);
  }
  String [] getFullDescription(int idx, String _platform){
    return(getFullDescription(idx, _platform, false));

  }
  String [] getUploadableFullDescription(int idx, String _platform){
    return(getFullDescription(idx, _platform, true));
  }


  /* 
   * Search in a JSONArray the object with "id" field with value _id
   */
  public JSONObject idLookup(JSONArray objArr, String _id)  {
    int aSize=objArr.size();
    for (int i = 0; i < aSize; i++) {
      JSONObject jobj = objArr.getJSONObject(i); // objArr[i]
      String objid    = getJSONStringOrExit(jobj, "id");
      if(objid.equals(_id.trim())){
        return(jobj);          
      } 
    } 
    return(null);
  }
  
  
  /*
   *
   */
  String [] mergeSoftwareDescriptions(JSONArray mainDescArr, JSONArray verDescArr, JSONArray plaDescArr){
      String [] newArr=null;
      int mN,vN, pN;
      if(mainDescArr==null)  mN=0;
      else                   mN = mainDescArr.size();      
      if(verDescArr==null)   vN=0;
      else                   vN = verDescArr.size();
      if(plaDescArr==null)   pN=0;
      else                   pN = plaDescArr.size();
            
      newArr = new String[mN+vN+pN];
      int y=0;
      for(int i=0; i<mN;i++)  newArr[y++]=mainDescArr.getString(i);
      for(int i=0; i<vN;i++)  newArr[y++]=verDescArr.getString(i); 
      for(int i=0; i<pN;i++)  newArr[y++]=plaDescArr.getString(i); 
      return(newArr);
  }



  
  /*
   *
   */
  String toString(String _platform, boolean uploadableOnly){
    StringBuilder sb = new StringBuilder();
    
    ArrayList<Software> softwareList = getList(_platform, uploadableOnly) ;
    int n = softwareList.size();
    for (int i=0; i<n ; i++) {
      sb.append(softwareList.get(i).toString());    
    }  
    return(sb.toString());
  } 
  
  String[][] toString(String _platform){
    return(getContent(_platform, false));
  }
 

}
