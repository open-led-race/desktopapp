/**
 *   
 */
import java.util.HashMap;
import java.util.Map;
static class CodeList {

  private final Map<String, Item> BY_CODE = new HashMap<String, Item>();
  private final Map<String, Item> BY_NAME = new HashMap<String, Item>();
  
  public class Item {
    String code;
    String name;
    int  order;
    Item(String c, String n, int o){
      this.code=c;
      this.name = n;
      this.order = o;
    }
  }
  
  boolean add(String code, String name, int ord){    
    BY_CODE.put(code, new Item(code,name,ord));
    BY_NAME.put(name, new Item(code,name,ord));
    return(true);
  }
  
  Item byCode(String code){
    return BY_CODE.getOrDefault(code,new Item(null,"invalid Code",-1));
  }
  Item byName(String name){
    return BY_NAME.getOrDefault(name,new Item(null,"invalid Name",0));
  }
  
  String getName(String code) {
    Item item = byCode(code);
    return(item.name);
  }
  String decode(String code) { // alias
    return(getName(code));
  }
  
  String getCode(String name) {
    Item item = byName(name);
    if(item.code == null) {
      return(item.name);
    }
    return(item.code);
  }

  String validateCode(String code) {
    Item item = byCode(code);
    return(item.code);
  }
  
  int getOrder(String code) {
    Item item = byCode(code);
    return(item.order);
  }

  String dumpByCode(){
    String s="";
    for (String key : BY_CODE.keySet()) {
      Item item = BY_CODE.get(key);
      s = String.format("%sKey[%s]: code[%s] name[%s] val[%d]\n",s,key, item.code, item.name, item.order);
    }
    return(s);
  }

  String dumpByName(){
    String s="";
    for (String key : BY_NAME.keySet()) {
      Item item = BY_NAME.get(key);
      s = String.format("%sKey[%s]: code[%s] name[%s] val[%d]\n",s,key, item.code, item.name, item.order);
    }
    return(s);
  }

  String toString(){
    String s= "byCode:\n" + dumpByCode() + "byName:\n" + dumpByName(); 
    return(s);
  }

}
