/*********************************************************
 
 UI Components  
 =============
 
 This source file contains
 -------------------------
 - Declaration of Global Variables related to UI components
 GDroplist, ScrollableGrid, GWindow, .... objects
 
 - Settings for the UI components
 Position, Size, Color, etc
 
 - createUI() 
 Called in setup() to instantiate UI objects
 
 *********************************************************/

import java.awt.Rectangle;
import java.awt.Font;

// Main Sketch windos size
void settings() {
  size (800, 700,P2D);
}


///////////////////////////////////
// Global Vars related to the UI //
///////////////////////////////////

GIcon infoIconFull;

GLabel         yourBoard_label; 

// Local Client Selection Dropdown List
// To choose between OLR devices when MORE than one is connected
GDropList lcDroplist;
GLabel    lcDroplist_label;
GLabel    lcSelectedPort_label;
GLabel    lcBoardInfo_label;
GLabel    lcBoardInfo_text;
Rectangle lcRect   = null;  


// User Options (Description, Configure Board, Upload Software)
GOption        g_optDescribe, g_optConfig, g_optUpload;
Rectangle      g_optDescribe_r, g_optConfig_r, g_optUpload_r;  // Rectangles around the selected option

GToggleGroup   g_tgOptions;
GGroup         g_grpOpt;
 

// Board Software Description
GTextArea g_boardDescr;
 

// Board Software Configuration Panel
GPanel         pnlCfg_4Pv1;    // Panel
Rectangle      pnlCfg_4Pv1_r;  // Panel Area


GTabManager    expertModeTab;

// Board Configuration Panel Fields
GCustomSlider    g_laps;           // Laps in a single Race
GLabel           g_laps_label; 
GLabel           g_laps_infotext;
GLabel           g_laps_helpText;
GControlPalette  g_laps_help;

GCheckbox        g_autostart_AlwaysOn;
GLabel           g_autostart_helpText;
GControlPalette  g_autostart_help;

GCheckbox        g_player3_AlwaysOn;
GLabel           g_player3_helpText;
GControlPalette  g_player3_help;

GCheckbox        g_player4_AlwaysOn;
GLabel           g_player4_helpText;
GControlPalette  g_player4_help;

GCheckbox        g_ExpertMode_on;

GCustomSlider    g_stripN;         // LED strips in the Racetrack  
GLabel           g_stripN_label; 
GLabel           g_stripN_infotext; 
int              g_singleStripLEDNumber = 300;
GLabel           g_stripN_helpText;
GControlPalette  g_stripN_help;

GLabel           g_stripN_expert_helpText;
GControlPalette  g_stripN_expert_help;
GTextField       g_stripN_expert_ledN;


GCustomSlider    g_boxl;           // Box (Pitlane) length
GLabel           g_boxl_label; 
GCheckbox        g_boxl_DisplayPitlane;
GLabel           g_boxl_helpText;
GControlPalette  g_boxl_help;
GCheckbox        g_boxl_AlwaysOn;
GLabel           g_boxlAO_helpText;
GControlPalette  g_boxlAO_help;

GTextField       g_boxl_expert;

GCustomSlider    g_slopeC;         // Slope Center
GLabel           g_slopeC_label; 
GLabel           g_slopeC_helpText;
GControlPalette  g_slopeC_help;

GTextField       g_slopeS_expert, g_slopeC_expert, g_slopeE_expert;
GLabel           g_slopeC_expert_helpText;
GControlPalette  g_slopeC_expert_help;

GCustomSlider    g_slopeH;         // Slope Height
GLabel           g_slopeH_label; 

GTextField       g_slopeH_expert;

GCheckbox        g_slope_DisplaySlope;
GLabel           g_slopeH_helpText;
GControlPalette  g_slopeH_help;
GCheckbox        g_slope_AlwaysOn;
GLabel           g_slopeAO_helpText;
GControlPalette  g_slopeAO_help;


GLabel           g_battery_label;
GCheckbox        g_battery_On;
GLabel           g_battery_helpText;
GControlPalette  g_battery_help;
GTextField       g_batteryD_expert, g_batteryM_expert, g_batteryB_expert;
GLabel           g_battery_expert_helpText;
GControlPalette  g_battery_expert_help;


GLabel           g_demo_label;
GCheckbox        g_demo_On;
GLabel           g_demo_helpText;
GControlPalette  g_demo_help;


GButton        g_btnSave;
GButton        g_btnCancel;

GLabel         g_user_infotext; 

GButton        g_upgradeAvailable;

//StripShape   stp;
GView        stripView;



// Upload software to Board Panel
ScrollableGrid  g_upListArea   = null;  // Race List Display Object
Rectangle       g_upListRect   = null;  
GTextArea       g_upListdescr  = null;


// Please Wait animation and user message area
Mover         g_pleaseWait;
Rectangle     g_pleaseWait_r;  
GButton       g_pleaseWait_OkButton;




/*********************************************************
 Function called in the setup() CREATE UI objects
 *********************************************************/
public void createUI() {

  g_t.log(Trace.METHOD, String.format("createUI()\n"));  

  G4P.messagesEnabled(false); // false -> to use control-specific callback
  // Load index 8 (line 9) of the modifies palette in ./data/user_gui_palette.png
  //   see: http://www.lagers.org.uk/g4p/guides/g04-colorschemes.html
  G4P.setGlobalColorScheme(8); 

  G4P.setDisplayFont("Noto Sans", Font.PLAIN, 12); 
  G4P.setInputFont("Noto Sans", Font.PLAIN, 14); 
  // To change font of a specific control: 
  //    <control>.setFont(new Font("Monospaced", Font.PLAIN, 14));

  infoIconFull = new GIcon(this, "info2b.png", 1, 1);

  // Top part of the UI, always visible
  // Serial Port choice, Software found, Available Actions
  createBoardActionsUI(); 
  
  // Uploead Software  
  createUploadUI();  

  // 4 Players Software configuration 
  create4PlayersConfigUI();  
  
  // Please wait area/animation
  createPleaseWaitArea();
  
  G4P.setMouseOverEnabled(true);  
  
}


/*
 *
 */
public void createBoardActionsUI() {

  // Title for Your Board area
  yourBoard_label = new GLabel(this, 20, 2, 180, 30);
  yourBoard_label.setTextAlign(GAlign.LEFT, GAlign.TOP);
  yourBoard_label.setText("Your Board:");
  yourBoard_label.setTextBold();
  
  // Serial Port/Board Droplist - Active if 0 or more than 1 Serials found
  ////////////////////////////////////////////////////////////////////////
  // Label
  lcDroplist_label = new GLabel(this, 30, 25, 140, 30);
  lcDroplist_label.setTextAlign(GAlign.RIGHT, GAlign.TOP);
  lcDroplist_label.setText("Connected to:");
  lcDroplist_label.setTextBold();
  // Droplist
  lcDroplist = new GDropList(this, 175, 28, 600, 300, 15, 40);
  lcDroplist.setLocalColorScheme(GCScheme.ORANGE_SCHEME);
  // lcDroplist.setItems() done in findPort() on changes of Serial List 


  // Selected Serial Port/Board (substitute Droplist when one is selected)
  ////////////////////////////////////////////////////////////////////////
  lcSelectedPort_label = new GLabel(this, 175, 25, 620, 40);
  lcSelectedPort_label.setTextAlign(GAlign.LEFT, GAlign.TOP);
  lcSelectedPort_label.setTextBold();
  lcSelectedPort_label.setOpaque(false);
  lcSelectedPort_label.setVisible(false);


  // Board Software
  /////////////////
  // Label  
  lcBoardInfo_label = new GLabel(this, 30, 48, 140, 30);
  lcBoardInfo_label.setTextAlign(GAlign.RIGHT, GAlign.TOP);
  lcBoardInfo_label.setText("Current Software:");
  lcBoardInfo_label.setTextBold();
  lcBoardInfo_label.setOpaque(false);
  lcBoardInfo_label.setVisible(false);
  // Text 
  lcBoardInfo_text = new GLabel(this, 175, 48, 620, 30);
  lcBoardInfo_text.setTextAlign(GAlign.LEFT, GAlign.TOP);
  lcBoardInfo_text.setTextBold();
  lcBoardInfo_text.setOpaque(false);
  lcBoardInfo_text.setVisible(false);
  
  // Frame for "Your Board" display area
  lcRect   = new Rectangle(20, 22, 770, 60); 


  // Available Options for the Board (Configure software, Upload new software)
  ////////////////////////////////////////////////////////////////////////////
  g_optDescribe = new GOption(this, 30, 90, 160, 20, "Software Description");
  g_optDescribe_r = new Rectangle(20, 82, 180, 30); // visual frame when option is selected
g_optDescribe.addEventHandler(this, "g_optDescribe_option");

  g_optUpload = new GOption(this, 220, 90, 170, 20, "Upload new Software");
  g_optUpload_r = new Rectangle(200, 82, 200, 30); // visual frame when option is selected
g_optUpload.addEventHandler(this, "g_optUpload_option");
  
  g_optConfig = new GOption(this, 420, 90 , 160, 20, "Configure Board");
  g_optConfig_r  = new Rectangle(400, 82, 180, 30); // visual frame when option is selected
g_optConfig.addEventHandler(this, "g_optConfig_option");

  // Upgrade Available button
  g_upgradeAvailable = new GButton(this, 590, 84, 160, 30, "Upgrade available..."); 
  g_upgradeAvailable.addEventHandler(this, "g_upgradeAvailable_button");
  g_upgradeAvailable.setLocalColor(2, color(#aa2222));   // idx 2: Text
  g_upgradeAvailable.setLocalColor(3, color(#f7f7f7));    // idx 3: Border  
  g_upgradeAvailable.setLocalColor(4, color(#f7f7f7));     // Face 
  g_upgradeAvailable.setLocalColor(6, color(#f7f7f7));  // Face Over
  g_upgradeAvailable.setLocalColor(14,color(#face01));  // Face Over-Clicked
  g_upgradeAvailable.setVisible(false);



  g_tgOptions = new GToggleGroup();
  
  g_tgOptions.addControls(g_optDescribe,g_optConfig, g_optUpload);
  g_optDescribe.setSelected(true);

  g_grpOpt = new GGroup(this);
  
//  g_grpOpt.addControls(grpOpt_label, g_optDescribe, g_optUpload);
  g_grpOpt.addControls(g_optDescribe, g_optUpload);
  g_optConfig.setVisible(false);
  g_grpOpt.setVisible(false);



  // Board Software Description
  /////////////////////////////

  g_boardDescr = new GTextArea(this, 10, 130, 780, 280, G4P.SCROLLBARS_VERTICAL_ONLY | G4P.SCROLLBARS_AUTOHIDE);
  g_boardDescr.setVisible(false);
  g_boardDescr.setTextEditEnabled(false);

}



/*
 *  User Config for 4 Players OLR Software
 */
void createUploadUI(){

  
  // Display Areas for Availables Software Lists
  //////////////////////////////////////////////
  g_upListRect   = new Rectangle(15, 130, 770, 280); // Availables Software List Display Area
  g_upListArea   = new ScrollableGrid(this, g_upListRect.x, g_upListRect.y, g_upListRect.width, g_upListRect.height);
  g_upListArea.setBackground(#f7f7f7);
  g_upListArea.setLineNumColor(#81bd4d);
  g_upListArea.setCellColors(#f7f7f7, #f5f5f5, #222222); 
  g_upListArea.setFooterColors(#f4f4f4, #f1f1f1, #444444);
  g_upListArea.setTitleColors(#fdfdd2, #d2e7e7, #080b74);
  g_upListArea.setHeaderColors(#e5e9ed, #d5d9dd, #111111); 
  g_upListArea.setTitle(" Available Software", 14);
  g_upListArea.highlightMouseRow(true);

  // Single Software Description
  g_upListdescr = new GTextArea(this, 15, 420, 770, 260, G4P.SCROLLBARS_VERTICAL_ONLY | G4P.SCROLLBARS_AUTOHIDE);
  g_upListdescr.setVisible(false);
  g_upListdescr.setTextEditEnabled(false);

}  


/*
 *  User Config for 4 Players OLR Software
 */
void create4PlayersConfigUI(){
  
  // Panel containing the User Controls
  /////////////////////////////////////
  pnlCfg_4Pv1_r = new Rectangle(10, 130, 780, 370); // Board Config display area (panel) 
  pnlCfg_4Pv1   = new GPanel(this, pnlCfg_4Pv1_r.x, pnlCfg_4Pv1_r.y, pnlCfg_4Pv1_r.width, pnlCfg_4Pv1_r.height, "");
  pnlCfg_4Pv1.setOpaque(true); // Display or not the 'area' and the TAB text
  // (!!!) Without explicit setFont the TAB size doesn't scale to fit text
  pnlCfg_4Pv1.setFont(new Font("Monospaced", Font.PLAIN, 14)); 
  pnlCfg_4Pv1.setText("4 Players OLR Configuration");
  pnlCfg_4Pv1.setCollapsible(false);
  pnlCfg_4Pv1.setDraggable(false);
  pnlCfg_4Pv1.setCollapsed(false);
  pnlCfg_4Pv1.setVisible(false);


  // LAPs Number 
  //////////////
  // Label 
  g_laps_label = new GLabel(this, 2, 20, 160, 60);
  g_laps_label.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  g_laps_label.setText("How many Laps");
  g_laps_label.setTextBold();
  // Slider
  g_laps = new GCustomSlider(this, 190, 20, 300, 60, "red_yellow18px");  // red_yellow18px    blue18px
  g_laps.addEventHandler(this, "g_laps_slider");
  g_laps.setOpaque(false); 
  g_laps.setNumberFormat(G4P.INTEGER);
  g_laps.setNbrTicks(100); 
  g_laps.setShowValue(true); 
  // Info Text
  g_laps_infotext = new GLabel(this, 500, 20, 200, 60);
  g_laps_infotext.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);

  // Sostituire con load da JSON file MultilLingua
  g_laps_helpText = new GLabel(this, 0, 0, 260, 30);
  g_laps_helpText.setTextAlign(GAlign.LEFT, GAlign.TOP);
  g_laps_helpText.setText("Number of Laps in a single race.\n" +
                           "\n");
  g_laps_helpText.setOpaque(true);                         
  g_laps_help = new GControlPalette(this, infoIconFull, GAlign.EAST, 175, 45);
  g_laps_help.addControls(g_laps_helpText);


  // LED Strip Number 
  ///////////////////
  // Label
  g_stripN_label = new GLabel(this, 2, 75, 180, 60);
  g_stripN_label.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  g_stripN_label.setText("How many LED Strips ?");
  g_stripN_label.setTextBold();
  // Field
  g_stripN = new GCustomSlider(this, 190, 75, 80, 60, "red_yellow18px");  // red_yellow18px    blue18px
  g_stripN.addEventHandler(this, "g_stripN_slider");
  g_stripN.setNumberFormat(G4P.INTEGER);
  g_stripN.setNbrTicks(5); 

  g_stripN.setShowValue(true); 
  g_stripN.setStickToTicks(true);
  // Info Text
  g_stripN_infotext = new GLabel(this, 280, 75, 220, 60);
  g_stripN_infotext.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  g_stripN_infotext.setText("5 mt");

  // Sostituire con load da JSON file MultilLingua
  g_stripN_helpText = new GLabel(this, 0, 0, 460, 60);
  g_stripN_helpText.setTextAlign(GAlign.LEFT, GAlign.TOP);
  g_stripN_helpText.setText("Number of LED Strips in the Racetrack.\n" +
                             "Limited to ONE for the standard Open LED Race (5 mt Racetrack).\n" +
                             "Up to 5  (25 mt Racetrack) for the Open LED Race PRO");
  g_stripN_helpText.setOpaque(true);                         
  g_stripN_help = new GControlPalette(this, infoIconFull.copy(), GAlign.EAST, 175, 100);
  g_stripN_help.addControls(g_stripN_helpText);

  // Expert Mode cfg (insert LEDs number in a textbox)
  g_stripN_expert_helpText = new GLabel(this, 0, 0, 460, 60);  // substitute "g_stripN_helpText" info text when [Advanced] is checked 
  g_stripN_expert_helpText.setTextAlign(GAlign.LEFT, GAlign.TOP);
  g_stripN_expert_helpText.setText("LEDS number in your special Strip.\n" +
                            "In advanced mode you are allowed to set values\n" +
                            "not multiple of 300 (Standard LED strip length)\n");
  g_stripN_expert_helpText.setOpaque(true);                         
  g_stripN_expert_help = new GControlPalette(this, infoIconFull.copy(), GAlign.EAST, 175, 100);
  g_stripN_expert_help.addControls(g_stripN_expert_helpText);
  g_stripN_expert_help.setVisible(false);
  
  // textfield: user inserted value
  g_stripN_expert_ledN = new GTextField(this, 195, 86, 100, 30);
  g_stripN_expert_ledN.setNumericType(G4P.INTEGER); 
//  g_stripN_expert_ledN.addEventHandler(this, "g_stripN_expert_ledN");
//  g_stripN_expert_ledN.setPromptText("LEDs number");
  g_stripN_expert_ledN.setVisible(false);


  // AutoStart
  ////////////
  g_autostart_AlwaysOn  = new GCheckbox(this, 650, 10, 100, 60, "Autostart");
  g_autostart_AlwaysOn.addEventHandler(this, "g_autostartOn_checkbox");
  g_autostart_AlwaysOn.setTextAlign(GAlign.LEFT, GAlign.LEFT); 

  g_autostart_helpText = new GLabel(this, 0, 0, 480, 120);
  g_autostart_helpText.setTextAlign(GAlign.LEFT, GAlign.TOP);
  g_autostart_helpText.setText("Race Autostart active.\n" +
                            "When a race ends, the countdown for a new one will start automatically.\n" +
                            "When not checked, the OLR will wait for players confirmation:\n" +
                            " **Every player have to push the Controller**\n" +
                            "When all Controllers are 'ON', countdown will start.\n" +
                            "\n");
  g_autostart_helpText.setOpaque(true);

  g_autostart_help = new GControlPalette(this, infoIconFull.copy(), GAlign.WEST, 635, 38);
  g_autostart_help.addControls(g_autostart_helpText);

  // Player 3
  ///////////
  g_player3_AlwaysOn  = new GCheckbox(this, 650, 45, 100, 60, "Player 3");
  g_player3_AlwaysOn.addEventHandler(this, "g_player3On_checkbox");
  g_player3_AlwaysOn.setTextAlign(GAlign.LEFT, GAlign.LEFT); 

  g_player3_helpText = new GLabel(this, 0, 0, 480, 120);
  g_player3_helpText.setTextAlign(GAlign.LEFT, GAlign.TOP);
  g_player3_helpText.setText("Player 3 (Blue) Always Active.\n" +
                            "Checked: On startup, OLR will always activate Player 3 (Blue).\n" +
                            "Not Checked: To activate Player 3 (Blue) you need to push blue controller on startup.\n" +
                            "\n");
  g_player3_helpText.setOpaque(true);

  g_player3_help = new GControlPalette(this, infoIconFull.copy(), GAlign.WEST, 635, 73);
  g_player3_help.addControls(g_player3_helpText);


  // Player 4
  ///////////
  g_player4_AlwaysOn  = new GCheckbox(this, 650, 80, 100, 60, "Player 4");
  g_player4_AlwaysOn.addEventHandler(this, "g_player4On_checkbox");
  g_player4_AlwaysOn.setTextAlign(GAlign.LEFT, GAlign.LEFT); 

  g_player4_helpText = new GLabel(this, 0, 0, 480, 120);
  g_player4_helpText.setTextAlign(GAlign.LEFT, GAlign.TOP);
  g_player4_helpText.setText("Player 4 (White) Always Active.\n" +
                            "Checked: On startup, OLR will always activate Player 4 (White).\n" +
                            "Not Checked: To activate Player 4 (White) you need to push white controller on startup.\n" +
                            "\n");
  g_player4_helpText.setOpaque(true);

  g_player4_help = new GControlPalette(this, infoIconFull.copy(), GAlign.WEST, 635, 108);
  g_player4_help.addControls(g_player4_helpText);



  // Boxes Length 
  ///////////////
  // Label 
  g_boxl_label = new GLabel(this, 2, 125, 160, 60);
  g_boxl_label.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  g_boxl_label.setText("Pitlane Length ?");
  g_boxl_label.setTextBold();
  // Slider
  g_boxl = new GCustomSlider(this, 190, 125, 300, 60, "red_yellow18px");  // red_yellow18px    blue18px
  g_boxl.addEventHandler(this, "g_boxl_slider");
  g_boxl.setOpaque(false); 
  g_boxl.setNumberFormat(G4P.INTEGER);
  g_boxl.setShowLimits(true); 
  g_boxl.setShowValue(true); 

  // textfield: user inserted value
  g_boxl_expert = new GTextField(this, 195, 136, 100, 30);
  g_boxl_expert.setNumericType(G4P.INTEGER); 
  g_boxl_expert.setVisible(false);

  g_boxl_AlwaysOn = new GCheckbox(this, 520, 125, 100, 60, "Always On");
  g_boxl_AlwaysOn.addEventHandler(this, "g_boxl_PitlaneAlwaysOn_checkbox");
  g_boxl_AlwaysOn.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);

  g_boxl_DisplayPitlane = new GCheckbox(this, 650, 125, 200, 60, "Display Pitlane");
  g_boxl_DisplayPitlane.addEventHandler(this, "g_boxl_DisplayPitlane_checkbox");
  g_boxl_DisplayPitlane.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);

  // Sostituire con load da JSON file MultilLingua
  g_boxl_helpText = new GLabel(this, 0, 0, 460, 100);
  g_boxl_helpText.setTextAlign(GAlign.LEFT, GAlign.TOP);
  g_boxl_helpText.setText("Number of LEDs reserved for the Pitlane (Boxes).\n" +
                           "To create the pitlane, the final part of the Strip will be bent like in the image below.\n" +
                           "The two LEDs where Pitlane begin/ends will be turned on to let you put the Strip in the correct position.\n");
  g_boxl_helpText.setOpaque(true);                         
  g_boxl_help = new GControlPalette(this, infoIconFull.copy(), GAlign.EAST, 175, 150);
  g_boxl_help.addControls(g_boxl_helpText);

  
  g_boxlAO_helpText = new GLabel(this, 0, 0, 460, 60);
  g_boxlAO_helpText.setTextAlign(GAlign.LEFT, GAlign.TOP);
  g_boxlAO_helpText.setText("Pitlane Always active.\n" +
                            "Your OLR will always activate the Pitlane on Startup (no need to push the Green Controller)\n" +
                            "\n");
  g_boxlAO_helpText.setOpaque(true);                         
  g_boxlAO_help = new GControlPalette(this, infoIconFull.copy(), GAlign.NORTH, 505, 150);
  g_boxlAO_help.addControls(g_boxlAO_helpText);



  // Slope 
  ////////
  // Center - Label
  g_slopeC_label = new GLabel(this, 2, 190, 160, 60);
  g_slopeC_label.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  g_slopeC_label.setText("Slope Center ?");
  g_slopeC_label.setTextBold();
  // Center - Slider
  g_slopeC = new GCustomSlider(this, 190, 190, 580, 60, "red_yellow18px");  // red_yellow18px    blue18px
  g_slopeC.addEventHandler(this, "g_slopeC_slider");
  g_slopeC.setNumberFormat(G4P.INTEGER);
  g_slopeC.setShowLimits(true); 
  g_slopeC.setShowValue(true); 

  // Expert Mode textfield: user inserted value
  // Start
  g_slopeS_expert = new GTextField(this, 195, 206, 100, 30);
  g_slopeS_expert.setNumericType(G4P.INTEGER); 
  g_slopeS_expert.setVisible(false);
  // Center
  g_slopeC_expert = new GTextField(this, 300, 206, 100, 30);
  g_slopeC_expert.setNumericType(G4P.INTEGER); 
  g_slopeC_expert.setVisible(false);
  // End
  g_slopeE_expert = new GTextField(this, 405, 206, 100, 30);
  g_slopeE_expert.setNumericType(G4P.INTEGER); 
  g_slopeE_expert.setVisible(false);

  // Height - Label
  g_slopeH_label = new GLabel(this, 2, 230, 160, 60);
  g_slopeH_label.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  g_slopeH_label.setText("How Steep?");
  g_slopeH_label.setTextBold();
  // Height - Slider
  g_slopeH = new GCustomSlider(this, 190, 230, 80, 60, "red_yellow18px");  // red_yellow18px    blue18px
  g_slopeH.addEventHandler(this, "g_slopeH_slider");
  g_slopeH.setNumberFormat(G4P.INTEGER);
  g_slopeH.setShowLimits(true); 
  g_slopeH.setShowValue(true); 

  // Height - Expert Mode textfield: user inserted value
  g_slopeH_expert = new GTextField(this, 195, 246, 100, 30);
  g_slopeH_expert.setNumericType(G4P.INTEGER); 
  g_slopeH_expert.setVisible(false);


  g_slope_AlwaysOn = new GCheckbox(this, 520, 230, 100, 60, "Always On");
  g_slope_AlwaysOn.addEventHandler(this, "g_slope_SlopeAlwaysOn_checkbox");
  g_slope_AlwaysOn.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);

  g_slope_DisplaySlope = new GCheckbox(this, 650, 230, 350, 60, "Display Slope");
  g_slope_DisplaySlope.addEventHandler(this, "g_slope_DisplaySlope_checkbox");
  g_slope_DisplaySlope.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);


  // Sostituire con load da JSON file MultilLingua
  g_slopeC_helpText = new GLabel(this, 0, 0, 460, 100);
  g_slopeC_helpText.setTextAlign(GAlign.LEFT, GAlign.TOP);
  g_slopeC_helpText.setText("Slope Position in the Racetrack (center).\n" +
                             "Do not put the Slope too close to the Start Line (you need some speed to pass over it).\n" +
                             "Maximum position for the Slope Center depends from the Pitlane (see the image below).\n");
  g_slopeC_helpText.setOpaque(true);                         
  g_slopeC_help = new GControlPalette(this, infoIconFull.copy(), GAlign.EAST, 175, 215);
  g_slopeC_help.addControls(g_slopeC_helpText);

  // Expert Mode - Substitute Slope Center Help Text
  g_slopeC_expert_helpText = new GLabel(this, 0, 0, 460, 100);
  g_slopeC_expert_helpText.setTextAlign(GAlign.LEFT, GAlign.TOP);
  g_slopeC_expert_helpText.setText("Slope Position in the Racetrack (Start, Center, End).\n" +
                             "PLEASE NOTE: DO NOT TURN ON TOO MANY LEDS AT ONCE !!!.\n" +
                             ".\n");
  g_slopeC_expert_helpText.setOpaque(true);                         
  g_slopeC_expert_help = new GControlPalette(this, infoIconFull.copy(), GAlign.EAST, 175, 215);
  g_slopeC_expert_help.addControls(g_slopeC_expert_helpText);
  g_slopeC_expert_help.setVisible(false);


  g_slopeH_helpText = new GLabel(this, 0, 0, 460, 60);
  g_slopeH_helpText.setTextAlign(GAlign.LEFT, GAlign.TOP);
  g_slopeH_helpText.setText("Slope Height.\n" +
                             "High values can make very difficult for cars to get over it.\n" +
                             "Don't put a high slope close to the Start line.\n");
  g_slopeH_helpText.setOpaque(true);                         
  g_slopeH_help = new GControlPalette(this, infoIconFull.copy(), GAlign.EAST, 175, 255);
  g_slopeH_help.addControls(g_slopeH_helpText);


  g_slopeAO_helpText = new GLabel(this, 0, 0, 460, 60);
  g_slopeAO_helpText.setTextAlign(GAlign.LEFT, GAlign.TOP);
  g_slopeAO_helpText.setText("Slope Always active.\n" +
                            "Your OLR will always activate the Slope on Startup (no need to push the Red Controller)\n" +
                            "\n");
  g_slopeAO_helpText.setOpaque(true);                         
  g_slopeAO_help = new GControlPalette(this, infoIconFull.copy(), GAlign.NORTH, 505, 255);
  g_slopeAO_help.addControls(g_slopeAO_helpText);


  // Battery
  //////////
  g_battery_label = new GLabel(this, 2, 285, 160, 60);
  g_battery_label.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  g_battery_label.setText("Battery");
  g_battery_label.setTextBold();
  
  g_battery_On  = new GCheckbox(this, 190, 285, 100, 60, "Active");
  g_battery_On.addEventHandler(this, "g_batteryOn_checkbox");
  g_battery_On.setTextAlign(GAlign.LEFT, GAlign.LEFT); 

  g_battery_helpText = new GLabel(this, 0, 0, 480, 120);
  g_battery_helpText.setTextAlign(GAlign.LEFT, GAlign.TOP);
  g_battery_helpText.setText("Battery mode active.\n" +
                            "The cars in your racetrack are Electric Cars!\n" +
                            "When checked, car's battery will discharge as you push your controller\n" +
                            "and the car will loose power and speed.\n" +
                            "You have to recharge it in the 'Power Station' to have full speed again\n" +
                            "\n");
  g_battery_helpText.setOpaque(true);

  g_battery_help = new GControlPalette(this, infoIconFull.copy(), GAlign.EAST, 175, 315);
  g_battery_help.addControls(g_battery_helpText);

  // Expert Mode textfield: user inserted value
  // Help Text
  g_battery_expert_helpText = new GLabel(this, 0, 0, 480, 120);
  g_battery_expert_helpText.setTextAlign(GAlign.LEFT, GAlign.TOP);
  g_battery_expert_helpText.setText("Battery mode active [Y|N].\n" +
                            "Delta: [1-254] Battery usage for a single 'click'->[0.01% - 2.54 %]\n" +
                            "Min: [1-99] Battery does not descharge below [1%-99%]\n" +
                            "Boost:[1-254] Speed boost when a car gets fully recharged\n" +
                            "\n");
  g_battery_expert_helpText.setOpaque(true);
  g_battery_expert_help = new GControlPalette(this, infoIconFull.copy(), GAlign.EAST, 275, 315);
  g_battery_expert_help.addControls(g_battery_expert_helpText);
  g_battery_expert_help.setVisible(false);
  
  // Delta
  g_batteryD_expert = new GTextField(this, 300, 300, 60, 30);
  g_batteryD_expert.setNumericType(G4P.INTEGER); 
  g_batteryD_expert.setVisible(false);
  // Min
  g_batteryM_expert = new GTextField(this, 365, 300, 60, 30);
  g_batteryM_expert.setNumericType(G4P.INTEGER); 
  g_batteryM_expert.setVisible(false);
  // Boost
  g_batteryB_expert = new GTextField(this, 430, 300, 60, 30);
  g_batteryB_expert.setNumericType(G4P.INTEGER); 
  g_batteryB_expert.setVisible(false);


  // Demo mode checkbox
  /////////////////////
  g_demo_label = new GLabel(this, 2, 320, 160, 60);
  g_demo_label.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  g_demo_label.setText("Demo mode");
  g_demo_label.setTextBold();
  
  g_demo_On  = new GCheckbox(this, 190, 320, 100, 60, "Active");
  g_demo_On.addEventHandler(this, "g_demoModeOn_checkbox");
  g_demo_On.setTextAlign(GAlign.LEFT, GAlign.LEFT); 

  g_demo_helpText = new GLabel(this, 0, 0, 480, 120);
  g_demo_helpText.setTextAlign(GAlign.LEFT, GAlign.TOP);
  g_demo_helpText.setText("Demo mode active.\n" +
                            "On boot Board plays a 'Simulated' race where cars run without any real player.\n" +
                            "Useful to get people attention in Fairs, etc.\n" +
                            "On user activity (somebody touch a controller) the boards jumps\n" +
                            "back automatically to Normal mode (Users play races via Controllers).\n" +
                            "After 5 mins of inactivity, it gets back again in Demo mode.\n" +
                             "  \n" +
                             "!!! Board needs a REBOOT to after changig this setting.\n" +
                            "\n");
  g_demo_helpText.setOpaque(true);

  g_demo_help = new GControlPalette(this, infoIconFull.copy(), GAlign.EAST, 175, 350);
  g_demo_help.addControls(g_demo_helpText);



  // Expert Mode CheckBox
  ///////////////////////
  g_ExpertMode_on = new GCheckbox(this, 650, 340, 120, 20, "Expert mode");
  g_ExpertMode_on.addEventHandler(this, "g_ExpertMode_on_checkbox");
  g_ExpertMode_on.setTextAlign(GAlign.LEFT, GAlign.MIDDLE);
  g_ExpertMode_on.setOpaque(true);



  // "Save" / "Cancel" buttons
  g_btnSave = new GButton(this, 670, 520, 100, 30, "Save to Board"); // Outside panel limits !!! display below Racetrack 
  g_btnSave.setLocalColor(4, color(134,88,74)); // Color index 4: Button Background
  g_btnSave.addEventHandler(this, "g_btnSave_button");
  g_btnSave.setLocalColor(2, color(255,255,200));   // idx 2: Text
  g_btnSave.setLocalColor(3, color(200,145,90));    // idx 3: Border  
  g_btnSave.setLocalColor(4, color(116,78,42));     // Face 
  g_btnSave.setLocalColor(6, color(154,95,37));     // Face Over
  g_btnSave.setLocalColor(14, color(195,158,122));  // Face Over-Clicked
  
  g_btnCancel = new GButton(this, 10, 520, 100, 30, "Cancel"); // Outside panel limits !!! display below Racetrack
  g_btnCancel.setLocalColor(2, color(255,255,200));   // idx 2: Text
  g_btnCancel.setLocalColor(3, color(200,145,90));    // idx 3: Border  
  g_btnCancel.setLocalColor(4, color(116,78,42));     // Face 
  g_btnCancel.setLocalColor(6, color(154,95,37));  // Face Over
  g_btnCancel.setLocalColor(14, color(195,158,122));  // Face Over-Clicked
  

  g_btnCancel.addEventHandler(this, "g_btnCancel_button");

  // Set values for Sliders and Text Fields values in the UI
  UIC_set4PlayersConfigUIValues();

  // Add controls to the Panel
  pnlCfg_4Pv1.addControls(g_laps, g_laps_label, g_laps_infotext, g_laps_help, g_autostart_AlwaysOn,  g_autostart_help);
  pnlCfg_4Pv1.addControls(g_player3_help, g_player3_AlwaysOn, g_player4_help, g_player4_AlwaysOn);
  pnlCfg_4Pv1.addControls(g_battery_label, g_battery_On, g_battery_help, g_batteryD_expert,  g_batteryM_expert, g_batteryB_expert, g_battery_expert_help);
  pnlCfg_4Pv1.addControls(g_demo_label, g_demo_On, g_demo_help);  

  pnlCfg_4Pv1.addControls(g_boxl, g_boxl_expert, g_boxl_label,g_boxl_help, g_boxl_AlwaysOn, g_boxl_DisplayPitlane, g_boxlAO_help);  
  pnlCfg_4Pv1.addControls(g_slopeC_label, g_slopeC, g_slopeH_label, g_slopeC_help, g_slopeH_help, g_slopeH, g_slope_DisplaySlope, g_slope_AlwaysOn, g_slopeAO_help);
  pnlCfg_4Pv1.addControls(g_slopeS_expert, g_slopeC_expert, g_slopeE_expert, g_slopeC_expert_help,  g_slopeH_expert);

  pnlCfg_4Pv1.addControls(g_stripN_label, g_stripN, g_stripN_infotext,g_stripN_help, g_ExpertMode_on, g_stripN_expert_help, g_stripN_expert_ledN);
  pnlCfg_4Pv1.addControls(g_btnSave,g_btnCancel   );

  // Allows TABBING between text controls in Expert Mode
  // Note:
  //    TAB does not work (but at least ENTER moves cursor to next field)
   expertModeTab = new GTabManager();
   boolean ok=expertModeTab.addControls(g_stripN_expert_ledN, g_boxl_expert, g_slopeS_expert, g_slopeC_expert, g_slopeE_expert, g_slopeH_expert, g_batteryD_expert,  g_batteryM_expert, g_batteryB_expert );
  

  create4PlayersConfigStripVisualizationUI();

}  

  
  
/*
 *  Racetrack (Strip) visualization;
 */
void create4PlayersConfigStripVisualizationUI(){

  stripView = new GView(this, 10, 400, 780, 300, P3D); 

  // Set Racetrack visualization for SwConfig_A4P0 object into the GView
  cfgA4P0.setVisualizationPGraphics(stripView.getGraphics());

  stripView.setVisible(false);
}


void createPleaseWaitArea(){
  
  // Please wait message
  //   display area
  g_pleaseWait_r = new Rectangle(300, 200, 150, 200);  
  g_pleaseWait = new Mover(g_pleaseWait_r.x, g_pleaseWait_r.y, g_pleaseWait_r.width, g_pleaseWait_r.height); 
  //   text
  g_user_infotext = new GLabel(this, g_pleaseWait_r.x, g_pleaseWait_r.y + 50, g_pleaseWait_r.width, g_pleaseWait_r.height);
  g_user_infotext.setTextAlign(GAlign.CENTER, GAlign.MIDDLE);
  g_user_infotext.setTextBold();
  g_user_infotext.setLocalColorScheme(0); // Reddish...
  g_user_infotext.setVisible(false);


  int butW = 60;
  int butH = 20;
  g_pleaseWait_OkButton = new GButton(this, g_pleaseWait_r.x + (g_pleaseWait_r.width / 2 - butW/2) , g_pleaseWait_r.y + g_pleaseWait_r.height - butH/2 , butW, butH, "OK"); 
  g_pleaseWait_OkButton.addEventHandler(this, "g_pleaseWait_OkButton");
  g_pleaseWait_OkButton.setVisible(false);
}

  
  
  
                                 /*******************************************************
                                  UIC_xxxx()
                                    -> UI-related functions used in other UI source files
                                  *******************************************************/
 
 
 
  
void UIC_displayYourBoardFrame(){
  stroke(210);
  //noFill();
  fill(#f7f7f7);
  rect(lcRect.x, lcRect.y, lcRect.width, lcRect.height); // Board Device, Software area
  if(g_status >= STATUS.USR_DESCR_BOARD) {
    if(g_optDescribe.isSelected()) {
      rect(g_optDescribe_r.x, g_optDescribe_r.y, g_optDescribe_r.width, g_optDescribe_r.height);
    } else if(g_optConfig.isSelected()) {
      rect(g_optConfig_r.x, g_optConfig_r.y, g_optConfig_r.width, g_optConfig_r.height);
    } else if(g_optUpload.isSelected()) {
      rect(g_optUpload_r.x, g_optUpload_r.y, g_optUpload_r.width, g_optUpload_r.height);
    }
  }
}  


/*
 *  Check if the values just read from board need an "Expert Mode" UI
 */
boolean UIC_needExpertMode (){

  // if MaxLed (loaded from the Board) is NOT a multiple of 300, we need the Advanced Configuration 
  int chk = (cfgA4P0.maxLed / g_singleStripLEDNumber) * g_singleStripLEDNumber;
  if(chk != cfgA4P0.maxLed){
    return(true);
  } 
  return(false);
}  
  
  
  
/*
 *  Set values for sliders in "4PlayersConfigUI"
 *  The values are copied from the ones in memory (SwConfig_A4P0.laps, SwConfig_A4P0.maxLed, etc)
 *  this method is used on UI creation and when new values are read from the Board
 */
void UIC_set4PlayersConfigUIValues (){

  if(cfgA4P0.isPlayer3Active()){
    g_player3_AlwaysOn.setSelected(true);
  } 
  if(cfgA4P0.isPlayer4Active()){
    g_player4_AlwaysOn.setSelected(true);
  }

  if(cfgA4P0.isAutostartActive()){
    g_autostart_AlwaysOn.setSelected(true);
  }
  if(cfgA4P0.isBoxAlwaysOnActive()){
    g_boxl_AlwaysOn.setSelected(true);
  }
  if(cfgA4P0.isSlopeAlwaysOnActive()){
    g_slope_AlwaysOn.setSelected(true);
  }
  
  if(cfgA4P0.isBatteryActive()){
    g_battery_On.setSelected(true);
  }

  if(cfgA4P0.isDemoModeActive()){
    g_demo_On.setSelected(true);
  }
  
  
  
  // laps
  g_laps.setValue(cfgA4P0.laps);
  g_laps.setLimits(cfgA4P0.laps, 1, 99);

  // maxLed  
  g_stripN.setLimits(cfgA4P0.maxLed/g_singleStripLEDNumber, 1, cfgA4P0.maxPLed/g_singleStripLEDNumber);
  g_stripN.setNbrTicks(cfgA4P0.maxPLed/g_singleStripLEDNumber); 
  g_stripN.setValue(cfgA4P0.maxLed/g_singleStripLEDNumber);


  // Pitlane (Boxes)  
  if(cfgA4P0.maxLed <= g_singleStripLEDNumber){
    g_boxl.setLimits(cfgA4P0.boxLen, 40, 80);
    g_boxl.setNbrTicks(40); 
  } else {
    g_boxl.setLimits(cfgA4P0.boxLen, 60, 120);
    g_boxl.setNbrTicks(80);
  }
  // Slope Center
  int maxSlopeCenter = cfgA4P0.maxLed - cfgA4P0.boxLen - (cfgA4P0.slopeEnd - cfgA4P0.slopeCenter) - 10;
  g_slopeC.setLimits(cfgA4P0.slopeCenter, cfgA4P0.SLOPE_CENTER, maxSlopeCenter); // DEFAULT.SWCFG_4P.SLOPE.CENTER is the MINIMUM valid value of the range
  g_slopeC.setNbrTicks((maxSlopeCenter - cfgA4P0.SLOPE_CENTER)/10);
  // Slope Height
  g_slopeH.setLimits(cfgA4P0.slopeHeight, cfgA4P0.SLOPE_HMIN, cfgA4P0.SLOPE_HMAX);
  g_slopeH.setNbrTicks(cfgA4P0.SLOPE_HMAX - cfgA4P0.SLOPE_HMIN); 
  
}  


/*
 *
 */
void UIC_setExpertMode(boolean active) {
  
    if(active) {  
      g_ExpertMode_on.setSelected(true);
      // Set values according the one from the Board
      g_stripN_expert_ledN.setText(String.valueOf(cfgA4P0.maxLed));
      g_boxl_expert.setText(String.valueOf(cfgA4P0.boxLen));
      g_slopeS_expert.setText(String.valueOf(cfgA4P0.slopeStart));
      g_slopeC_expert.setText(String.valueOf(cfgA4P0.slopeCenter));
      g_slopeE_expert.setText(String.valueOf(cfgA4P0.slopeEnd));
      g_slopeH_expert.setText(String.valueOf(cfgA4P0.slopeHeight));

      g_batteryD_expert.setText(String.valueOf(cfgA4P0.bdelta));
      g_batteryM_expert.setText(String.valueOf(cfgA4P0.bmin));
      g_batteryB_expert.setText(String.valueOf(cfgA4P0.bboost));

      
        // set the "Advanced" input field range according to the value of "cfgA4P0.maxPLed " (loaded from JSON cfg for board)         
    g_stripN_expert_ledN.setNumeric(10, cfgA4P0.maxPLed , 0 ); // g_stripN_expert_ledN.getValueI() will return 0 on invalid range

  
      // Led/LedStrip Number
      g_stripN.setVisible(false);               // Slider
      g_stripN_expert_ledN.setVisible(true);    // Input Textfield  
      g_stripN_label.setText("How many LEDs?"); // Label
      g_stripN_label.setTextBold();
      g_stripN_infotext.setVisible(false);      // Infotext for Slider (5 mt, 10 mt, etc)
      g_stripN_help.setVisible(false);          // Contextual help for Slider
      g_stripN_expert_help.setVisible(true);    // Contextual help for Input Textfield
      
      // Box Length
      g_boxl.setVisible(false);                 // Slider
      g_boxl_expert.setVisible(true);           // Input Textfield
      g_boxl_DisplayPitlane.setVisible(false);  
 
      // Slope 
      g_slopeC_help.setVisible(false);          // Contextual help for Slider
      g_slopeC_expert_help.setVisible(true);    // Contextual help for Input Textfield
      g_slopeC_label.setText("Slope Start/Center/End"); // Label
      g_slopeC_label.setTextBold();
      
      g_slopeC.setVisible(false);               // Slider
      g_slopeS_expert.setVisible(true);         // Input Textfield
      g_slopeC_expert.setVisible(true);         // Input Textfield
      g_slopeE_expert.setVisible(true);         // Input Textfield
      g_slopeH.setVisible(false);               // Slider
      g_slopeH_expert.setVisible(true);         // Input Textfield
      g_slope_DisplaySlope.setVisible(false);

      // Battery
      g_battery_label.setText("Battery Delta/Min/Boost"); // Label
      g_battery_label.setTextBold();
      g_battery_expert_help.setVisible(true);
      g_batteryD_expert.setVisible(true);         // Input Textfield
      g_batteryM_expert.setVisible(true);         // Input Textfield
      g_batteryB_expert.setVisible(true);         // Input Textfield

      g_stripN_expert_ledN.setFocus(true);
      

    } else {

      // Exiting from EXPERT mode reload defaults for Base mode
      // User inserted Values are not managed by StripVisualization (need 300 led strip and more constrains)
      cfgA4P0.loadDefaults();
      
      // Led/LedStrip Number
      g_stripN_infotext.setVisible(true); 
      g_stripN_expert_ledN.setVisible(false);
      g_stripN.setVisible(true);
      g_stripN_label.setText("How many LED Strips ?");
      g_stripN_label.setTextBold();
      g_stripN_expert_help.setVisible(false);
      g_stripN_help.setVisible(true);
      
      // Box Length
      g_boxl_expert.setVisible(false);  
      g_boxl.setVisible(true); 
      g_boxl_DisplayPitlane.setVisible(true);  
 
      // Slope 
      g_slopeC_help.setVisible(true);            // Contextual help for Slider
      g_slopeC_expert_help.setVisible(false);    // Contextual help for Input Textfield
      g_slopeC_label.setText("Slope Center ?");
      g_slopeC_label.setTextBold();     
      g_slopeC.setVisible(true);
      g_slopeS_expert.setVisible(false);         // Input Textfield
      g_slopeC_expert.setVisible(false);         // Input Textfield
      g_slopeE_expert.setVisible(false);         // Input Textfield
      g_slopeH.setVisible(true);
      g_slopeH_expert.setVisible(false);
      g_slope_DisplaySlope.setVisible(true);

      // Battery
      g_battery_label.setText("Battery"); // Label
      g_battery_label.setTextBold();
      g_battery_expert_help.setVisible(false);
      g_batteryD_expert.setVisible(false);         // Input Textfield
      g_batteryM_expert.setVisible(false);         // Input Textfield
      g_batteryB_expert.setVisible(false);         // Input Textfield


    }
    
    // Update SwConfig_A4P0 internal image
    cfgA4P0.setExpertModeVal(active);   

}


void  UIC_setUserMessage (String msg){
  g_user_infotext.setText(msg);
  g_user_infotext.setVisible(true);
  rect(g_pleaseWait_r.x, g_pleaseWait_r.y, g_pleaseWait_r.width, g_pleaseWait_r.height);
}

void  diplayUserMessage(){
  stroke(210);
  noFill();
  rect(g_pleaseWait_r.x, g_pleaseWait_r.y, g_pleaseWait_r.width, g_pleaseWait_r.height);
}

void  UIC_removeUserMessage(){
  g_user_infotext.setVisible(false);
  g_pleaseWait_OkButton.setVisible(false);
}
