
class SwConfig_A4P0 {

  // Default values
  public static final boolean EXPERTMODE = false;   // Advanced Configuration Y|N (allow Led Number not multiple of 300, etc) 
  public static final int MAXLED         = 300; // Number of LEDs connected to the board - dovrebbe chiamarsi numLed....
  public static final int MAXPLED        = 300; // Max number of LEDs the board can manage (Platform-dependent) 
  public static final int AUTOSTART      = 1;
  public static final int PLAYER3        = 0;
  public static final int PLAYER4        = 0;
  public static final int DEMO           = 0;
  public static final int NETWORK        = 0;
  
  
  public static final int LAPS           = 5;
  public static final int BOXLEN         = 60;
  public static final int BOXALWAYSON    = 0;
  public static final int SLOPE_UP_NUM   = 10; // Uphill, from start to Center
  public static final int SLOPE_DOWN_NUM = 10; // Downhill from Center to end
  public static final int SLOPE_CENTER   = 90;
  public static final int SLOPE_HEIGHT   = 3;
  public static final int SLOPEALWAYSON  = 0;
  public static final int BDELTA         = 3; // Battery Delta  [1|254] (Battery discharge BDELTA/100 each controller activation)
  public static final int BMIN           = 60; // Battery Min value  [1|99] (descharge no more than BMIN %)
  public static final int BBOOST         = 10; // Battery Boost on full recharge [1|254]
  public static final int BACTIVE        = 0; // Battery mode active on startup [0|1]
  
  
  public static final int SLOPE_HMIN     = 3;
  public static final int SLOPE_HMAX     = 25;

  boolean  expertMode; 
  int      maxLed;    
  int      maxPLed; 
  int      autoStart;
  int      player3;
  int      player4;
  int      demoMode;
  int      networkMode;
  int      laps;      
  int      boxLen;    
  int      boxAlwaysOn;
  int      slopeStart;
  int      slopeCenter;
  int      slopeEnd; 
  int      slopeHeight;  
  int      slopeAlwaysOn;
  int      bdelta;
  int      bmin;
  int      bboost;
  int      bactive;
  
  private RaceDevice raceDevice;
  private boolean readcfgQuerySent;
  
//  private boolean setCfgSent; 
  private boolean setConfigurationModeDone, setRaceCfgDone, setTrackLenCfgDone, setAutostartCfgDone, setPlayersCfgDone, setBoxLenCfgDone, setSlopeCfgDone;
  private boolean setBatteryCfgDone, saveConfigurationDone, setDemoModeCfgDone;
  private boolean lastBoardOperationOK;

  private ProtocolSerial.Cmd.GetBoardConfig.BoardConfig boardCfg; // Values read from board

  // Inner Class:
  //   Racetrack 3D Visualization Parameters (for StripShape class)
  //   Used to 'scale' values according to the ratio between Visualized points and LED number in the real Strip
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////
  class Visualization {
    
    int     numPoints = 300; // Number of LED represented in the Strip 
    int     ecx       = 370; // center x  
    int     ecy       =  80; // center y
    float   ea        = 930; // lengtht on X axis (semi-axe a)
    float   eb        = 270; // lengtht on Y axis (semi-axe b)
    float   eyeZ      = 750; // eyeZ for Camera - make it bigger if Strip does not fit into the view

    float   slopeScale = 5.0;  // Magnification factor for slope height
    float   rotateX=60;        // Rotation around X plane (in degrees)
    float   boxS=6;            // Size of the Box() representing a single Joint (LED)
    float   stripW=5;          // strokeWeight (to draw the line between Joints)
    int     shiftVal= 20;      // do not modify ;)            

    int   ratio;
    int   boxLen;
    int   slopeCenter;
    int   slopeStart ;
    int   slopeEnd   ;
    int   slopeHeight;
    
    StripShape   stp;  // 3D Racetrack Visualization 
    
    //  Constructor
    Visualization(String logStr){

      stp = new StripShape (null,
                            this.ecx,
                            this.ecy,
                            this.ea,
                            this.eb,
                            this.numPoints,
                            this.eyeZ,
                            logStr);
      stp.setShiftVal(this.shiftVal);
      stp.setSlopeScaleFactor(this.slopeScale);

      stp.setSlopeRange( SLOPE_HMIN,  SLOPE_HMAX);
      
      stp.setRotX(this.rotateX); 
      stp.setBoxS(this.boxS); 
      stp.setStripW(this.stripW); 
   
    }
        
    /*
     *  Update parameters for StripShape 
     */
    void update(){
      if(SwConfig_A4P0.this.maxLed < this.numPoints){
        t.error(String.format("Visualization.update() - No visualization available for Numled < [%d]\n", this.numPoints) );
        return;
      }
      
      this.ratio = SwConfig_A4P0.this.maxLed/this.numPoints;
      this.boxLen      =  SwConfig_A4P0.this.boxLen/this.ratio;
      
      this.slopeCenter =  SwConfig_A4P0.this.slopeCenter/this.ratio;
      this.slopeStart  =  SwConfig_A4P0.this.slopeStart/this.ratio;
      this.slopeEnd    =  SwConfig_A4P0.this.slopeEnd/this.ratio;
      this.slopeHeight =  SwConfig_A4P0.this.slopeHeight;
      
      SwConfig_A4P0.this.t.log(Trace.LOWLEVEL,String.format("Visualization.update(): maxLed[%d]  Box[%d]->[%d] Start[%d]->[%d] Center[%d]->[%d] End[%d]->[%d] Height[%d]->[%d]\n",
        SwConfig_A4P0.this.maxLed,
        SwConfig_A4P0.this.boxLen,       this.boxLen     ,
        SwConfig_A4P0.this.slopeStart,   this.slopeStart , 
        SwConfig_A4P0.this.slopeCenter,  this.slopeCenter, 
        SwConfig_A4P0.this.slopeEnd,     this.slopeEnd, 
        SwConfig_A4P0.this.slopeHeight,  this.slopeHeight 
        ));        
    }
    
  } // Visualization Inner Class

    
  // Strip Visualization 
  Visualization visualization;

  private Trace t;
  private static final String _CLASS_ = "SwConfig_A4P0";

  SwConfig_A4P0 (RaceDevice rd, String logStr) {

    this.loadDefaults();
    
    this.raceDevice    = rd;
    this.readcfgQuerySent   = false;

    resetSaveBoardCfgStatus();

    this.lastBoardOperationOK = false;
    this.boardCfg = new ProtocolSerial.Cmd.GetBoardConfig.BoardConfig() ;
    
    if(logStr.equals("-") ){
      t = new Trace(Trace.NONE);
    } else {
      t = new Trace(logStr);
    }      
    t.setClassId(_CLASS_);
    
    this.visualization = new Visualization(logStr);
    // Box
    this.visualization.stp.setBoxLen(this.boxLen);
    // Slope
    this.visualization.stp.setSlope( this.slopeStart,
                  this.slopeCenter, 
                  this.slopeEnd, 
                  this.slopeHeight);

    visualization.update();
    
    if(logStr.equals("-") ){
      t = new Trace(Trace.NONE);
    } else {
      t = new Trace(logStr);
    }      
    t.setClassId(_CLASS_);
    
  }   
  
  void setVisualizationPGraphics(PGraphics pg){
    this.visualization.stp.setVisualizationPGraphics(pg);
  }
  
  /*
   *
   */
  void loadDefaults(){
    this.maxLed        = MAXLED;
    this.maxPLed       = MAXPLED;
    this.autoStart     = AUTOSTART;
    this.player3       = PLAYER3;
    this.player4       = PLAYER4;
    this.demoMode      = DEMO;
    this.networkMode   = NETWORK;
    this.expertMode    = EXPERTMODE;
    this.laps          = LAPS;
    this.boxLen        = BOXLEN;
    this.boxAlwaysOn   = BOXALWAYSON;
    this.slopeStart    = SLOPE_CENTER - SLOPE_UP_NUM;
    this.slopeCenter   = SLOPE_CENTER;
    this.slopeEnd      = SLOPE_CENTER + SLOPE_DOWN_NUM;
    this.slopeHeight   = SLOPE_HEIGHT;
    this.slopeAlwaysOn = SLOPEALWAYSON;
    this.bdelta        = BDELTA;
    this.bmin          = BMIN;
    this.bboost        = BBOOST;
    this.bactive       = BACTIVE;
  }

  /*
   *
   */
  void setMaxLed(int n){
      this.maxLed=n;
      visualization.update();
  }
  
  /*
   *
   */
  void setMaxPLed(int n){
      this.maxPLed=n;
  } 
  
  
  /*
   *
   */
  void setAutostart(int n){
      this.autoStart=n;
  }
  boolean isAutostartActive() { 
   if(this.autoStart == 1) return(true);
   return(false);
  }
  
  /*
   *
   */
  void setPlayer3(int n){
      this.player3=n;
  }
  boolean isPlayer3Active() { 
   if(this.player3 == 1) return(true);
   return(false);
  }
  void setPlayer4(int n){
      this.player4=n;
  }
  boolean isPlayer4Active() { 
   if(this.player4 == 1) return(true);
   return(false);
  }

  void setDemoMode(int n){
      this.demoMode=n;
  }
  boolean isDemoModeActive() { 
   if(this.demoMode == 1) return(true);
   return(false);
  }

  void setNetworkMode(int n){
      this.networkMode=n;
  }
  boolean isNetworkActive() { 
   if(this.networkMode == 1) return(true);
   return(false);
  }

  
  /*
   * Set/Get ExpertMode
   */
  void setExpertModeVal(boolean bval){
      this.expertMode=bval;
  }
  boolean isExpertModeActive() { // same as getExpertMode()
      return(this.expertMode);
  }

  
  /*
   *
   */
  void setBoxLen(int n){
      this.boxLen=n;
      visualization.update();
  }

  /*
   *
   */
  void setBoxAlwaysOn(int n){
      this.boxAlwaysOn=n;
      visualization.update();
  }
  boolean isBoxAlwaysOnActive() { 
   if(this.boxAlwaysOn == 1) return(true);
   return(false);
  }

  /*
   *
   */
  void setSlope(int s, int c, int e){
    this.slopeStart    = s;
    this.slopeCenter   = c;
    this.slopeEnd      = e;
    visualization.update();
  }

  /*
   *
   */
  void setSlopeH(int n){
      this.slopeHeight=n;
      visualization.update();
  }
  
  /*
   *
   */
  void setSlopeAlwaysOn(int n){
      this.slopeAlwaysOn=n;
//      visualization.update();
  }
  boolean isSlopeAlwaysOnActive() { 
   if(this.slopeAlwaysOn == 1) return(true);
   return(false);
  }
  
  
  /*
   *
   */
  void setBattery(int n){
      this.bactive=n;
  }
  
  boolean isBatteryActive() { 
   if(this.bactive == 1) return(true);
   return(false);
  }  
  
  /////////////////////////////////////////////// 
  // Methods to talk to the Board (RaceDevice) //
  ///////////////////////////////////////////////
  
  /**
   * Non-blocking (to be called in a loop until it returns TRUE) 
   * Read current Cfg Values from Board
   */  
  boolean readBoardCfg() {
  
    // On first call -> Send "enter cfg mode" request to board
    //   - according to the protocol, if the board is already in cfg mode it will respond OK)
    // When Configuration Mode is confirmed, send a "Get Current Configuration" to board 
    if (! this.readcfgQuerySent ) {
      if (! this.setConfigurationModeDone) {
        String str = String.format("%c%c",ProtocolSerial.Cmd.EnterCfgMode.ID,ProtocolSerial.EOC);
        int done = phRaceDev.sendConfirmedCmdToBoard(str); 
        if ( done == 1 ) {
          this.setConfigurationModeDone = true;  // Done for [Enter Configuration mode] command
          return(false);  // on next call will send next command to board 
        } else if ( done == -1 ) {
          t.error("readBoardCfg() -> [NOK] ERROR from Board for [Enter Config Mode] command\n");
          return(true); // do not continue  
        }
        return(false);  // done=0 -> wait for confirmation
      }
      // Board answer OK to the ENTERCFG request => Now Send "GetBoardConfig" commands
      
    byte cmd1[]={ProtocolSerial.Cmd.GetBoardConfig.ID, ProtocolSerial.EOC};
      this.raceDevice.sendCmdToBoard(cmd1);
      this.readcfgQuerySent = true;
      this.boardCfg.reset(); // in case user calls readBoardCfg() again in the same session 
    }
    

    // Serial "Commands" received from OLRBoard  
    ///////////////////////////////////////////
    if( ! this.raceDevice.fromBoardQ.isEmpty()) {
      String cmd  = this.raceDevice.fromBoardQ.poll() ;
      t.log(Trace.DETAILED, String.format("readBoardCfg() -> received from board:[%s]\n", cmd));
      
      char cmdId=cmd.charAt(0);  // first char = "Command.ID"
      if(cmdId == ProtocolSerial.Cmd.GetBoardConfig.ID) {
        String error = ProtocolSerial.Cmd.GetBoardConfig.getParam(cmd,this.boardCfg);

        if(error != null){
          t.error(error);
        }
      }     
      if(this.boardCfg.isComplete()){
        t.log(Trace.DETAILED, String.format("readBoardCfg() -> done:[\n%s]\n", this.boardCfg.toString()));
        updateInternalCfg();
        this.readcfgQuerySent=false;  //reset
        this.setConfigurationModeDone=false;  //reset
        return(true);
      }
    }     
    return(false);
  }
  
  

  /**
   * Send to board the "Set Pitlane Length" command  
       !!! Board SHOULD be already in "Configuration Mode" !!!
   */    
  void sendBoardPitlaneCfg () {
      String str = ProtocolSerial.Cmd.SetBoxLen.getCommand(this.boxLen, this.boxAlwaysOn );   
      phRaceDev.sendString(str);
  }
   
  /**
   * Send to board the "Set Slope Configuration" command  
       !!! Board SHOULD be already in "Configuration Mode" !!!
   */    
  void sendBoardSlopeCfg () {
      String str = ProtocolSerial.Cmd.SetSlopeConfig.getCommand(this.slopeStart, this.slopeCenter, this.slopeEnd, this.slopeHeight, this.slopeAlwaysOn);
      phRaceDev.sendString(str);
  }
  
  

  /**
   * Non-blocking (to be called in a loop until it returns TRUE) 
   * Save Current cfg Values to Board
   *
   * Please note: Send one command at the time !!!
   *   send one command and "returns" until it receives the answer for the command (OK|NOK)
   *
   * @return  0 if "saving configuration process" is not completed  
   * @return  1 if configuration saved succesfully 
   * @return -1 if some of the Save config commands returned Error (<CommandId>NOK received) 
   */    
  int saveBoardCfg () {

    // Send "Enter in configuration mode" command 
    // Note:
    //  According to the protocol, if the board is already in cfg mode it will respond OK) 
    if (! this.setConfigurationModeDone) {
      String str = String.format("%c%c",ProtocolSerial.Cmd.EnterCfgMode.ID,ProtocolSerial.EOC);
      int done = phRaceDev.sendConfirmedCmdToBoard(str);     
      if ( done == 1 ) {
        this.setConfigurationModeDone = true;  // Done for this command -> on next call will send next command to board 
        return(0);  // Save cfg not completed yet
      } else if ( done == -1 ) {
        t.error("saveBoardCfg() -> [NOK] ERROR from Board for [Enter Config Mode] command\n");
        resetSaveBoardCfgStatus(); // "Board Answer"=[Error] -> done
      }
      return(done);  // returns 0|-1
    }
   
    // Racetrack Length (MaxLED)
    if (! this.setTrackLenCfgDone) {
      String str = ProtocolSerial.Cmd.SetTrackLen.getCommand(this.maxLed);
      int done = phRaceDev.sendConfirmedCmdToBoard(str);
      if ( done == 1 ) {
        this.setTrackLenCfgDone = true;  // Done for this command -> on next call will send next command to board 
        return(0);  // Save cfg not completed yet
      } else if ( done == -1 ) {
        resetSaveBoardCfgStatus(); // "Board Answer"=[Error] -> done
      }
      return(done);  // returns 0|-1
    }

    // Autostart
    if (! this.setAutostartCfgDone) {
      String str = ProtocolSerial.Cmd.SetAutostart.getCommand(this.autoStart);
      int done = phRaceDev.sendConfirmedCmdToBoard(str);
      if ( done == 1 ) {
        this.setAutostartCfgDone = true;  // Done for this command -> on next call will send next command to board 
        return(0);  // Save cfg not completed yet
      } else if ( done == -1 ) {
        resetSaveBoardCfgStatus(); // "Board Answer"=[Error] -> done
      }
      return(done);  // returns 0|-1
    }


    // Players
    if (! this.setPlayersCfgDone) {
      String str = ProtocolSerial.Cmd.SetPlayers.getCommand(2 + this.player3 + this.player4);
      int done = phRaceDev.sendConfirmedCmdToBoard(str);
      if ( done == 1 ) {
        this.setPlayersCfgDone = true;  // Done for this command -> on next call will send next command to board 
        return(0);  // Save cfg not completed yet
      } else if ( done == -1 ) {
        resetSaveBoardCfgStatus(); // "Board Answer"=[Error] -> done
      }
      return(done);  // returns 0|-1
    }
        

    // Boxes (Pitlane) Length 
    if (! this.setBoxLenCfgDone) {
      String str = ProtocolSerial.Cmd.SetBoxLen.getCommand(this.boxLen, this.boxAlwaysOn );   
      int done = phRaceDev.sendConfirmedCmdToBoard(str);
      if ( done == 1 ) {
        this.setBoxLenCfgDone = true;  // Done for this command -> on next call will send next command to board 
        return(0);  // Save cfg not completed yet
      } else if ( done == -1 ) {
        resetSaveBoardCfgStatus(); // "Board Answer"=[Error] -> done
      }
      return(done); // returns 0|-1
    }
    
    
    // Slope  
    if (! this.setSlopeCfgDone) {
      String str = ProtocolSerial.Cmd.SetSlopeConfig.getCommand(this.slopeStart, this.slopeCenter, this.slopeEnd, this.slopeHeight, this.slopeAlwaysOn);
      int done = phRaceDev.sendConfirmedCmdToBoard(str);
      if ( done == 1 ) {
        this.setSlopeCfgDone = true;  // Done for this command -> on next call will send next command to board 
        return(0);  // Save cfg not completed yet
      } else if ( done == -1 ) {
        resetSaveBoardCfgStatus(); // "Board Answer"=[Error] -> done
      }
      return(done); // returns 0|-1
    }
    

    // Laps number
    if (! this.setRaceCfgDone) {
      String str = ProtocolSerial.Cmd.SetConfig.getCommand(1,this.laps,1,1); // StartHere=Repeat=FinishHere always = 1 !!!
      int done = phRaceDev.sendConfirmedCmdToBoard(str);
      if ( done == 1 ) {
        this.setRaceCfgDone = true;  // Done for this command -> on next call will send next command to board 
        return(0);  // Save cfg not completed yet
      } else if ( done == -1 ) {
        resetSaveBoardCfgStatus(); // "Board Answer"=[Error] -> done
      }
      return(done); // returns 0|-1
    
    }

    // Battery  
    if (! this.setBatteryCfgDone) {
      String str = ProtocolSerial.Cmd.SetBatteryConfig.getCommand(this.bdelta, this.bmin, this.bboost, this.bactive);
      int done = phRaceDev.sendConfirmedCmdToBoard(str);
      if ( done == 1 ) {
        this.setBatteryCfgDone = true;  // Done for this command -> on next call will send next command to board 
        return(0);  // Save cfg not completed yet
      } else if ( done == -1 ) {
        resetSaveBoardCfgStatus(); // "Board Answer"=[Error] -> done
      }
      return(done); // returns 0|-1
    }
    
    // Demo mode  
    if (! this.setDemoModeCfgDone) {
      String str = ProtocolSerial.Cmd.SetDemoModeConfig.getCommand(this.demoMode);
      int done = phRaceDev.sendConfirmedCmdToBoard(str);
      if ( done == 1 ) {
        this.setDemoModeCfgDone = true;  // Done for this command -> on next call will send next command to board 
        return(0);  // Save cfg not completed yet
      } else if ( done == -1 ) {
        resetSaveBoardCfgStatus(); // "Board Answer"=[Error] -> done
      }
      return(done); // returns 0|-1
    }
    
    
    if (! this.saveConfigurationDone) {
      String str = String.format("%c%c",ProtocolSerial.Cmd.SaveCfg.ID,ProtocolSerial.EOC);
      int done = phRaceDev.sendConfirmedCmdToBoard(str);
      if ( done == -1 ) {
        t.error("saveBoardCfg() -> [NOK] ERROR from Board saving new configuration\n");
      } else if ( done == 1 ) {
        this.saveConfigurationDone = true;  
      }
      return(0); 
    }

    // done with config commands
    resetSaveBoardCfgStatus();
    
    // Send "Leave Configuration mode" command
    String str = String.format("%c%c",ProtocolSerial.Cmd.LeaveCfgMode.ID,ProtocolSerial.EOC);
    phRaceDev.sendString(str);


    return(1);
  }
 
 
  /*
   *
   */
  void resetSaveBoardCfgStatus(){
    this.setConfigurationModeDone = this.setRaceCfgDone = this.setTrackLenCfgDone = this.setAutostartCfgDone = 
    this.setPlayersCfgDone = this.setBoxLenCfgDone = this.setSlopeCfgDone = this.setBatteryCfgDone = 
    this.saveConfigurationDone = this.setDemoModeCfgDone = false;
  }
 
    
  /*
   * Set values for internal parateters to the values read from Board
   */
  void updateInternalCfg(){
    this.maxLed        = (Integer)boardCfg.track.get("MAXLED");
    this.laps          = (Integer)boardCfg.race.get("LAPS");  
    this.autoStart     = (Integer)boardCfg.track.get("AUTOSTART");
    this.player3       = (Integer)boardCfg.race.get("PLAYER3");
    this.player4       = (Integer)boardCfg.race.get("PLAYER4");
    this.demoMode      = (Integer)boardCfg.race.get("DEMO");
    this.networkMode   = (Integer)boardCfg.race.get("NETWORK");
    this.boxLen        = (Integer)boardCfg.track.get("BOXLEN");
    this.boxAlwaysOn   = (Integer)boardCfg.track.get("BOXALWAYSON");
    this.slopeStart    = (Integer)boardCfg.ramp.get("INIT");
    this.slopeCenter   = (Integer)boardCfg.ramp.get("CENTER");
    this.slopeEnd      = (Integer)boardCfg.ramp.get("END");
    this.slopeHeight   = (Integer)boardCfg.ramp.get("HEIGHT");
    this.slopeAlwaysOn = (Integer)boardCfg.ramp.get("ALWAYSON");
    this.bdelta        = (Integer)boardCfg.battery.get("DELTA");
    this.bmin          = (Integer)boardCfg.battery.get("MIN");
    this.bboost        = (Integer)boardCfg.battery.get("BOOST");
    this.bactive       = (Integer)boardCfg.battery.get("ACTIVE");
  }

  /*
   *
   */
  boolean lastBoardOperationResultOK() {
    return(this.lastBoardOperationOK);
  }




}
