/* 
 * Application "Logic" of the UI:   
 *   Handlers for UI events
 */


/**
 * event Handlers for BOARD (USB Ports) SELECTION Droplist
 */
void handleDropListEvents(GDropList list, GEvent event)    { 
  if (list == lcDroplist)  {
    int idx = lcDroplist.getSelectedIndex() - 1 ; // first item (idx=0) in Droplist is: "Please select ..."
    g_t.log(Trace.DETAILED, String.format("handleDropListEvents() - Item [%d] Selected - [%s]\n",idx, lcDroplist.getSelectedText()));

    if( ! g_serialPortFound && idx < g_ports.length) {
      g_serialPortFound = true;  
      g_serialPort = g_ports[idx];         

      lcSelectedPort_label.setText(new String(String.format("[%s] [%s]\n", g_ports[idx].getSystemPortName(), g_ports[idx].getDescriptivePortName()) ));
      lcDroplist.setVisible(false);
      lcSelectedPort_label.setVisible(true);
    }
  } else {
    g_t.log(Trace.DETAILED, String.format("handleDropListEvents() - UNEXPECTED DropList EVENT:[%s]\n",event));
  }
}


public void g_optDescribe_option(GOption option, GEvent event) { 
    g_t.log(Trace.DETAILED, String.format("g_optDescribe_option() GEvent[%s]\n", event ));
    g_status=STATUS.USR_DESCR_BOARD;
}

public void g_optUpload_option(GOption option, GEvent event) { 
    g_t.log(Trace.DETAILED, String.format("g_optUpload_option() GEvent[%s]\n", event ));
    g_status=STATUS.USR_UPLOAD_BOARD;  
    UIC_removeUserMessage();
}

public void g_optConfig_option(GOption option, GEvent event) { 
    g_t.log(Trace.DETAILED, String.format("g_optConfig_option() GEvent[%s]\n", event ));
    g_status=g_configStatus;
    UIC_setUserMessage("Loading Board Configuration...");
}

/**
 * event Handlers for "HOW MANY LAPS IN A RACE" Slider (4Players software config)
 */
public void g_laps_slider(GCustomSlider slider, GEvent event) { 
    g_t.log(Trace.DETAILED, String.format("g_laps_slider() - Slider getValuesS(%s) tag[%s] GEvent[%s]\n",slider.getValueS(), slider.tag, event ));

    // Update maxLed value
    int num = g_laps.getValueI();
    cfgA4P0.laps   = num;
    
    // Update info text 
    if(num < 10){
      g_laps_infotext.setText(String.format("%d laps Race", num));
    } else if (num > 10 && num < 30){
      g_laps_infotext.setText(String.format("%d laps Race (!)", num));
    } else if (num >=20){
      g_laps_infotext.setText(String.format("%d laps Race (!!!) ", num));
    }
}


/**
 * event Handlers for "HOW MANY LED STRIP IN RACETRACK" Slider (4Players software config)
 */
public void g_stripN_slider(GCustomSlider slider, GEvent event) { 
    g_t.log(Trace.DETAILED, String.format("g_stripN_slider() - Slider getValuesS(%s) tag[%s] GEvent[%s]\n",slider.getValueS(), slider.tag, event ));
    
    // Update maxLed value
    int num = g_stripN.getValueI();
    cfgA4P0.setMaxLed(num * g_singleStripLEDNumber);

    update_stripN_slider_infotext();
    
    // On maxLed change, the visualization Ratio also changes 
    // (Slope and Pitlane position changes)
    /////////////////////////////////////////////////////////
    refreshRacetrack();       
    
}
/**
 * Update Infotext for Racetrack length 
 */
void update_stripN_slider_infotext () {

    int mt = g_stripN.getValueI() * 5 ;
    g_stripN_infotext.setText(String.format("%d mt", mt));
}



/**
 * event Handlers for "Autostart On" checkbox (4Players software config)
 */
public void g_autostartOn_checkbox(GCheckbox cbox, GEvent event) { 
    g_t.log(Trace.DETAILED, String.format("g_autostartOn_checkbox() - tag[%s] GEvent[%s]\n", cbox.tag, event ));
    
     if(g_autostart_AlwaysOn.isSelected()) {
      cfgA4P0.setAutostart(1);    
    } else {
      cfgA4P0.setAutostart(0);    
    }    
}

/**
 * event Handlers for "Player3 On" checkbox (4Players software config)
 */
public void g_player3On_checkbox(GCheckbox cbox, GEvent event) { 
    g_t.log(Trace.DETAILED, String.format("g_player3On_checkbox() - tag[%s] GEvent[%s]\n", cbox.tag, event ));
    
     if(g_player3_AlwaysOn.isSelected()) {
      cfgA4P0.setPlayer3(1);    
    } else {
      cfgA4P0.setPlayer3(0);
      g_player4_AlwaysOn.setSelected(false); // Force Plauyer 4 off (3=off, 4=on NOT permitted)  
      cfgA4P0.setPlayer4(0);
    }    
}

/**
 * event Handlers for "Player4 On" checkbox (4Players software config)
 */
public void g_player4On_checkbox(GCheckbox cbox, GEvent event) { 
    g_t.log(Trace.DETAILED, String.format("g_player4On_checkbox() - tag[%s] GEvent[%s]\n", cbox.tag, event ));
    
     if(g_player4_AlwaysOn.isSelected()) {
      cfgA4P0.setPlayer4(1);    
      g_player3_AlwaysOn.setSelected(true); // Force Plauyer 3 on (3=off, 4=on NOT permitted)  
      cfgA4P0.setPlayer3(1);
    } else {
      cfgA4P0.setPlayer4(0);    
    }    
}



/**
 * event Handlers for "PITLANE" Lenght Slider (4Players software config)
 */
public void g_boxl_slider(GCustomSlider slider, GEvent event) { 
    g_t.log(Trace.LOWLEVEL, String.format("g_boxl_slider() - Slider getValuesS(%s) tag[%s] GEvent[%s]\n",slider.getValueS(), slider.tag, event ));

    // Force display of Pitlane when user changes it
    g_boxl_DisplayPitlane.setSelected(true); 

    // Update Pitlane length
    cfgA4P0.setBoxLen(g_boxl.getValueI());

    // Slope Valid Range depends on boxLen 
    refreshRacetrack();
    
  }


/**
 * event Handlers for "Display Pitlane" checkbox (4Players software config)
 */
public void g_boxl_DisplayPitlane_checkbox(GCheckbox cbox, GEvent event) { 
    g_t.log(Trace.DETAILED, String.format("g_boxl_DisplayPitlane_checkbox() - tag[%s] GEvent[%s]\n", cbox.tag, event ));
    refreshRacetrack();
}

/**
 * event Handlers for "Pitlane Always On" checkbox (4Players software config)
 */
public void g_boxl_PitlaneAlwaysOn_checkbox(GCheckbox cbox, GEvent event) { 
    g_t.log(Trace.DETAILED, String.format("g_boxl_PitlaneAlwaysOn_checkbox() - tag[%s] GEvent[%s]\n", cbox.tag, event ));
    
     if(g_boxl_AlwaysOn.isSelected()) {
      g_boxl_DisplayPitlane.setSelected(true); // Force Pitlane display  
      cfgA4P0.setBoxAlwaysOn(1);    
    } else {
      cfgA4P0.setBoxAlwaysOn(0);    
    }    
    
    refreshRacetrack();
}


/**
 * event Handlers for "SLOPE Center" Slider (4Players software config)
 */
public void g_slopeC_slider(GCustomSlider slider, GEvent event) { 
    g_t.log(Trace.LOWLEVEL, String.format("g_slopeC_slider() - Slider getValuesS(%s) tag[%s] GEvent[%s]\n",slider.getValueS(), slider.tag, event ));

    // Force display of Slope when user changes it
    g_slope_DisplaySlope.setSelected(true); 

    int slopeC = g_slopeC.getValueI();
    cfgA4P0.setSlope(slopeC - cfgA4P0.SLOPE_UP_NUM,  slopeC, slopeC + cfgA4P0.SLOPE_DOWN_NUM);

    refreshSlope();
    
}



/**
 * event Handlers for "SLOPE Height" Slider (4Players software config)
 */
public void g_slopeH_slider(GCustomSlider slider, GEvent event) { 
    g_t.log(Trace.DETAILED, String.format("g_slopeH_slider() - Slider getValuesS(%s) tag[%s] GEvent[%s]\n",slider.getValueS(), slider.tag, event ));

    // Force display of Slope when user changes it
    g_slope_DisplaySlope.setSelected(true); 

    // Update Slope Center and Start/End (calculated)
    int slopeH = g_slopeH.getValueI();
    cfgA4P0.setSlopeH(slopeH);

    refreshSlope();

}



/**
 * event Handlers for "Display Slope" checkbox (4Players software config)
 */
public void g_slope_DisplaySlope_checkbox(GCheckbox cbox, GEvent event) { 
    g_t.log(Trace.DETAILED, String.format("g_slope_DisplaySlope() - tag[%s] GEvent[%s]\n", cbox.tag, event ));
    refreshRacetrack();
}


/**
 * event Handlers for "Display Slope" checkbox (4Players software config)
 */
public void g_slope_SlopeAlwaysOn_checkbox(GCheckbox cbox, GEvent event) { 
    g_t.log(Trace.DETAILED, String.format("g_slope_SlopeAlwaysOn_checkbox() - tag[%s] GEvent[%s]\n", cbox.tag, event ));
    
    if(g_slope_AlwaysOn.isSelected()) {
      g_slope_DisplaySlope.setSelected(true); // Force display of Slope 
      cfgA4P0.setSlopeAlwaysOn(1);    
    } else {
      cfgA4P0.setSlopeAlwaysOn(0);    
    }

    refreshRacetrack();
}


/**
 * event Handlers for "Battery On" checkbox (4Players software config)
 */
public void g_batteryOn_checkbox(GCheckbox cbox, GEvent event) { 
    g_t.log(Trace.DETAILED, String.format("g_batteryOn_checkbox() - tag[%s] GEvent[%s]\n", cbox.tag, event ));
    
     if(g_battery_On.isSelected()) {
      cfgA4P0.setBattery(1);    
    } else {
      cfgA4P0.setBattery(0);    
    }    
}

/**
 * event Handlers for "Demo mode On" checkbox (4Players software config)
 */
public void g_demoModeOn_checkbox(GCheckbox cbox, GEvent event) { 
    g_t.log(Trace.DETAILED, String.format("g_demoModeOn_checkbox() - tag[%s] GEvent[%s]\n", cbox.tag, event ));
    
     if(g_demo_On.isSelected()) {
      cfgA4P0.setDemoMode(1);    
    } else {
      cfgA4P0.setDemoMode(0);    
    }    
}





/*   
 *  - CHECK/SET boxLen
 *  - CHECK/SET max range for SlopeCenter and check current value
 *
 *    Reset to default if out-of-range  
 */
void checkConstrains () {
  // Check Pirtlane
  if(cfgA4P0.maxLed == g_singleStripLEDNumber){
    g_boxl.setLimits(cfgA4P0.boxLen, 40, 80);
    g_boxl.setNbrTicks(40); 
    if(cfgA4P0.boxLen > 80){
      cfgA4P0.setBoxLen(60);
      g_boxl.setValue(60);
    }
  } else {
    g_boxl.setLimits(cfgA4P0.boxLen, 60, 120);
    g_boxl.setNbrTicks(80);
    if(cfgA4P0.boxLen < 60){
      cfgA4P0.setBoxLen(60);
      g_boxl.setValue(60);
    }
  }
  
  // Check current Slope Center Value   
  int maxSlopeCenter = cfgA4P0.maxLed - cfgA4P0.boxLen - (cfgA4P0.slopeEnd - cfgA4P0.slopeCenter) - 10;
  if(cfgA4P0.slopeCenter > maxSlopeCenter){ 
    cfgA4P0.setSlope(cfgA4P0.SLOPE_CENTER - cfgA4P0.SLOPE_UP_NUM,  cfgA4P0.SLOPE_CENTER, cfgA4P0.SLOPE_CENTER + cfgA4P0.SLOPE_DOWN_NUM);
    cfgA4P0.visualization.stp.setSlope( cfgA4P0.visualization.slopeStart, cfgA4P0.visualization.slopeCenter, cfgA4P0.visualization.slopeEnd, cfgA4P0.visualization.slopeHeight,cfgA4P0.visualization.slopeScale);
  }
  // Update Slope Range upper limit
  g_slopeC.setLimits(cfgA4P0.slopeCenter, cfgA4P0.SLOPE_CENTER, maxSlopeCenter); // DEFAULT.SWCFG_4P.SLOPE.CENTER is the MINIMUM valid value of the range
  g_slopeC.setNbrTicks((maxSlopeCenter - cfgA4P0.SLOPE_CENTER)/10);
    
}


/*   
 *  
 */
void refreshPitlane () {
  checkConstrains();
  cfgA4P0.visualization.stp.setBoxLen(cfgA4P0.visualization.boxLen);
  if(g_boxl_DisplayPitlane.isSelected()) {
    cfgA4P0.visualization.stp.closeRacetrack();
    // Update visualization on Board (physical ledstrip)
    cfgA4P0.sendBoardPitlaneCfg();
  } else {
    cfgA4P0.visualization.stp.resetStrip();
  }
}


/*   
 *  
 */
void refreshSlope () {
  checkConstrains();
  cfgA4P0.visualization.stp.setSlopeH(cfgA4P0.visualization.slopeHeight);
  cfgA4P0.visualization.stp.setSlope( cfgA4P0.visualization.slopeStart, cfgA4P0.visualization.slopeCenter, cfgA4P0.visualization.slopeEnd, cfgA4P0.visualization.slopeHeight,cfgA4P0.visualization.slopeScale);
  if(g_slope_DisplaySlope.isSelected()) {
    cfgA4P0.visualization.stp.slopeDisplay(true); 
    // Update visualization on Board (physical ledstrip)
    cfgA4P0.sendBoardSlopeCfg();
  } else {
    cfgA4P0.visualization.stp.slopeDisplay(false); 
  }

}


/*   
 *  
 */
void refreshRacetrack () {
  if(cfgA4P0.isExpertModeActive()){
    return; // no Strip Visualization in Expert Mode
  }   
  refreshPitlane();
  refreshSlope();
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////  EXPERT Mode ////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * event Handlers for "Expert Mode" checkbox (4Players software config)
 */
public void g_ExpertMode_on_checkbox(GCheckbox cbox, GEvent event) { 
    g_t.log(Trace.DETAILED, String.format("g_ExpertMode_on_checkbox() - tag[%s] GEvent[%s]\n", cbox.tag, event ));
    UIC_setExpertMode(g_ExpertMode_on.isSelected()); // set visible/invisible UI objects for the specific UI (Std|Advanced)   
}

/*
 *  Validates EXPERT MODE fields 
 */
String [] validateExpertModeFields(){
  // LEDs number
  int n = g_stripN_expert_ledN.getValueI(); 
  if( n == -1 || n<10 || n > cfgA4P0.maxPLed ) {
    return( new String[] {"LEDS Number Error", String.format("[%d] Out of Range [10,%d]", n, cfgA4P0.maxPLed)} );
  }
  // Box Length
  int boxL = g_boxl_expert.getValueI();
  if( (boxL !=0) && (boxL < 40 || boxL > n / 3) ) {  
    return( new String[] {"Pitlane Length Error", String.format("[%d] Out of Range [0, 40-%d]", boxL, n / 3)} );
  }
  
  // Slope
  int slopeS = g_slopeS_expert.getValueI();
  int slopeC = g_slopeC_expert.getValueI();
  int slopeE = g_slopeE_expert.getValueI();
  if( ! (slopeS < slopeC && slopeC < slopeE)) {  
    return( new String[] {"Slope Configuration Error", String.format("Start/Center/End order error. Should be [%d < %d < %d]", slopeS, slopeC, slopeE)} );
  }
  int maxSlopeEnd = n - boxL - 1;
  if( slopeE >= maxSlopeEnd ) {  
    return( new String[] {"Slope Configuration Error", String.format("Slope ends at [%d] after End of Racetrack. Should be < [%d]", slopeE, maxSlopeEnd)} );
  }
  if( slopeS != 0 && slopeS < 10 ) {  
    return( new String[] {"Slope Configuration Error", String.format("Slope starts at [%d] too close to Race Start. Should be > [%d]", slopeS, 10)} );
  }
  int slopeH = g_slopeH_expert.getValueI(); 
  if( slopeH < 0 || slopeH > 20 ) {  
    return( new String[] {"Slope Height Error", String.format("[%d] above max value [%d]", slopeH, 20)} );
  }
  
  // fields OK, copy to SwConfig_A4P0 obj to be Saved
  cfgA4P0.setMaxLed(n);
  cfgA4P0.setBoxLen(boxL);
  cfgA4P0.setSlope(slopeS,  slopeC, slopeE);
  cfgA4P0.setSlopeH(slopeH);
  return(null);
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


/*
 * Save configuration Button
 */
public void g_btnSave_button(GButton source, GEvent event) {
    if(event == GEvent.CLICKED ) {
      g_t.log(Trace.BASE, String.format("g_btnSave_button() - [%s] BUTTON   GEvent[%s]\n",source.getText(),event ));
      
      if(cfgA4P0.isExpertModeActive()) { // in Expert Mode need to check value s
        String [] errStr = validateExpertModeFields();
        if ( errStr != null) {
          G4P.showMessage(this, errStr[1] , errStr[0] , G4P.ERROR_MESSAGE);
          g_status = g_configStatus;
        } else {
            g_status=g_saveConfigStatus; // jump to save status
        }      
      } else {
        g_status=g_saveConfigStatus;
      }
    }
}


/*
 * Cancel configuration Button
 */
 public void g_btnCancel_button(GButton source, GEvent event) {
    if(event == GEvent.CLICKED ) {
      g_t.log(Trace.BASE, String.format("g_btnCancel_button() - [%s] BUTTON   GEvent[%s]\n",source.getText(),event ));
      g_status=STATUS.USR_DESCR_BOARD;
      // reset flag set in status USR_CONFIG_BOARD_A4P0_A
      phRaceDev.ignoreMessagesFromBoard(false); 
    }
}


public void g_pleaseWait_OkButton(GButton source, GEvent event) {
    if(event == GEvent.CLICKED ) {
      g_t.log(Trace.BASE, String.format("g_pleaseWait_OkButton() - [%s] BUTTON   GEvent[%s] -> NEXT Status:[%d]\n",source.getText(),event, g_waitUserOk_nextStatus ));
      //g_pleaseWait_OkButton.setVisible(false);
      UIC_removeUserMessage();
      g_status=g_waitUserOk_nextStatus;

    }
}


public void g_upgradeAvailable_button(GButton source, GEvent event) {
    if(event == GEvent.CLICKED ) {
      g_t.log(Trace.BASE, String.format("g_upgradeAvailable_button() - [%s] BUTTON   GEvent[%s]\n",source.getText(),event ));
      g_upgradeAvailable.setVisible(false);

      String url = g_upgrade.getPage();
      if(url != null && ! url.isEmpty()){
          g_t.log(Trace.LOWLEVEL, String.format("g_upgradeAvailable_button(): url[%s]\n",url ));
          // Open Link in default browser
          link(url);      
      }
    }
}


/**
 * GENERIC buttonEvents handler
 *   Intercepts BUTTON events for buttons that don't have a specific event handler
 *   like  DINAMICALLY created ones (ex: "Upload Software" buttons) 
 */
public void handleButtonEvents(GButton button, GEvent event) {
    g_t.log(Trace.DETAILED, String.format("handleButtonEvents() - Button[%s] tag[%s] GEvent[%s]\n",button.getText(), button.tag, event ));

    // UPLOAD Software button
    if(button.tag.indexOf("UPLOAD_") != -1 ) {
  
      if(event == GEvent.CLICKED ) {
        g_t.log(Trace.BASE, String.format("handleButtonEvents().UploadSoftware Button: tag[%s]\n",button.tag ));
        
        g_selectedHexfile = getUploadButtonHexfile("Upload Software Button",button.tag);
        if(g_selectedHexfile != null){
          g_t.log(Trace.LOWLEVEL, String.format("handleButtonEvents().UploadSoftware Button: hexfile[%s]\n",g_selectedHexfile ));
          g_status=STATUS.USR_UPLOAD_BOARD_WRITE;
          // cleanup textArea from las visualizad software description
          // it will be used to display avrdude output during upload.
          g_upListdescr.setText("", 780); 
          // Display user msg   
          UIC_setUserMessage("Uploading - please wait...");
        } // error output managed in getUploadButtonHexfile()
      }
      return;
    }
    
    if(button.tag.indexOf("URL_") != -1 ) { // tag[ URL_A4P0_0.9.6_NanoV3] GEvent[CLICKED]
  
      if(event == GEvent.CLICKED ) {
        g_t.log(Trace.BASE, String.format("handleButtonEvents().ReadMore Button: tag[%s]\n",button.tag ));
        
        String readMoreUrl = getUploadButtonUrl("Read More Button",button.tag);
        if(readMoreUrl != null){
          g_t.log(Trace.LOWLEVEL, String.format("handleButtonEvents().ReadMore Button Button: url[%s]\n",readMoreUrl ));
          // Open Link in default browser
          link(readMoreUrl);
        } // error output managed in getUploadButtonUrl()
      }
    } 
    
    
}


/**
 * Split UPLOAD TAG, get SoftwareList.Software
 * @return RaceId
 */
private SoftwareList.Software getSoftwareFromTag(String dStr, String bTag) { 

    String [] list = split (bTag, "_" );
    if(list.length != 4) {
      g_t.error(String.format("%s:getSoftwareFromTag() - split(%s, \"_\" ) ERROR - Expected 4 parts, Found [%d]\n", dStr, bTag, list.length));
      G4P.showMessage(this, dStr+ ": split(" + bTag + ", \"_\" ) ERROR - See Logfile" , "Software Error", G4P.ERROR_MESSAGE);
    } else {
      String uid = list[1].trim();
      String uve = list[2].trim();
      String upl = list[3].trim();
      SoftwareList.Software soft = softwareList.getSoftware(uid, uve, upl);
      if(soft.accuracy != 3) {
        g_t.error(String.format("%s:getSoftwareFromTag() - Id/Ver/Platform NOT FOUND:[%s,%s,%s]\n", dStr, uid, uve, upl));
        G4P.showMessage(this, dStr + ":  Id/Ver/Platform NOT FOUND ERROR - See Logfile" , "Software Error", G4P.ERROR_MESSAGE);
        return(null);    
      } else {   
        return(soft);
      } 
    } 
    return(null);    
} 

/**
 * Split UPLOAD TAG, get Hexfile
 * @return String hexfile
 */
private String getUploadButtonHexfile(String dStr, String bTag) { 
  SoftwareList.Software s = getSoftwareFromTag(dStr, bTag);
  if(s == null){
    return(null);
  }
  return(s.hexfile);
} 

/**
 * Split UPLOAD TAG, get Hexfile
 * @return String hexfile
 */
private String getUploadButtonUrl(String dStr, String bTag) { 
  SoftwareList.Software s = getSoftwareFromTag(dStr, bTag);
  if(s == null){
    return(null);
  }
  return(s.url);
} 
