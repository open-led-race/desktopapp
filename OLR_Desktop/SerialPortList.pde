/**
 * Get a list of VALID OS Ports 
 */
class SerialPortList {
  public SerialPort[] sList;

  private Trace t;
  private static final String _CLASS_ = "SerialPortList";
  
  SerialPortList(String bid, String logStr){
    if(logStr.equals("-") ){
      t = new Trace(Trace.NONE);
    } else {
      t = new Trace(logStr);
    }        
    t.setPublisherId(bid);
    t.setClassId(_CLASS_);
    t.log(Trace.METHOD, String.format("Constructor(%s)\n", logStr));  

    this.sList = new SerialPort[0];
    this.reloadSerialPortList();
  }
  
  /**
   *  Fills the available ports array with availables
   *  Ports retured by the OS (ports with some device 
   *  connected)
   */
  public int  reloadSerialPortList(){
    
    ArrayList<SerialPort> sl = new ArrayList<SerialPort >();
    SerialPort[] sp = SerialPort.getCommPorts();
    for(int i=0; i<sp.length;i++) {
      if(sp[i].isOpen() ){ // Skip Open Ports
        t.log(Trace.LOWLEVEL, String.format("PORT IN USE - discharged (Already Open): [%s][%s][%s]\n",sp[i].getSystemPortName(),sp[i].getDescriptivePortName(),sp[i].getPortDescription()));
        continue;
      }
      if(sp[i].getDescriptivePortName().indexOf("Physical") >= 0 ){ // Skip Phisical Ports - Linux
        t.error(String.format("reloadSerialPortList() - Phisical PORT (Linux)  - discharged: [%s][%s][%s]\n",sp[i].getSystemPortName(),sp[i].getDescriptivePortName(),sp[i].getPortDescription()));
      } else if(sp[i].getDescriptivePortName().indexOf("Intel(R)") >= 0 ){ // Skip Intel(R) Active Management Technology - SOL - Windows
        t.error(String.format("reloadSerialPortList() - Intel(R) PORT (Windows)  - discharged: [%s][%s][%s]\n",sp[i].getSystemPortName(),sp[i].getDescriptivePortName(),sp[i].getPortDescription()));
      } else {
        sl.add(sp[i]);
        t.log(Trace.LOWLEVEL, String.format("reloadSerialPortList() - Found:[%s][%s][%s]\n",sp[i].getSystemPortName(),sp[i].getDescriptivePortName(),sp[i].getPortDescription()));
      }
      
      
      
    }
    sList = sl.toArray(new SerialPort[0]);
    return(sList.length);
  }

  public SerialPort getPort(int idx){
    return(sList[idx]);
  }

  public SerialPort [] getPortList(){
    return(sList);
  }

  /**
   *  Returns a string with the cList content
   */
  String toString(){
    StringBuilder str = new StringBuilder();
    str.append("sList:\n");
    for(int i=0; i<sList.length;i++) {
      str.append( String.format("  sList[%d] - [%s][%s][%s]\n", i, sList[i].getSystemPortName(),sList[i].getDescriptivePortName(),sList[i].getPortDescription()) );
    }
    return(str.toString());
  }

}   
  
  
