# Check parameter(s)
case $# in
	2)
	HEXFILE=$1
	PORT=$2
	;;
	*)
		echo "Usage: $0 HexFileName Port"
		echo "   Example: $0 hexdir/myprogram.ino /dev/ttyUSB0"
		exit 1
	;;
esac

BASE=/mnt/data/Seafile/Sync_data/Buka/Progetti/SingularDevice/OpenLedRace/Code/OLRConfig/data/

${BASE}bin/linux/avrdude -C${BASE}bin/linux/avrdude.conf -v -patmega328p -carduino -P${PORT} -b57600 -D -Uflash:w:${BASE}/${HEXFILE}:i 

