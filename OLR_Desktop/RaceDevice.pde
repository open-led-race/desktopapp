
class RaceDevice extends PhysicalDevice {
  int startHere ;
  int laps;
  int repeat;
  int finishHere;
  int outTunnelNotifyPos;
  String  currentPhaseCode;
     
  private boolean configured;
  
  
  private Trace t;
  private static final String _CLASS_ = "RaceDevice";
  
  private static final int DEFAULT_OUTTUNNELNOTIFYPOS = 10;

    RaceDevice(String sname, JSONObject pd, String logStr) {
      super (sname,pd,logStr);
      this.startHere = -1; 
      this.laps = -1;
      this.repeat = -1 ;
      this.finishHere = -1 ;
      this.outTunnelNotifyPos = -1;
      this.currentPhaseCode = null;

      String boardId  = getJSONStringOrExit(pd,"id");
      
      if(logStr.equals("-") ){
        t = new Trace(Trace.NONE);
      } else {
        t = new Trace(logStr);
      }      
      t.setPublisherId(boardId);
      t.setClassId(_CLASS_);
      t.log(Trace.METHOD, String.format("Constructor(pd, %s)\n",logStr));

      configured=false;
  }   
    
    void setRaceParams(int p, int l, int r, int f, int o) {
      
      // fare check values -> ret errstr on error 
      
      this.startHere = p; 
      this.laps = l;
      this.repeat = r ;
      this.finishHere = f ;
      this.outTunnelNotifyPos=o;

      configured=true;
      return;
    }   

    void setRaceParams(int p, int l, int r, int f) {
      setRaceParams(p, l, r, f, DEFAULT_OUTTUNNELNOTIFYPOS);
    }


  /**
   * set current Race Phase (= Race Phase of the connected Board)
   */
  void setRacePhase(String p){
    this.currentPhaseCode = p;
  }  

  /**
   * get current Race Phase (= Race Phase of the connected Board)
   */
  String getRacePhase(){
    return(currentPhaseCode);
  }  
  
  
  public void sendConfig() {
    t.log(Trace.METHOD + Trace.DETAILED, String.format("sendConfig(): sending cfg to [%s][%s][%s]\n", this.port, super.name, super.description));
    if(! configured){
      t.error("sendConfig(): not yet configured (missing setRaceParams() call ?)");
      return;
    }
    
    sendRacePhase(ProtocolSerial.Cmd.RacePhase.Phase.ENTERCFG);
    
    String str = ProtocolSerial.Cmd.SetConfig.getCommand(this.startHere,this.laps,this.repeat,this.finishHere);
    sendString(str);

    // TODO LUCA: manca mandare OutTunnelDistanceNotify in cmd separato 
    
    return;
    
  }
  


  /**
   * send Race Status command to the board 
   */
  void sendRacePhase(String sCode){
    t.log(Trace.METHOD + Trace.DETAILED, String.format("sendRacePhase(): sending [%s] to [%s][%s][%s]\n", sCode, this.port, super.name, super.description));

    String str = ProtocolSerial.Cmd.RacePhase.getCommand(sCode);
    if(str != null) {
      sendString(str);
      setRacePhase(sCode);
    }
    return;
  }



  
  /**
   *
   */
  void sendCarEnter(int carN, int speed){
    String str = ProtocolSerial.Cmd.CarEnter.getCommand(carN,speed);
    if(str != null) {
      sendString(str);
    }
    return;
  }

  /**
   *
   */
  void sendCarWin(int carN){
    String str = ProtocolSerial.Cmd.CarWin.getCommand(carN);
    if(str != null) {
      sendString(str);
    }
    return;
  }
  

}
  
