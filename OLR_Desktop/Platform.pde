
class Platform  {

    String   id ;
    String   name ;
    String   idString ;
    String   portName;
    String   avrdude ;
    String [] avrdudeParams ;
    
    String           osName;
    String           localPath;
    private boolean  isSet ;
    boolean          hexUploadStarted;  
    private double   hexUploadStartTime;
    private double   hexUploadTimeout;
    private Process  uploadProcess ;
    BufferedReader   procOutput;

    JSONArray  jsonPList=null;  
    int        pNum=0;
    
   private Trace t;
   private static final String _CLASS_ = "Platform";
  
  // Constructor
  Platform(String _osName, String _localpath, String pl_fname, String logStr) {

      if(logStr.equals("-") ){
        t = new Trace(Trace.NONE);
      } else {
        t = new Trace(logStr);
      }      
      t.setClassId(_CLASS_);
      t.log(Trace.METHOD, String.format("Constructor(Logstring:%s)\n",logStr));

      JSONObject jsonPlatformList=null;
      
      try {
        jsonPlatformList = loadJSONObject(pl_fname);
      } catch (RuntimeException e) {  
        System.err.printf("Error loading Platform cfg file:[%s] - [%s]\n ", pl_fname, e);
        noLoop();
        exit();
      }
      this.jsonPList = jsonPlatformList.getJSONArray("Platform");
      if(this.jsonPList == null){
         t.error(String.format("Error! Json cfg file - Missing \"Platform\" section."));
         exit();
      }
      
      t.log(Trace.LOWLEVEL, String.format("Constructor() - Loaded JSON cfg file[%s]:\n%s\n",pl_fname,this.jsonPList.toString()));
      
      this.osName = _osName;
      this.localPath = _localpath;
      this.pNum = this.jsonPList.size();
      this.isSet = false;
      this.hexUploadStarted = false;  
      this.hexUploadStartTime = 0;
      this.hexUploadTimeout = 0;
      this.uploadProcess = null;
      this.procOutput = null;  
      
  }   


  /*
   *  
   */
  boolean setPlatform(String portName, String portDescription) {
    t.log(Trace.DETAILED, String.format("setPlatform(%s,%s)\n", portName, portDescription));   

    int nFound=0;
    int idx = -1;
    for (int i = 0; i < this.pNum; i++) {
      JSONObject jpl = this.jsonPList.getJSONObject(i); // Platform[i]
      
      //String idString = getJSONStringOrExit(jpl, "idString");
      JSONArray  idStringArr = getJSONArrayOrExit(jpl, "idString");
      int idN = idStringArr.size();
      for(int j=0; j<idN;j++) {
        String idString = idStringArr.getString(j);
        t.log(Trace.DETAILED, String.format("setPlatform() -> Read config Json obj -> idString: [%s]\n", idString));   
        if(portDescription.toUpperCase().indexOf(idString.toUpperCase()) > -1) { 
          t.log(Trace.DETAILED, String.format("setPlatform() -> Found idString [%s] in portDescription [%s]\n", idString, portDescription));   
          nFound++;
          idx=i;
        }
      }
    }
    
    if(nFound==0){
      return(false);
    } else if (nFound==1){
      JSONObject jpl = this.jsonPList.getJSONObject(idx); // Platform[i]
      t.log(Trace.DETAILED, String.format("setPlatform() -> Get Platform JSON obj for port [%s:%s]:\n%s\n", portName, portDescription, jpl.toString() ));      

      this.id = getJSONStringOrExit(jpl, "id");
      this.name = getJSONStringOrExit(jpl, "name");
      this.idString = getJSONStringOrExit(jpl, "idString");
      this.avrdude = getJSONStringOrExit(jpl, "avrdude");
      JSONArray params = getJSONArrayOrExit(jpl, "avrdudeParams");
      int pNum = params.size();
      avrdudeParams = new String[pNum];
      for (int j = 0; j < pNum; j++) {
        avrdudeParams[j] = params.getString(j);
      }
      
      // portName note:
      //   SerialPortlist class uses [com.fazecast.jSerialComm.SerialPort.getSystemPortName()]
      //   The String returned by this method 'strips' the "/dev/" part from the port name
      //   so we have just "ttyUSB0" in "portName" parameter.
      //   But avrdude needs the full device name in the "-P" parameter
      if(this.osName.indexOf("nux") >= 0){
        this.portName = "/dev/" + portName; 
      } else if(this.osName.indexOf("win") >= 0){
        this.portName = portName; 
      } else {
        t.error( String.format("setPlatform() -> Unmanaged OS name:[%s]\n",this.osName ));      
      }
      
      if( ! patchAvrdudeSettings()){
        return(false); // errors in avrdude config parameters
      }
      
      t.log(Trace.BASE, String.format("setPlatform() -> Board at port [%s:%s] identified as [%s][%s]\n", portName, portDescription, this.id, this.name ));      
      this.isSet = true;
      return(true);
      
     } else {
      t.error(String.format("setPlatform() -> error in JSON cfg file: Multiple [idString] associatons found for portDescription [%s]\n", portDescription));   
      return(false);
     }
  }
  
  
  /*
   * Check mandatory placeholders in avrdude configuration parameters (loaded from cfg file)
   * Replace __LOCALPORT__, __LOCALPATH__ and __OSNAME__  placeholders
   */
  boolean patchAvrdudeSettings() {

    // patch avrdudeParams configuration value
    for(int i=0; i<this.avrdudeParams.length; i++) {
      this.avrdudeParams[i]=this.avrdudeParams[i].replace(PLACEHOLDER.LOCALPORT,this.portName);
      this.avrdudeParams[i]=this.avrdudeParams[i].replace(PLACEHOLDER.LOCALPATH,this.localPath);
      this.avrdudeParams[i]=this.avrdudeParams[i].replace(PLACEHOLDER.OSNAME,this.osName);
    }

    // patch avrdude configuration value
    this.avrdude=this.avrdude.replace(PLACEHOLDER.LOCALPATH,this.localPath);
    this.avrdude=this.avrdude.replace(PLACEHOLDER.OSNAME,this.osName);

    return(true);
  }
  
  
  /*
   *  On first call:
   *    - "Starts" the external executable implementing the "upload" of the "hexfile"
   *      received as a parameter.
   *  On next calls:
   *    - Returns the output of the external program or "null"
   *      if there is no output available.
   *
   *  This method should be called in a loop, in conjunction with "uploadDone()" 
   * 
   */
  String upload(String hexfile) throws IOException {
    if(! this.isSet){
      uploadReset();
      return(new String("upload()->ERROR: Platform NOT Set!") );
    }

    if( ! this.hexUploadStarted ) { // first loop only -> run process
      this.hexUploadStarted = true;

      t.log(Trace.LOWLEVEL,String.format("upload()->avrdude:[%s]\n", this.avrdude));
      t.log(Trace.LOWLEVEL,String.format("upload()->avrdudeParams:[%s]\n", Arrays.toString(this.avrdudeParams)));

      // Create ProcessBuilder Parameters (avrdudeExecutable, parameters)
      ///////////////////////////////////////////////////////////////////
      List<String> pbParams = new ArrayList<String>();
      pbParams.add(this.avrdude);
      t.log(Trace.LOWLEVEL,String.format("upload()->ProcessBuilder param[%d]:[%s]\n", 0, pbParams.get(0) ));
      for(int i=0; i<this.avrdudeParams.length; i++){
        // add parameter to the list:
        //   - trim():
        //     trailing spaces are interpreted by  ProcessBuilder() as
        //     empty parameters (avrdude through "unknown parameter --")
        //   - replace(PLACEHOLDER.HEXFILE
        //     The parameter for hexfilename (-U) still contains the __HEXFILE__ placeholder.
        //     and __HEXFILE__ config string contains __LOCALPATH__ placeholder
        pbParams.add(this.avrdudeParams[i].trim().replace(PLACEHOLDER.HEXFILE,hexfile).replace(PLACEHOLDER.LOCALPATH,this.localPath));
        t.log(Trace.LOWLEVEL,String.format("upload()->ProcessBuilder param[%d]:[%s]\n", i+1, pbParams.get(i+1) ));
      }
      
      hexUploadStartTime = System.currentTimeMillis(); // Start timer for Upload operation
      t.log(Trace.DETAILED,String.format("upload()->Starting upload process at: [%f]\n", hexUploadStartTime));
      
      // Start process for avrdude 
      ProcessBuilder pb = new ProcessBuilder(pbParams);     
      //ProcessBuilder pb = new ProcessBuilder("ls","-alR");
      
      pb.redirectErrorStream(true);
      uploadProcess = pb.start();

      this.procOutput = new BufferedReader(new InputStreamReader(this.uploadProcess.getInputStream()));
      t.log(Trace.DETAILED,String.format("upload()->Upload process Started at: [%f]\n", (double) System.currentTimeMillis()));
      return(new String("Upload started:"));
    } // first loop only

    String line;
    if((line = this.procOutput.readLine()) != null) {
      return(line);
    }
    
    return(null); 
  
  }
  
  /*
   *  
   */
  boolean uploadDone()  {
    if(uploadProcess.isAlive()){
      return(false);
    }
    try {
      if(this.procOutput.ready()) { // Process done but there are still output chars to read.
        return(false);
      }
    } catch (IOException ioe) {  
        g_t.error(String.format("uploadDone:Error on this.procOutput.ready():[%s]\n ", ioe));
    }
    return(true);
  }
  

  /*
   *
   */
  int uploadExitValue() {
    return(uploadProcess.exitValue());
  }
  
  /*
   *
   */
  void uploadReset() {
      this.hexUploadStarted = false;
  }
  
  
  
}
