/**
   ____                     _      ______ _____    _____                
  / __ \                   | |    |  ____|  __ \  |  __ \               
 | |  | |_ __   ___ _ __   | |    | |__  | |  | | | |__) |__ _  ___ ___ 
 | |  | | '_ \ / _ \ '_ \  | |    |  __| | |  | | |  _  // _` |/ __/ _ \
 | |__| | |_) |  __/ | | | | |____| |____| |__| | | | \ \ (_| | (_|  __/
  \____/| .__/ \___|_| |_| |______|______|_____/  |_|  \_\__,_|\___\___|
        | |    
        |_|    
                                                  
 Open LED Race Desktop: Upload and Configuration board firmware
 
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 2020/12/26 - Ver 1.0.0
   --see changelog.txt
 */

import java.util.Set;
import java.util.Formatter;
import java.util.Iterator;
import java.util.Deque;
import java.util.ArrayDeque;
import java.util.List;
import java.util.Arrays;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ArrayBlockingQueue;

import java.lang.System.*;
import java.io.IOException;
import java.io.File;
import java.io.OutputStream ;
import java.io.FileOutputStream; 
import java.io.PrintStream;
import java.io.InputStreamReader;

import com.fazecast.jSerialComm.*;  
import g4p_controls.*;    
import http.requests.*;



///////////////////////////////////////////
////////  Compile-time Parameters  ////////
///////////////////////////////////////////
final String thisOlrManagerVersion  = "1.0.2";  // This Software Version

final String dataPath               = "data";         // [dataDir] !!! __DO NOT__ add a path separator at the end of the string !!!
final String jsonFileName           = "config.json";  // Main Configuration file 

// Timeouts
final double  lookupInterval        = 3000;  // delay between port lookup when no connected board found
final double  identifyTimeout       = 12000; // OLR firmware 0.9.5 have a blocking DELAY() instructions in the countdown phase -> use 10000 or more
final double  tmpUserMsgTimeout     = 5000;  // Timeout for user messages  
final double  saveTimeout           = 10000; // Timeout for "Save Board Configuration" 

// [frameRate]:                                                   //
//   setup()--->frameRate(frameRate);                             //
// -------------------------------------------------------------  //
// The default value (120) is usually ok
// Usually the first clue of an insufficient framerate            //
// is when we see a "delay" in Serial Communications              //
// received FROM Board                                            //
final int frameRate = 120;

///////////////////////////////////////////
///////////////////////////////////////////


// Placeholder used in JSON Config files (Platform List, Software List)
static class PLACEHOLDER {
  public static final String LOCALPATH             = "__LOCALPATH__";
  public static final String LOCALPORT             = "__LOCALPORT__";
  public static final String HEXFILE               = "__HEXFILE__";
  public static final String OSNAME                = "__OSNAME__";
}

// FSM Status
static class STATUS {
  public static final int START_OVER                 = 0;
  public static final int BOARD_LOOKUP               = 1;
  public static final int BOARD_IDENTIFY_PLATFORM    = 2;
  public static final int BOARD_IDENTIFY_SOFTWARE    = 3;
  public static final int USR_DESCR_BOARD            = 4;
  public static final int USR_UPLOAD_BOARD           = 5; 
  public static final int USR_UPLOAD_BOARD_WRITE     = 6; 
  public static final int USR_UPLOAD_BOARD_WRITE_MSG = 7; 

  // Configuration software 
  public static final int USR_CONFIG_BOARD_NONE      = 20; // No config option for the firmware running on the connected board
  // Configuration for Arduino Software A4P0 / our 4 Players code
  public static final int USR_CONFIG_BOARD_A4P0_A      = 21; // Status for the UI
  public static final int USR_CONFIG_BOARD_A4P0_A_SAVE = 22; // Status to save values to Board

  public static final int USR_CONFIG_BOARD_xxxx      = 31; 
  
  public static final int USER_MESSAGE_TIMEOUT       = 97;
  public static final int USER_MESSAGE_WAIT_USER_CONFIRMATION  = 98;
  public static final int RESTART_APPLICATION        = 99;

}


// The present source PLUS [UIComponents.pde] + [UIHandlers.pde] represents the "main" scope
// for this app
// Any other source contains a CLASS with its own methods and variables
// Vars defined here are "GLOBAL" for the present source, [UIComponents.pde] and [UIHandlers.pde] 
// If the name of the variable starts with "g_" prefix,  
// it means is used in more than one of the three source files

boolean        g_serialPortFound = false;  // Y|N
SerialPort     g_serialPort = null;        // Serial Port where the Board is connected 
SerialPort     g_ports[];                  // List of available serial ports 
int            g_status;                   // Program current status
int            g_configStatus;             // Status for Config Board (used to choose the method to configure the specific software running on the Board found) 
int            g_saveConfigStatus;         // Status for Save Config to Board (used to choose the method to save configuration for the specific software running on the Board found)
int            g_waitUserOk_nextStatus;    // Status where to 'jump' after a blocking user message
Trace          g_t;                        // Trace/Log Obj 
String         g_CLASS_ = "UI";            // Class Name used in Trace/Log for global "g_t" object
String         g_selectedHexfile = null;  
String         g_upgradeURL = null;


SerialPortList           serialPortList;         // OBJ instance used to get list of Available (Valid) OS serial ports 
RaceDevice               phRaceDev;              // RaceDevice->PhisicalDevice - OBJ instance implementing communication with the Firmware runninmg on the board   
SwConfig_A4P0            cfgA4P0;                // OBJ instance implementing UI Configuration for "A4P0" firmware (our 4 Players software)
SoftwareList             softwareList;           // OBJ instance for "Available Firmware" list - ID, Name, Description, hexfile, etc (load data from JSON cfg)
SoftwareList.Software    g_currentBoardSoftware; // OBJ instance containing ID,Name,Description,etc for the firmware found on the connected board 
Platform                 g_platform;             // OBJ instance for "Available Platforms" list (load data from JSON cfg)
AsyncGetAvailabelUpgrade chkUpgrade;             // OBJ instance to check for available upgrade online (load data from JSON cfg)
UpdaterAPI.Upgrade       g_upgrade;              // OBJ instance to containig available upgrade data (if any available)


boolean        configFileExist = false;
String         cfgFileName = null;

int            lastLookupListLength = 0;
double         lastPortLookupTime;  
double         portOpenTime = 0;
boolean        identifyStarted = false;
double         identifyStartTime = 0;
boolean        saveStarted = false;
double         saveStartTime = 0;
boolean        hexUploadForcePortResetDone = false;
boolean        hexUploadStarted = false;
double         hexUploadStartTime = 0;
boolean        tmpUserMsgStarted = false;
double         tmpUserMsgStartTime = 0;

boolean        g_boardCfgLoaded = false;  
boolean        uploadListLoaded = false;

boolean        chekUpgradeDone = false;  

JSONObject     jsonMainCfg=null;             // JSON object instance loaded from jsonFileName (Main Configuration file) 
JSONObject     jsonMainCfg_Loglevel = null;  // JSON object instance containing Loglevels for the different modules (loaded from Main Configuration file)

int            prevMouseRow=-2; 

  
public void setup(){
  
  cfgFileName = sketchPath(dataPath) + File.separator + jsonFileName;
  File f = dataFile(cfgFileName);
  boolean exist = f.isFile();
  if(! exist) {
    System.err.printf("Missing configuration file:[%s]\n ", cfgFileName);
    noLoop();
    exit();
  }
  
  frameRate(frameRate);
  configFileExist = true;
  loadConfig(); // instantiates OBJs with values found in JSON config files  
  createUI();  
  
  g_status=STATUS.BOARD_LOOKUP;
  g_configStatus=STATUS.USR_CONFIG_BOARD_NONE;
  g_waitUserOk_nextStatus = STATUS.BOARD_LOOKUP;
  
  // Start thread checking for Upgrades 
//  chkUpgrade.start();
  
}



/*
 *  Draw loop.
 *  Main switch for current Status (FSM)
 */
public void draw(){
  background(#e5e9ed);
  UIC_displayYourBoardFrame();

  // Call "phRaceDev.loop()" to READ from Board (serial)
  //   "phRaceDev.loop()" is non blocking - Single chars received 
  //   from Serial are grouped in "commands" and stored in a Queue
  phRaceDev.loop();   
  
  if(! chekUpgradeDone) {
    checkUpgrade();
  }


  // FSM
  switch (g_status) {
    /////////////////////////////////////////       
    case STATUS.START_OVER :
      lastPortLookupTime=0;
      g_serialPortFound = false;  
      lcBoardInfo_label.setVisible(false);
      lcBoardInfo_text.setVisible(false);
      
      g_optConfig.setVisible(false);    
      g_grpOpt.setVisible(false);        
      g_upListdescr.setVisible(false);
      
      g_serialPort = null;  // Serial Port of the OLR Board
      identifyStarted = false;
      saveStarted = false;
      hexUploadForcePortResetDone = false;
      hexUploadStarted = false;
      resetUploadList();
      g_status = STATUS.BOARD_LOOKUP;
      phRaceDev.ignoreMessagesFromBoard(false); 

      break;
    
   
    /////////////////////////////////////////       
    case STATUS.BOARD_LOOKUP:
      phRaceDev.ignoreMessagesFromBoard(false); 
      findPort();
      if(g_serialPortFound) { // g_serialPortFound is set to true in findPort() or in UIHandlers::handleDropListEvents() callback
        g_status=STATUS.BOARD_IDENTIFY_PLATFORM;
      }
      break;

      
    /////////////////////////////////////////       
    case STATUS.BOARD_IDENTIFY_PLATFORM:
      // Look if the board in the PortDescription is listed in 
      // the JSON config file (look if it's a known platform)
      
      //if( g_platform.setPlatform(g_serialPort.getSystemPortName(), g_serialPort.getDescriptivePortName())) {
      if( g_platform.setPlatform(g_serialPort.getSystemPortName(), g_serialPort.getDescriptivePortName() + g_serialPort.getPortDescription() )) {
        g_status=STATUS.BOARD_IDENTIFY_SOFTWARE;
        lastPortLookupTime = 0;
      } else {
        UIC_setUserMessage(String.format("Board [%s] not managed by this software",g_serialPort.getDescriptivePortName()));
        if(userMessageTimeElapsed()){
          g_status=STATUS.BOARD_LOOKUP; // Jump back 
        }      
      }
      break;

      
    /////////////////////////////////////////       
    case STATUS.BOARD_IDENTIFY_SOFTWARE:
      if( ! identifyStarted ) { 
        identifyStarted = true;
        lcBoardInfo_label.setVisible(true);
        lcBoardInfo_text.setText(new String("Checking..."));
        lcBoardInfo_text.setVisible(true);
        UIC_setUserMessage("Getting Board Software Version...");
        identifyStartTime = System.currentTimeMillis(); // Start timer for identifyBoard operation
      }   
      int res = boardIdentified();
      if(res == 1) { 
        // found or timeout
        lcBoardInfo_text.setText(getSoftwareBaseDescr());
        // To be able to run the cfg board Status, an accuracy=3 is needed
        g_configStatus = STATUS.USR_CONFIG_BOARD_NONE;
        if(g_currentBoardSoftware.accuracy == 3){
          g_configStatus=getConfigBoardStatus();  // Check if Software has a Config Method available
          if(g_configStatus != STATUS.USR_CONFIG_BOARD_NONE){
            g_optConfig.setVisible(true);  // Config Method for the Firmware running on the Board available
          } 
        }
        g_grpOpt.setVisible(true);       // Set others User Options visible
        loadSoftwareDescription();       // Load Software Descrition in UI field
        g_status=STATUS.USR_DESCR_BOARD; // Jump to Board Description Statue    
        
      } else if (res == -1) {
        String msg = String.format("Unrecoverable error Identifying software - see log file\n");
        g_t.error(msg);   
        g_status=STATUS.USER_MESSAGE_WAIT_USER_CONFIRMATION;     
        g_waitUserOk_nextStatus = STATUS.START_OVER; // Start over (open port, etc)
        UIC_setUserMessage(msg);
      }
      g_pleaseWait.loop();
      break;


    /////////////////////////////////////////         
    case STATUS.USR_DESCR_BOARD:
      phRaceDev.ignoreMessagesFromBoard(true); 
      stripView.setVisible(false);
      pnlCfg_4Pv1.setVisible(false);
      g_upListdescr.setVisible(false);
      g_boardDescr.setVisible(true);
      g_optDescribe.setSelected(true);
      UIC_removeUserMessage();
      resetUploadList();
      //g_boardCfgLoaded=false;
      resetBoardCfgLoaded();  // force Reload Config on next "Configure"
      break;

       
    /////////////////////////////////////////       
    case STATUS.USR_UPLOAD_BOARD:  
      //g_boardCfgLoaded=false;
      resetBoardCfgLoaded();  // force Reload Config on next "Configure"      
      stripView.setVisible(false);
      //g_user_infotext.setVisible(false);
      UIC_removeUserMessage();
      pnlCfg_4Pv1.setVisible(false);
      g_boardDescr.setVisible(false);
      g_optUpload.setSelected(true);
      g_upListdescr.setVisible(true);

      if( ! uploadListLoaded) {  
        phRaceDev.ignoreMessagesFromBoard(true); 
        uploadListLoaded=true;
        String newList[] = uiGetUploadList();      
        g_upListArea.fillGrid(newList);
        g_upListArea.setTitle(" Available Software", 14);
        g_upListArea.setFirstRowAsHeader();
      }
      // Update Upload area visualization 
      g_upListArea.showGrid();
      if( g_upListArea.curMouseRow != prevMouseRow) {
         prevMouseRow = g_upListArea.curMouseRow;
        if(g_upListArea.curMouseRow != -1) {
            g_upListdescr.setText(softwareList.getUploadableFullDescription(g_upListArea.curMouseRow -1, g_platform.id), 780); // index "-1"  because the first row in g_upListArea is the Header
        }
      }
      if(g_upListArea.curMouseRow == -1) g_upListdescr.setVisible(false);
      break;


    /////////////////////////////////////////       
    case STATUS.USR_UPLOAD_BOARD_WRITE :   
    
      if (! hexUploadForcePortResetDone) {
        resetUploadList();
        int done = phRaceDev.closeCurrentSerialPort_force1200bpsReset(); // needed for arduino Every (works ok with Nano as well)
        if(done == -1 ) {
            g_t.error("STATUS.USR_UPLOAD_BOARD_WRITE->closeCurrentSerialPort_force1200bpsReset() Error!\n");
            UIC_setUserMessage("Error preparing to Upload - See logfile");
            g_status=STATUS.USER_MESSAGE_WAIT_USER_CONFIRMATION; // Jump to User Message
            g_platform.uploadReset(); // make upload() ready for next call (clear internal status)  
            g_waitUserOk_nextStatus = STATUS.START_OVER;
            break;
        } else if(done == 0) {
            break; // reset not yet completed 
        }
        hexUploadForcePortResetDone=true;
      }
    
      if( ! hexUploadStarted ) { // one time only
        hexUploadStarted = true;
        hexUploadStartTime = System.currentTimeMillis(); // Start timer for Save operation  
        break; // g_platform.upload() will be called on next draw() loop
      }   
      try {
        String line = g_platform.upload(g_selectedHexfile);  
        if(line != null) {
          // display command output
          g_upListdescr.appendText(line);
          g_t.log(Trace.ALL, String.format("Upload Software->%s\n", line));
        }
        if(g_platform.uploadDone()) {
          String msg;
          int error = g_platform.uploadExitValue();
          if(error !=0){
            msg = String.format("Error Uploading [%d] - see log file\n ", error);
            g_t.error(msg);   
          } else {
            msg = String.format("Upload Done\n \nReboot the Board (Disconnect USB connector)");
          }
          g_status=STATUS.USER_MESSAGE_WAIT_USER_CONFIRMATION;     
          g_waitUserOk_nextStatus = STATUS.START_OVER; // Start over (open port, etc)
          UIC_setUserMessage(msg);
          g_platform.uploadReset(); // make upload() ready for next call (clear internal status)           
        } 
      } catch (IOException ioe) {  
        g_t.error(String.format("IOException Error Uploading software:[%s]\n ", ioe));
        UIC_setUserMessage("IOException Error Uploading - See logfile");
        g_status=STATUS.USER_MESSAGE_WAIT_USER_CONFIRMATION; // Jump to User Message
        g_waitUserOk_nextStatus = STATUS.START_OVER; // Start over (open port, etc)  
        g_platform.uploadReset(); // make upload() ready for next call (clear internal status) 
      }
      diplayUserMessage();
      break;
   
   
    /////////////////////////////////////////
    case STATUS.USR_CONFIG_BOARD_A4P0_A:
      resetUploadList();
      g_boardDescr.setVisible(false);
      g_upListdescr.setVisible(false);
      if( ! g_boardCfgLoaded){
        phRaceDev.ignoreMessagesFromBoard(false); 
        diplayUserMessage();
        g_pleaseWait.loop();
        g_boardCfgLoaded=cfgA4P0.readBoardCfg();
        if(g_boardCfgLoaded){ // Just loaded
          UIC_removeUserMessage();          
          // Get Platform-specific configuration parameter "maxled"
          // This will adjust "Strips Number" slider limits when calling UIC_set4PlayersConfigUIValues()
          int nled = g_currentBoardSoftware.pCfgparams.getInt("maxled", -1);
          if(nled == -1){
            nled = cfgA4P0.MAXPLED;
          } 
          cfgA4P0.setMaxPLed(nled);
          
          if(UIC_needExpertMode()){
            g_t.log(Trace.ALL, String.format("maxLed loaded from Board [%d] not a multiple of [%d] --> Advanced Config enabled\n", cfgA4P0.maxLed, g_singleStripLEDNumber));
            UIC_setExpertMode(true);
          } else {
            UIC_set4PlayersConfigUIValues(); // Set UI controls according to values loaded in memory (cfgA4P0.x) and Check if Expert Mode have to be activated
            cfgA4P0.visualization.stp.resetStrip();
            stripView.setVisible(true);
          }
          pnlCfg_4Pv1.setVisible(true);
          // UI controls in pnlCfg_4Pv1 Panel send commands to board
          // to update LED strip (BoxLen, Slope config, etc)
          // Board answer with OK|NOK messages
          // They have to be discharged to not interfere
          // with SAVE CONFIGURATION coming next
          phRaceDev.ignoreMessagesFromBoard(true);           
        }
        break;
      }
     if( ! cfgA4P0.isExpertModeActive()){
      cfgA4P0.visualization.stp.update(); // Update Strip Visualization
       stripView.setVisible(true);
     }  else {
       stripView.setVisible(false);
     }

      break;
      
      
    /////////////////////////////////////////  
    case STATUS.USR_CONFIG_BOARD_A4P0_A_SAVE:
      if( ! saveStarted ) { // first loop only
        // Stop discharging messages from board  
        phRaceDev.ignoreMessagesFromBoard(false); 
        saveStarted = true;
        stripView.setVisible(false);
        pnlCfg_4Pv1.setVisible(false);
        UIC_setUserMessage("Saving...");
        saveStartTime = System.currentTimeMillis(); // Start timer for Save operation 
      }
      diplayUserMessage();
      g_pleaseWait.loop();

      int saveres=boardConfigSaved();
      if(saveres == 0)         break;                                         // still saving
      else if(saveres == 1)    UIC_setUserMessage("Board Configured");            // Save OK
      else if (saveres == -1)  UIC_setUserMessage("Error Configuring Board!!!");  // Save terminates with errors or timeout
      
      resetBoardCfgLoaded();  // force Reload Config on next "Configure"
      g_status=STATUS.USER_MESSAGE_WAIT_USER_CONFIRMATION;     
      g_waitUserOk_nextStatus = STATUS.USR_DESCR_BOARD; // will Jump to Board Description Statue
      break;
      
   
    /////////////////////////////////////////
    case STATUS.USER_MESSAGE_WAIT_USER_CONFIRMATION:
      diplayUserMessage();
      g_pleaseWait_OkButton.setVisible(true); // User need to click on "OK" button to change state (see the OK button handler in UIHandlers)
      break;
    
    
    /////////////////////////////////////////
    case STATUS.USER_MESSAGE_TIMEOUT:
      if(userMessageTimeElapsed()){
        g_status=STATUS.USR_DESCR_BOARD; 
      }
      break;

  } // switch


}

/* 
 *
 */
 void checkUpgrade() {
/*   
  // "chkUpgrade" starts in a separate thread in setup()
  if(chkUpgrade.status == chkUpgrade.SUCCESS) {        // GOT Answer from Upgrade Server   
    chekUpgradeDone=true;  // done only once at startup 
    g_t.log(Trace.INTERNALS,"Check Upgrade Done\n" );   
    g_upgrade = chkUpgrade.get(); // get values returned from server api
    if(g_upgrade.getTotFound() > 0) {
      g_t.log(Trace.BASE, String.format("Available Download Found-> [%s]\n", g_upgrade.toString()));
      // Upgrade found: Show UI button - see UIComponents:g_upgradeAvailable Button, UIHandlers.g_upgradeAvailable_button()
      g_upgradeAvailable.setVisible(true); 
    }
  } else if(chkUpgrade.status == chkUpgrade.FAILURE) {  // "Available Ugrade Server" error  (no network available, etc) 
    chekUpgradeDone=true; // done only once at startup
    g_t.error("Error checking for Upgrades (Network problem ?)\n");
  }
  
*/  
 }


/**
 * Non-blocking (to be called in a loop) 
 * Scan Available serials and update the Droplist (or SET the port if only one found)
 */
void findPort() {
  
  // Complete scan only each "lookupInterval" millis
  if(lastPortLookupTime==0 || ( System.currentTimeMillis() > lastPortLookupTime + lookupInterval)) {  
    lastPortLookupTime = System.currentTimeMillis();  
    String[] item;
    lastLookupListLength = serialPortList.reloadSerialPortList(); // (re)Load current serial port list
    g_t.log(Trace.LOWLEVEL, String.format("findPort(): Port list (re)Loaded:\n%s", serialPortList.toString()));
    g_ports = serialPortList.getPortList();  // Get list of VALID Ports
  
    if(lastLookupListLength < 1) {           // No Board conected
      item = new String[1];
      item[0] = new String("No OLR Board Found - Please Connect You OLR to the Computer");
      lcDroplist.setItems(item, 0); // Upd droplist in UI   
      
    } else if(lastLookupListLength == 1) {   // One board detected
      String itemStr = new String(String.format("[%s] [%s]\n", g_ports[0].getSystemPortName(), g_ports[0].getDescriptivePortName()) );
      g_t.log(Trace.LOWLEVEL, String.format("findPort():Found just one port->[%s][%s][%s]\n", g_ports[0].getSystemPortName(),g_ports[0].getDescriptivePortName(),g_ports[0].getPortDescription()));
      g_serialPortFound = true;  
      g_serialPort = g_ports[0];
      // Remove droplist and set/show selected port in UI
      lcSelectedPort_label.setText(itemStr);
      lcDroplist.setVisible(false);
      lcSelectedPort_label.setVisible(true);
      
      } else {                                // More than one board detected
        item = new String[lastLookupListLength + 1];
        item[0] = new String("Please select the Board you want to Configure...");
        for(int i = 0; i<lastLookupListLength; i++){
          g_t.log(Trace.ALL, String.format("findPort():Found port->[%s][%s][%s]\n", g_ports[i].getSystemPortName(),g_ports[i].getDescriptivePortName(),g_ports[i].getPortDescription()));
          String dscr_a = new String(g_ports[i].getDescriptivePortName());
          String dscr_b = new String(g_ports[i].getPortDescription());
          // g_serialPort.getDescriptivePortName() + g_serialPort.getPortDescription()
          if(dscr_a.equals(dscr_b)){
            item[i+1] = new String(String.format("[%d] - [%s] [%s]\n", i, g_ports[i].getSystemPortName(), g_ports[i].getDescriptivePortName()));
          } else {
            item[i+1] = new String(String.format("[%d] - [%s] [%s] [%s]\n", i, g_ports[i].getSystemPortName(), g_ports[i].getDescriptivePortName(), g_ports[i].getPortDescription() ));
          }
        }     
        lcDroplist.setItems(item, 0); // Upd droplist in UI
        lcDroplist.setVisible(true);
      }
  } // lookupInterval
  
  return;
}



/**
 * Non-blocking (to be called in a loop) 
 * Calls phRaceDev.identifyBoard() with timeout
 * Instantiate "g_currentBoardSoftware" global obj
 *   [g_currentBoardSoftware] obj may be dummy/empty 
 *   if the answer from board contains an unknown software id
 *
 * @return 0 if board identication not yet completed 
 * @return 1 when done
 * @return -1 on error (ex: on Open Port error) 
 */  
int boardIdentified() {
  boolean success = phRaceDev.identifyBoard(g_serialPort);
  if(success) {
    String boardSoftware[] = phRaceDev.getBoardSoftware();
    if(boardSoftware != null){
      g_currentBoardSoftware = softwareList.getSoftware(boardSoftware[0], boardSoftware[1], g_platform.id);
      g_t.log(Trace.INTERNALS,String.format("boardIdentified(): Found Software Id/Version[%s][%s]\n", g_currentBoardSoftware.id, g_currentBoardSoftware.version));  
      g_t.log(Trace.LOWLEVEL,String.format ("boardIdentified(): Found Software OBJ dump:\n%s\n", g_currentBoardSoftware.toString()));
      return(1);
    } else {
      g_t.error(String.format("boardIdentified(): phRaceDev.identifyBoard() return TRUE but invalid SoftwareId/Ver returned! Board Set to [%s][%s]\n",g_currentBoardSoftware.id, g_currentBoardSoftware.version));
      g_currentBoardSoftware = softwareList.getSoftware(g_platform.id); // Empty/dummy obj with accuracy=0
      return(-1);
    }
    //return(true);
  }
  // check timeout     
  if(System.currentTimeMillis() > identifyStartTime + identifyTimeout) {
    g_currentBoardSoftware = softwareList.getSoftware(g_platform.id); // Empty/dummy obj with accuracy=0
    g_t.log(Trace.INTERNALS, String.format("boardIdentified(): TIMEOUT [%.0f] msec - Board Set to [%s][%s]\n", identifyTimeout, g_currentBoardSoftware.id, g_currentBoardSoftware.version));
    // Reset internal status of phRaceDev.identifyBoard()->sendConfirmedCmdToBoard()->sendConfirmedCmd_sent
    // Needed for next call to getBoardSoftware() (on START_OVER after an upload)
    phRaceDev.resetSendConfirmedCmdToBoard(); 
    if(phRaceDev.isConnected()) {
      return(1);
    }
    return(-1);
  }
  return(0);
}



/*
 * Used when FSM enters in [STATUS.USR_UPLOAD_BOARD] to generate
 * the String[] for "g_upListArea.fillGrid()" (UI with the availbale
 * firmware for the platform and "buttons" to Upgrade, Opload, etc)
 */
String [] uiGetUploadList() {

  StringBuilder sb = new StringBuilder();
  Formatter fmt = new Formatter(sb);
  ArrayList<String> content= new ArrayList<String>();

  content.add("Name; Version");
  // getContent() returns: String[n][7] [n]:Items / [7]:{id, name, version, platform, hexfile, description[0], url}
  String[][] ul = softwareList.getUploadableContent(g_platform.id);

  for(int i = 0 ; i < ul.length ; i++){
    String uplId     = ul[i][0].trim();
    String uplName    = ul[i][1].trim();
    String uplVersion = ul[i][2].trim();
    String uplUrl     = ul[i][6].trim();

    String prevUplId = null;
    if (i>0) prevUplId=ul[i-1][0].trim() ;
    
    if((uplId.equals(prevUplId))){
       fmt.format(String.format(" ;%s", uplVersion)); // do not repeat "Name" for a different version of the same software
    } else {
      fmt.format(String.format("%s;%s", uplName,uplVersion));
    }
    // Add Update/Upload buttons.
    
    // Add "more..." link
    if(!(uplUrl == null || uplUrl.trim().isEmpty())) {    
          fmt.format("; %s", uiAddLink("more...","URL_"+uplId+"_"+uplVersion+"_"+g_platform.id, 80, 20));
    }
    
    if(uplId.trim().equals(g_currentBoardSoftware.id.trim())) { 
      // Board is running this Software
      int cmp = Utils.compareSoftwareVersion(g_currentBoardSoftware.version, uplVersion);
      if(cmp==0){
        // Board is running the same version of this Software
        fmt.format(";Installed");
      } else if(cmp == -1) {
        // Board is running a previous version of this Software
        fmt.format("; %s", uiAddButton("Upgrade","UPLOAD_"+uplId+"_"+uplVersion+"_"+g_platform.id, 80, 20));
      }  else if(cmp == 1) {
        // Board is running a newer version of this Software
        fmt.format("; %s ;<L>Board runs a newer Version !", uiAddButton("Downgrade","UPLOAD_"+uplId+"_"+uplVersion+"_"+g_platform.id, 90, 20));
      } else {
        fmt.format(";<L>Error on version number !!! see errorlog");
        content.add(sb.toString()); 
        return(content.toArray(new String[0]));
      }
    } else { 
      // Board is running a different Software
      fmt.format("; %s", uiAddButton("Upload","UPLOAD_"+uplId+"_"+uplVersion+"_"+g_platform.id, 80, 20));
    }
    content.add(sb.toString()); 
    sb.setLength(0); //empty StringBuilder
  }
  return(content.toArray(new String[0]));
  
}

/*
 * helper method for uiGetUploadList() above
 */
String uiAddButton(String id, String text, String tag, int w, int h) {            

  return(String.format("%s, %s, %s, %d,%d,%d", 
                  id,       // The keyword telling ScrollableGrid Class that this cell is a Button/Link
                  text,     // Button Text
                  tag ,     // ButtonTag - Identify Button
                            // used in UIHandlers--->handleButtonEvents()
                  0 ,       // Button Numeric Id - not used (uses ButtonTag)
                  w,        // Button Width
                  h         // Button Height
                  ));            
}
String uiAddButton(String text, String tag, int w, int h) {
  return(uiAddButton("BUTTON", text, tag, w, h));
}
String uiAddLink(String text, String tag, int w, int h) {
  return(uiAddButton("LINK", text, tag, w, h));
}

  
/*
 * Dispose "g_upListArea" to destroy dynamically create buttons 
 */
void resetUploadList() {
  if(uploadListLoaded) {  
    uploadListLoaded=false;
    g_upListArea.dispose();
  }
}



/**
 * Used in [STATUS.USR_CONFIG_BOARD_A4P0_A_SAVE] 
 * to loop until board configuration save completed.
 * Non-blocking - Calls cfgA4P0.saveBoardCfg() with TIMEOUT
 */  
int boardConfigSaved() {
  int done = cfgA4P0.saveBoardCfg();
  if(done != 0) {
    saveStarted = false; // reset timer
    return(done);
  }
  // check timeout     
  if(System.currentTimeMillis() > saveStartTime + saveTimeout) {
    g_t.error(String.format("boardConfigSaved(): TIMEOUT [%.0f] msec\n", saveTimeout));
    saveStarted = false; // reset timer
    return(-1);
  }
  return(0);
}

/*
 * 
 */
void resetBoardCfgLoaded() {
    g_boardCfgLoaded =false;
}


/**
 *  
 */  
String getSoftwareBaseDescr(){
  switch(g_currentBoardSoftware.accuracy) {
    case 0:
      return(g_currentBoardSoftware.id.concat("(Unknown)"));
    case 1:
      return(g_currentBoardSoftware.name.concat(" - Unknown Version"));
    case 2:
    case 3:
      return(g_currentBoardSoftware.name.concat(" -  Ver:").concat(g_currentBoardSoftware.version));

    default:
      g_t.error(String.format("getSoftwareBaseDescr() - Software error: accuracy=[%d]", g_currentBoardSoftware.accuracy));
      return(String.format("getSoftwareBaseDescr() - Software error: accuracy=[%d]", g_currentBoardSoftware.accuracy));
  }
}

/**
 *  
 */ 
boolean loadSoftwareDescription() {  
  if(g_currentBoardSoftware.accuracy == 0){
    String[] tmp = {
        "The Board is running an Unknown Software",
        ""};
    g_boardDescr.setText(tmp, 780);
    return(false);    
  }
  g_boardDescr.setText(g_currentBoardSoftware.description, 780);
  return(true);  
}


/*
 * Check if this version of the OLRManager implements a "Configure" UI for the firmware
 * Note:
 *  Updates of the OLRManger package can contain:
 *    - new OLRManager 'executable' plus  "data/hex/cfg" files 
 *    - "data/hex/cfg" files ONLY (in case there is a new firmware available
 *      and no need to update this OLRConfig App 
 * 
 * After a "data/hex/cfg" ONLY Update, the version of this package is newer
 * than the version of this OLRConfig
 *
 * This is why the information about "OLRManager implements UIConfig for this firmware"
 * is stored in the "softwareList.json" cfg file.
 *
 * Returns: STATUS.code managing the "Configure" for the specific firmware Id/Version
 */
int getConfigBoardStatus(){
    int status = STATUS.USR_CONFIG_BOARD_NONE;
    if(g_currentBoardSoftware.minOlrManagerVersion.equals("NA") || g_currentBoardSoftware.maxOlrManagerVersion.equals("NA")){
      g_t.log(Trace.METHOD + Trace.LOWLEVEL, "getConfigBoardStatus() -> configuration file contains [NA] for min/max Manager Version\n");
      return(status);
    }
    if(g_currentBoardSoftware.id.equals("A4P0")){
      int managerVer[]=ProtocolSerial.Cmd.GetSoftwareVersion.getParam(thisOlrManagerVersion);
      int minReqVer[]=ProtocolSerial.Cmd.GetSoftwareVersion.getParam(g_currentBoardSoftware.minOlrManagerVersion);
      int maxReqVer[]=ProtocolSerial.Cmd.GetSoftwareVersion.getParam(g_currentBoardSoftware.maxOlrManagerVersion);
      g_t.log(Trace.DETAILED, String.format("getConfigBoardStatus()->Configuration for firmware=[%s] managed by OLRManager versions from [%s] to [%s] - This is version [%s]\n","A4P0", 
                                              g_currentBoardSoftware.minOlrManagerVersion, 
                                              g_currentBoardSoftware.maxOlrManagerVersion,
                                              thisOlrManagerVersion));
      
      // compareSoftwareVersion(a,b) returns:  -1:if a<b, 0:if a=b, 1:if a>b, -2:on Error
      int lo = Utils.compareSoftwareVersion(managerVer, minReqVer); 
      int hi = Utils.compareSoftwareVersion(managerVer, maxReqVer);
      if(lo >= 0 && hi <= 0){
        status=STATUS.USR_CONFIG_BOARD_A4P0_A;    
        g_saveConfigStatus=STATUS.USR_CONFIG_BOARD_A4P0_A_SAVE;
        g_t.log(Trace.DETAILED, String.format("getConfigBoardStatus()->Firmware Configuration managed by Status:[%d:USR_CONFIG_BOARD_A4P0_A]\n", status));
        return(status);
      } else {
        g_t.log(Trace.DETAILED, String.format("getConfigBoardStatus()->Firmware Configuration NOT managed by this version=[%s]\n", thisOlrManagerVersion));
      }
    }
    return(status);
}
     

/**
 * 
 */
public boolean userMessageTimeElapsed(){  
  if( ! tmpUserMsgStarted ) { 
      tmpUserMsgStarted = true;
      tmpUserMsgStartTime = System.currentTimeMillis(); 
  }
  if(System.currentTimeMillis() > tmpUserMsgStartTime + tmpUserMsgTimeout) {
    tmpUserMsgStarted = false;
    return(true);
  }
  return(false);
}


/*
 * Load JSON configuration files
 */

void loadConfig() {

   
  String redirConsole = null ;
  
//  JSONObject json=null;
  try {
    jsonMainCfg = loadJSONObject(cfgFileName);
  } catch (RuntimeException e) {  
    System.err.printf("Error loading cfg file:[%s] - [%s]\n ", cfgFileName, e);
    noLoop();
    exit();
  }
  
  // Get Operating System Name (Linux, Windows)  
  String osName= System.getProperty("os.name").toLowerCase().replaceAll(" ", "");
  
  if( ! ( (osName.indexOf("nux") >= 0) || (osName.indexOf("windows10") >= 0) ) )  {
    System.err.printf("Error: Operating System Name not recognized:[%s] - Should be [linux] or [windows10]\n ", osName);
    noLoop();
    exit();
  }
  
  if(jsonMainCfg != null ) {
    
    // Base parameters
    //////////////////
    JSONObject base = jsonMainCfg.getJSONObject("Base");
    if(base != null ) {  
  
      String logStr   = trim(base.getString("LogLevel" , "-"));    
      if(logStr.equals("-") ){
        g_t = new Trace(Trace.NONE);
      } else {
        g_t = new Trace(logStr);
      }
      g_t.setClassId(g_CLASS_);
      g_t.log(Trace.METHOD, String.format("loadConfig()\n"));

      g_t.log(Trace.ALL, String.format("loadConfig() - Operating System:[%s]\n",osName));
      g_t.log(Trace.LOWLEVEL, String.format("loadConfig() - Loaded JSON cfg file[%s]:\n%s\n",cfgFileName,jsonMainCfg.toString()));

      // Console output redirection
      String r = trim(base.getString("redirConsole"));
      if(r != null) {
        redirConsole = new String(trim(r));
        if( ! redirConsole.equals("-") ) {
          try { 
            String file = sketchPath("")+ redirConsole;
            FileOutputStream outStr = new FileOutputStream(file, false);
            PrintStream printStream = new PrintStream(outStr);
            System.setOut(printStream);
            System.setErr(printStream);
          } catch (IOException e) {
            System.err.println("Unrecoverable Error - Check path, or filename, or security manager! "+e);
            exit();
          }
        }
      }

      // Code name
      String cn = trim(base.getString("codeName"));
      if(cn != null ) surface.setTitle(cn + " - ver " + thisOlrManagerVersion);
 
 
      // App Version 
      // When the Main JSON CFG file is "created" for a Download the Value for the "app_version" 
      // Attribute is SET to the "thisOlrManagerVersion"
      // This is just a paramoid check (...) to see if the Main JSON CFG file has been
      // updated and is the one intended for this OLRManager Version
      String appVer = trim(base.getString("app_version"));
      if(appVer != null ){
        if( ! appVer.equals(thisOlrManagerVersion)){
          System.err.printf("Unrecoverable Error - JSON Config file was generated for a diferent Manager version:[%s] - This Manager Version:[%s]\n ", appVer, thisOlrManagerVersion);
          noLoop();
          exit();
        }
      } 

      // Read Loglevel settings
      //JSONObject logLevel = getJSONObjectOrExit(json, "LogLevel");
      jsonMainCfg_Loglevel = getJSONObjectOrExit(jsonMainCfg, "LogLevel");

      if ( jsonMainCfg.isNull("RaceDevice")) {
         System.err.printf("Unrecoverable Error - Json cfg file:[%s] - Missing \"RaceDevice\" section.", cfgFileName);
         noLoop();
         exit();
      }      
      // Instantiate [RaceDevice] object passing the specific JSON cfg [RaceDevice] section to the constuctor 
      JSONObject pd = getJSONObjectOrExit(jsonMainCfg, "RaceDevice");
      phRaceDev=new RaceDevice(pd.getString("name"), pd, trim(jsonMainCfg_Loglevel.getString("RaceDevice" , "-")));

      // Instantiate [SwConfig_A4P0] object passing the specific JSON cfg [RaceDevice] section to the constuctor
      cfgA4P0 = new SwConfig_A4P0(phRaceDev,trim(jsonMainCfg_Loglevel.getString("SwConfig_A4P0" , "-"))); 
 
      
      // Get PlatformList config file name and instantiate Object
      ///////////////////////////////////////////////////////////
      String localpath = sketchPath(dataPath);
      String pl_fname = localpath +  File.separator + getJSONStringOrExit(base,"platformlistFile");
      g_platform = new Platform(osName, localpath, pl_fname, trim(jsonMainCfg_Loglevel.getString("PlatformList" , "-"))); 
      
      
      // Get SoftwareList config file name and instantiate Object
      ///////////////////////////////////////////////////////////
      String sl_fname = sketchPath(dataPath) +  File.separator + getJSONStringOrExit(base,"softwarelistFile");
      softwareList = new SoftwareList(sl_fname,trim(jsonMainCfg_Loglevel.getString("SoftwareList" , "-")));

      // Get Package Upgrade config file name  and instantiate Object
      ///////////////////////////////////////////////////////////////
      String pkg_fname = sketchPath(dataPath) +  File.separator + getJSONStringOrExit(base,"packageUpgradeFile");
//      chkUpgrade = new AsyncGetAvailabelUpgrade(pkg_fname,trim(jsonMainCfg_Loglevel.getString("PackageUpgrade" , "-")));

      // Set Publisher ID in LOG/TRACE object
      g_t.setPublisherId("");

      // Available Serial port list Obj 
      /////////////////////////////////
      serialPortList = new SerialPortList ("board",trim(jsonMainCfg_Loglevel.getString("SerialPortList" , "-")));

    } else {
      System.err.printf("Unrecoverable Error - Json cfg file:[%s] - Missing \"base\" section.", cfgFileName);
      exit();
    }
 
  } else {
     System.err.printf("Unrecoverable Error - JSON Object not loaded - Check filename:[%s]",cfgFileName);
  }

}
