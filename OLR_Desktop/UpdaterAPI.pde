class UpdaterAPI {

  class Upgrade {
    int    _totFound = -1;
    String _date = null;
    String _dwl  = null;
    String _page = null; 
  
    Upgrade(int totFound, String date, String dwl, String page){
      this._totFound = totFound;
      this._date = date;
      this._dwl  = dwl;
      this._page = page; 
    }
    Upgrade(int totFound){
      this._totFound = totFound;
      this._date = null;
      this._dwl  = null;
      this._page = null; 
    }
    String getDate(){
      return(this._date);
    }
    String getDownload(){
      return(this._dwl);
    }
    String getPage(){
      return(this._page);
    }
    int getTotFound(){
      return(this._totFound);
    }
    String toString(){
      return(String.format("Upgrade:\n totFound:[%d]\n date:[%s]\n dwl:[%s]\n page:[%s]\n", _totFound, _date, _dwl, _page));
    }
    
  }
  
  String _baseURL ;  
  String _getUpgrade ;
  JSONObject packageData=null;
  JSONObject postParam=null;

  private Trace t;
  private static final String _CLASS_ = "UpdaterAPI";
  
  // Constructor
  UpdaterAPI(String pl_fname, String logStr) {

      if(logStr.equals("-") ){
        t = new Trace(Trace.NONE);
      } else {
        t = new Trace(logStr);
      }      
      t.setClassId(_CLASS_);
      t.log(Trace.METHOD, String.format("Constructor(Logstring:%s)\n",logStr));

      JSONObject json=null;
      
      try {
        json = loadJSONObject(pl_fname);
      } catch (RuntimeException e) {  
        System.err.printf("Error loading Package cfg file:[%s] - [%s]\n ", pl_fname, e);
        noLoop();
        exit();
      }
      this.packageData = json.getJSONObject("packageData");
      if(this.packageData == null){
         t.error(String.format("Error! Json cfg file - Missing \"packageData\" section."));
         exit();
      }
      this.postParam = json.getJSONObject("postParam");
      if(this.postParam == null){
         t.error(String.format("Error! Json cfg file - Missing \"postParam\" section."));
         exit();
      }
      
      this._baseURL    = getJSONStringOrExit(json, "apiURL");
      this._getUpgrade = getJSONStringOrExit(json, "routeUpgrade");
          
      
      t.log(Trace.LOWLEVEL, String.format("Constructor() - Loaded JSON cfg file[%s]:\n%s\n%s\n",pl_fname,this.packageData.toString(),this.postParam.toString()));
  }   
  
  boolean setBaseURL(String url){
    this._baseURL = url;
    return(true);
  }
  
  /*
   *
   */
  Upgrade getAvailabelUpgrade() {
    String url = _baseURL + _getUpgrade;
    PostRequest post = new PostRequest(url);
    post.addData(this.postParam.getString("platform", "_"), this.packageData.getString("platform", "_") );
    post.addData(this.postParam.getString("os", "_"), this.packageData.getString("os", "_") );
    post.addData(this.postParam.getString("platform_ver", "_"), this.packageData.getString("platform_ver", "_") );
    post.addData(this.postParam.getString("pkg_id", "_"), this.packageData.getString("pkg_id", "_") );
    post.addData(this.postParam.getString("pkg_type", "_"), this.packageData.getString("pkg_type", "_") );
    post.addData(this.postParam.getString("pkg_date", "_"), this.packageData.getString("pkg_date", "_") );
   
    post.send();
      
    String result = post.getContent();
    JSONObject json;
    if(result == null) {
      return(null);
    }
    json = parseJSONObject(result);
    //
    int res = json.getInt("result", -3);
    Upgrade upg;
    if(res > 0) {
      String date = json.getJSONObject("data").getString("pkg_date", "-");
      String dwl  = json.getJSONObject("data").getString("d_url", "-");
      String page = json.getJSONObject("data").getString("p_url", "-");
      upg = new Upgrade(res, date,dwl,page);
    } else {
      upg = new Upgrade(res);
    }
    return(upg);
    //return(json);
  }
  
  
}

 
// Set asynchronous class
class AsyncGetAvailabelUpgrade extends Thread {

    public byte FAILURE = 0;
    public byte WORKING = 1;
    public byte SUCCESS = 2;

    public byte status;
    
    private UpdaterAPI _updater;
    private UpdaterAPI.Upgrade  _update;

  
    public AsyncGetAvailabelUpgrade(String pl_fname, String logStr) {
      status = WORKING;
      _updater = new UpdaterAPI(pl_fname, logStr); 
    }
  
    public void run() {
      _update = _updater.getAvailabelUpgrade();      
      if(_update == null){
        status = FAILURE;
        return;
      }
      // An error message should have already printed
      if(_update.getTotFound() < 0) {
        status = FAILURE;
      }
      else {
        status = SUCCESS;
      }
    }
  
    public UpdaterAPI.Upgrade get() {
      if(status == SUCCESS) return(_update);
      return(null);
    }

}



  
