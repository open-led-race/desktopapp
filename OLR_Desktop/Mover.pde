class Mover {

  PVector position;
  PVector velocity;
  float _x;
  float _y;
  float _width;
  float _height;
  float _ballSize = 20;

  Mover(float x, float y, float w, float h) {
    setFrame(x, y, w, h);
  }

  void setFrame(float x, float y, float w, float h) {
    this._x = x;
    this._y = y;
    this._width = w;
    this._height = h;
    
    position = new PVector(_x + random(60, ((int)_width>>2)), y + random(60, ((int)_height >> 2)));
    velocity = new PVector(1, 2);
  }


  void update() {
    position.add(velocity);
  }

  void display() {
    stroke(210);
    noFill();
    rect(_x,_y,_width,_height,7);
    strokeWeight(2);
    fill(254,54,54);
    ellipse(position.x, position.y, _ballSize, _ballSize);
  }

  void checkEdges() {

    if (position.x > this._x + _width - ((int)_ballSize >> 2) || position.x < this._x + ((int)_ballSize >> 2)) {
      velocity.x *= -1;
    } 
    if ( position.y > this._y + _height - ((int)_ballSize >> 2)  || position.y < this._y + ((int)_ballSize >> 2) ) {
      velocity.y *= -1;
    } 
  }
  
  void loop() {
    update();
    checkEdges();
    display();
  }
}
